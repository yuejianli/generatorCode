package top.yueshushu.extension.business;

import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.extension.model.vo.ExtFastingPdfVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 *  提供的接口
 * @author yuejianli
 * @date 2022/6/11 15:10
 **/
public interface ExtFastingBusiness {
    /**
     * 执行 斋戒 的定时任务
     *
     * @param interfaceId 定时任务id
     */
    String fastingJob(Integer interfaceId);

    void show(HttpServletResponse httpServletResponse) throws IOException;

    OutputResult<List<ExtFastingPdfVo>> showList();

}
