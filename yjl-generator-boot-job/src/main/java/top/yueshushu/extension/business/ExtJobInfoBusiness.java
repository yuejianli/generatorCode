package top.yueshushu.extension.business;


import top.yueshushu.extension.enums.JobInfoType;

/**
 * 定时任务编排层
 *
 * @author Yue Jianli
 * @date 2022-06-02
 */

public interface ExtJobInfoBusiness {
    /**
     * 执行每天的任务
     *  @param jobInfoType 任务类型
     *
     */
    void execDay(JobInfoType jobInfoType);

    /**
      执行斋戒日期 处理
     * @param jobInfoType
     */
    void execFasting(JobInfoType jobInfoType);
}
