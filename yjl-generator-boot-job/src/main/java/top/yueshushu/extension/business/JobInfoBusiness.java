package top.yueshushu.extension.business;

import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.model.ro.JobInfoRo;
import top.yueshushu.extension.model.ro.OperateJobInfo;

/**
 * 定时任务编排层
 *
 * @author Yue Jianli
 * @date 2022-06-02
 */

public interface JobInfoBusiness {
    /**
     * 分页查询任务信息
     *
     * @param jobInfoRo 任务RO对象
     * @return top.yueshushu.learn.common.response.OutputResult
     * @date 2022/6/2 11:52
     * @author yuejianli
     */
    OutputResult listJob(JobInfoRo jobInfoRo);

    /**
     * 改变任务的状态，禁用还是启用
     *
     * @param id           任务编号
     * @param dataFlagType 任务状态
     * @return top.yueshushu.learn.common.response.OutputResult
     * @date 2022/6/2 13:53
     * @author yuejianli
     */
    OutputResult changeStatus(Integer id, DataFlagType dataFlagType);


    /**
     * 删除定时任务
     *
     * @param id 任务编号id
     */
    OutputResult deleteById(Integer id);

    /**
     * 手动执行定时任务
     *
     * @param id 任务编号id
     */
    OutputResult handlerById(Integer id);

    /**
     * 新的 cron 表达式
     *
     * @param id   任务编号id
     * @param cron 新的cron 表达式
     */
    OutputResult changeCron(Integer id, String cron);

    /**
     * 创建定时任务
     *
     * @param operateJobInfo 定时任务对象
     */
    OutputResult createInfo(OperateJobInfo operateJobInfo);
    /**
     * 更新定时任务
     * @param operateJobInfo 定时任务
     */
    OutputResult updateInfo(OperateJobInfo operateJobInfo);
}
