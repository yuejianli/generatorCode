package top.yueshushu.extension.business.impl;

import org.springframework.stereotype.Service;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.extension.business.ExtCustomerJobBusiness;
import top.yueshushu.extension.entity.ExtInterface;
import top.yueshushu.extension.model.ro.ExtCustomerJobRo;
import top.yueshushu.extension.model.vo.ExtCustomerJobVo;
import top.yueshushu.extension.service.ExtCustomerJobService;
import top.yueshushu.extension.service.ExtJobInterfaceService;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户配置功能接口
 * @author yuejianli
 * @date 2022/6/11 15:09
 **/
@Service
public class ExtCustomerJobBusinessImpl implements ExtCustomerJobBusiness {
    @Resource
    private ExtCustomerJobService extCustomerJobService;
    @Resource
    private ExtJobInterfaceService extJobInterfaceService;

    @Override
    public OutputResult list(ExtCustomerJobRo extCustomerJobRo) {
        OutputResult<ExtCustomerJobVo> outputResult = extCustomerJobService.list(extCustomerJobRo);
        //获取信息
        ExtCustomerJobVo data = outputResult.getData();
        List<ExtInterface> extInterfaceList = extJobInterfaceService.listAllByJobId(extCustomerJobRo.getExtJobId());
        data.setAllInterfaceList(extInterfaceList);
        return OutputResult.buildSucc(data);
    }

    @Override
    public OutputResult config(ExtCustomerJobRo extCustomerJobRo) {
        return extCustomerJobService.config(extCustomerJobRo);
    }
}
