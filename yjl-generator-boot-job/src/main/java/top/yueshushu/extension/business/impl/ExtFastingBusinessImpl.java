package top.yueshushu.extension.business.impl;

import cn.hutool.core.date.ChineseDate;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.extension.assembler.ExtFastingAssembler;
import top.yueshushu.extension.business.ExtFastingBusiness;
import top.yueshushu.extension.common.ConfigKey;
import top.yueshushu.extension.entity.ExtFasting;
import top.yueshushu.extension.enums.VelocityTemplateType;
import top.yueshushu.extension.model.vo.ExtFastingPdfVo;
import top.yueshushu.extension.model.vo.ExtFastingVo;
import top.yueshushu.extension.service.ExtFastingService;
import top.yueshushu.extension.util.PdfUtil;
import top.yueshushu.extension.util.RedisUtil;
import top.yueshushu.message.email.EmailService;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *  接口实现应用
 * @author yuejianli
 * @date 2022/6/11 15:10
 **/
@Service
public class ExtFastingBusinessImpl implements ExtFastingBusiness {
    public static final String NO_DAY = "不是禁忌日";
    @Resource
    private ExtFastingService extFastingService;
    @Resource
    private ExtFastingAssembler extFastingAssembler;
    @Resource
    private EmailService emailService;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public String fastingJob(Integer interfaceId) {
        //根据接口id ,查询相应的信息.
        //能得到信息.
        Date now = DateUtil.date();

        String nowMessage = dateConvert(now, 0);

        Date morringDate = DateUtil.offsetDay(now, 1);
        
        String tomorrowMessage = dateConvert(morringDate,1);

        String result = "";
        String line = System.lineSeparator();
        if (StringUtils.hasText(nowMessage)) {
            result = result.concat(nowMessage).concat(line);
        }
        if (StringUtils.hasText(tomorrowMessage)) {
            result = result.concat(tomorrowMessage).concat(line);
        }
        return result;
    }

    @Override
    public void show(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.setContentType("application/pdf");
        try {
            //2.获取response字节输出流
            ServletOutputStream os = httpServletResponse.getOutputStream();
            String nowText = getNowText();
            byte[] bytes = PdfUtil.html2Pdf(nowText);
            os.write(bytes);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public OutputResult<List<ExtFastingPdfVo>> showList() {
        // 查询数据.
        List<ExtFastingPdfVo> extFastingPdfVoList = new ArrayList<>();
        Date now = DateUtil.date();
        for (int i = -1; i < 9; i++) {
            Date tempDate = DateUtil.offsetDay(now, i);
            ExtFastingPdfVo extFastingPdfVo = new ExtFastingPdfVo();
            extFastingPdfVo.setDate(DateUtil.format(tempDate, DatePattern.NORM_DATE_PATTERN));
            String message = datePdfConvert(tempDate);
            extFastingPdfVo.setMessage(message);
            extFastingPdfVo.setShow(NO_DAY.equalsIgnoreCase(message) ? 0 : 1);
            extFastingPdfVoList.add(extFastingPdfVo);
        }
        return OutputResult.buildSucc(extFastingPdfVoList);
    }

    public String getNowText() {
        Object obj = redisUtil.get(ConfigKey.EXT_FASTING);
        if (!ObjectUtils.isEmpty(obj)) {
            return obj.toString();
        }
        // 查询数据.
        List<ExtFastingPdfVo> extFastingPdfVoList = showList().getData();
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("name", "岳泽霖");
        dataMap.put("notes", "禁忌日,请务必洁身自好");
        dataMap.put("dataList", extFastingPdfVoList);
        String velocityMailText = emailService.getVelocityContent(VelocityTemplateType.STOCK_FASTING, dataMap);
        redisUtil.set(ConfigKey.EXT_FASTING, velocityMailText, 6, TimeUnit.HOURS);
        return velocityMailText;
    }


    private String datePdfConvert(Date date) {
        ChineseDate chineseDate = new ChineseDate(date);
        // 获取当前的月索引
        int month = chineseDate.getMonth();
        int day = chineseDate.getDay();
        //是节气
        String term = chineseDate.getTerm();
        ExtFasting extFasting = extFastingService.getByMonthAndDay(month, day, term);
        //转换成 Vo
        ExtFastingVo extFastingVo = extFastingAssembler.entityToVo(extFasting);
        String message = "";
        if (extFastingVo != null) {
            message = covertVoToPdfMessage(extFastingVo, date);
        } else {
            message = NO_DAY;
        }
        return message;
    }


    private String dateConvert(Date date, Integer index) {
        ChineseDate chineseDate = new ChineseDate(date);
        // 获取当前的月索引
        int month = chineseDate.getMonth();
        int day = chineseDate.getDay();
        //是节气
        String term = chineseDate.getTerm();
        ExtFasting extFasting = extFastingService.getByMonthAndDay(month, day, term);
        //转换成 Vo
        ExtFastingVo extFastingVo = extFastingAssembler.entityToVo(extFasting);
        String message = "";
        if (extFastingVo != null) {
            message = covertVoToMessage(extFastingVo, date, index);
        } else {
            message = covertVoToNoMessage(date, index);
        }
        return message;
    }


    /**
     * 将对象转换成微信要发送的消息
     *
     * @param extFastingVo 禁期对象
     * @return 将对象转换成微信要发送的消息
     */
    private String covertVoToPdfMessage(ExtFastingVo extFastingVo, Date date) {

        if (StringUtils.isEmpty(extFastingVo.getFastingReason())) {
            extFastingVo.setFastingReasonList(Collections.emptyList());
        } else {
            extFastingVo.setFastingReasonList(Arrays.asList(extFastingVo.getFastingReason().split("\\,")));
        }
        if (StringUtils.isEmpty(extFastingVo.getDamage())) {
            extFastingVo.setDamageList(Collections.emptyList());
        } else {
            extFastingVo.setDamageList(Arrays.asList(extFastingVo.getDamage().split("\\,")));
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (extFastingVo.getType() == 1) {
            if (!CollectionUtils.isEmpty(extFastingVo.getFastingReasonList())) {
                //不为空，往下继续进行.
                int i = 0;
                for (String reason : extFastingVo.getFastingReasonList()) {
                    String damage = " - ";
                    if (extFastingVo.getDamageList().size() > i) {
                        damage = extFastingVo.getDamageList().get(i);
                    }
                    ++i;
                    stringBuilder.append("戒由").append(i).append(reason).append("  ")
                            .append(" 犯者报应：").append(damage);

                }
            }
        } else {
            stringBuilder.append("注意:").append(extFastingVo.getNotes());
        }
        return stringBuilder.toString();
    }


    /**
     * 将对象转换成微信要发送的消息
     *
     * @param extFastingVo 禁期对象
     * @return 将对象转换成微信要发送的消息
     */
    private String covertVoToMessage(ExtFastingVo extFastingVo, Date date, Integer index) {

        if (StringUtils.isEmpty(extFastingVo.getFastingReason())) {
            extFastingVo.setFastingReasonList(Collections.emptyList());
        } else {
            extFastingVo.setFastingReasonList(Arrays.asList(extFastingVo.getFastingReason().split("\\,")));
        }
        if (StringUtils.isEmpty(extFastingVo.getDamage())) {
            extFastingVo.setDamageList(Collections.emptyList());
        } else {
            extFastingVo.setDamageList(Arrays.asList(extFastingVo.getDamage().split("\\,")));
        }
        String dayMessage = index == 0 ? "今天" : "明天";
        ChineseDate chineseDate = new ChineseDate(date);
        StringBuilder stringBuilder = new StringBuilder();
        String line = System.lineSeparator();
        if (extFastingVo.getType() == 1) {
            stringBuilder.append(dayMessage + "是农历").append(chineseDate.getChineseMonth())
                    .append(" ").append(chineseDate.getChineseDay()).append(line);
            if (!CollectionUtils.isEmpty(extFastingVo.getFastingReasonList())) {
                //不为空，往下继续进行.
                int i = 0;
                for (String reason : extFastingVo.getFastingReasonList()) {
                    String damage = " - ";
                    if (extFastingVo.getDamageList().size() > i) {
                        damage = extFastingVo.getDamageList().get(i);
                    }
                    ++i;
                    stringBuilder.append("戒由").append(i).append(reason).append("  ")
                            .append(" 犯者报应：").append(damage)
                            .append(line);

                }
            }
        } else {
            stringBuilder.append(dayMessage + "是").append(extFastingVo.getJieQi()).append(line);
            stringBuilder.append("注意:").append(extFastingVo.getNotes());
        }
        stringBuilder.append(line).append("请务必洁身自好").append(line);
        return stringBuilder.toString();
    }

    /**
     * 将对象转换成微信要发送的消息
     * @return 将对象转换成微信要发送的消息
     */
    private String covertVoToNoMessage(Date date, Integer index) {
        String dayMessage = index == 0 ? "今天" : "明天";
        ChineseDate chineseDate = new ChineseDate(date);
        StringBuilder stringBuilder = new StringBuilder();
        String line = System.lineSeparator();
        stringBuilder.append(dayMessage + "是农历").append(chineseDate.getChineseMonth())
                .append(" ").append(chineseDate.getChineseDay()).append(line);
        stringBuilder.append(line).append("不是禁忌日,但也请注意洁身自好").append(line);
        return stringBuilder.toString();
    }

}
