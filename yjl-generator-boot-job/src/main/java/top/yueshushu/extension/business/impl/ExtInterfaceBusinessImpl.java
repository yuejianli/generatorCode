package top.yueshushu.extension.business.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.extension.business.ExtInterfaceBusiness;
import top.yueshushu.extension.model.dto.shici.PoemResponse;
import top.yueshushu.extension.model.ro.ExtInterfaceRo;
import top.yueshushu.extension.model.vo.PoemVo;
import top.yueshushu.extension.service.ExtFunctionService;
import top.yueshushu.extension.service.ExtInterfaceService;

import javax.annotation.Resource;

/**
 *  接口实现应用
 * @author yuejianli
 * @date 2022/6/11 15:10
 **/
@Service
public class ExtInterfaceBusinessImpl implements ExtInterfaceBusiness {
    @Resource
    private ExtInterfaceService extInterfaceService;
    @Resource
    private ExtFunctionService extFunctionService;

    @Override
    public OutputResult list(ExtInterfaceRo extInterfaceRo) {
        return extInterfaceService.pageList(extInterfaceRo);
    }

    @Override
    public OutputResult poem() {
        PoemResponse poemResponse = extFunctionService.getPoem();
        PoemVo poemVo = new PoemVo();
        BeanUtils.copyProperties(poemResponse, poemVo);
        return OutputResult.buildSucc(poemVo);
    }
}
