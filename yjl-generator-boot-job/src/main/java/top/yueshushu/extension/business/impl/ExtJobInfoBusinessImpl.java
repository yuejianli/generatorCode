package top.yueshushu.extension.business.impl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.extension.business.ExtFastingBusiness;
import top.yueshushu.extension.business.ExtJobInfoBusiness;
import top.yueshushu.extension.common.ConfigKey;
import top.yueshushu.extension.component.event.MessageInfo;
import top.yueshushu.extension.domain.ExtInterfaceDo;
import top.yueshushu.extension.domainservice.ExtInterfaceDomainService;
import top.yueshushu.extension.entity.ExtCustomer;
import top.yueshushu.extension.entity.ExtCustomerJob;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.enums.ExtInterfaceTemplateType;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.extension.enums.TopicType;
import top.yueshushu.extension.helper.PublishMessageHelper;
import top.yueshushu.extension.model.dto.JobInfoDto;
import top.yueshushu.extension.service.ExtCustomerJobService;
import top.yueshushu.extension.service.ExtCustomerService;
import top.yueshushu.extension.service.ExtTemplateTypeService;
import top.yueshushu.extension.service.JobInfoService;
import top.yueshushu.extension.util.MyDateUtil;
import top.yueshushu.extension.util.RedisUtil;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2022-06-02
 */
@Service
@Slf4j
public class ExtJobInfoBusinessImpl implements ExtJobInfoBusiness {
    @Resource
    private JobInfoService jobInfoService;

    @Resource
    private ExtCustomerJobService extCustomerJobService;
    @Resource
    private ExtInterfaceDomainService extInterfaceDomainService;

    @SuppressWarnings("all")
    @Resource(name = ConfigKey.ASYNC_SERVICE_EXECUTOR_BEAN_NAME)
    private AsyncTaskExecutor executor;
    @Resource
    private ExtTemplateTypeService extTemplateTypeService;
    @Resource
    private ExtCustomerService extCustomerService;
    @Resource
    private ExtFastingBusiness extFastingBusiness;
    @Resource
    private RedisUtil redisUtil;


    @Override
    public void execDay(JobInfoType jobInfoType) {
        if (jobInfoType == null) {
            return ;
        }
        //查询任务
        JobInfoDto jobInfoDto = jobInfoService.getByCode(jobInfoType);
        if (jobInfoDto == null) {
            log.info("当前任务 {} 已经被删除", jobInfoType.getDesc());
            return ;
        }
        // 禁用状态，不执行。
        if (!DataFlagType.NORMAL.getCode().equals(jobInfoDto.getTriggerStatus())) {
            log.info(">>当前任务 {}是禁用状态，不执行", jobInfoType.getDesc());
            return ;
        }
        //查询一下，当前任务所关联的用户对象列表.
        List<ExtCustomerJob> extCustomerJobList = extCustomerJobService.listByJobId(jobInfoDto.getId());
        if (CollectionUtils.isEmpty(extCustomerJobList)) {
            log.info(">>>>未查询出关联的用户");
            return ;
        }
        // 按照用户进行分组
        Map<Integer, List<ExtCustomerJob>> customerInterfaceMap = extCustomerJobList.stream()
                .collect(Collectors.groupingBy(ExtCustomerJob::getExtCustomerId));
        //获取所有的接口，并进行去重
        List<Integer> interfaceIdList = extCustomerJobList.stream().map(ExtCustomerJob::getExtInterfaceId).distinct().collect(Collectors.toList());

        List<ExtInterfaceTemplateType> extJobInfoTypeList = convertByIdList(interfaceIdList);
        // 去异步调用，每一个都获取信息.
        if (CollectionUtils.isEmpty(extCustomerJobList)) {
            log.info(">>>>未查询出相应的接口");
            return ;
        }
        //这里面获取响应信息了.
        boolean removeFlag =  extJobInfoTypeList.remove(ExtInterfaceTemplateType.WEATHER);
        Map<String, String> interfaceResponseMap = getResponse(extJobInfoTypeList);

        // 开始针对用户进行处理.
        Map<ExtCustomer, List<String>> convertCustomerInterfaceListMap = convertCustomerMap(customerInterfaceMap);

        // 查询出，用户和及相关的响应信息
        for (Map.Entry<ExtCustomer, List<String>> entry : convertCustomerInterfaceListMap.entrySet()) {
            // 查询出用户昵称
            ExtCustomer extCustomerDo = entry.getKey();
            if (extCustomerDo == null) {
                continue;
            }
            // 如果是移除成功，则进行处理.
            if (removeFlag) {
                redisUtil.set("city",extCustomerDo.getCity());
                Map<String, String> weatherResponseList = getResponse(Collections.singletonList(ExtInterfaceTemplateType.WEATHER));
                interfaceResponseMap.putAll(weatherResponseList);
            }
            List<String> interfaceCodeList = entry.getValue();
            // 获取响应信息.
            List<String> sendMessagetList = new ArrayList<>();
            StringBuilder stringBuilder = new StringBuilder();
            for (String interfaceCode : interfaceCodeList) {
                // 查询对应的信息
                String interfaceResponse = interfaceResponseMap.getOrDefault(interfaceCode, "");
                //如果是早安，或者是早安的话
                if (ExtInterfaceTemplateType.ZAOAN.getCode().equals(interfaceCode)
                        || ExtInterfaceTemplateType.WANAN.getCode().equals(interfaceCode)) {
                    if (!StringUtils.isEmpty(interfaceCode)) {
                        interfaceResponse = interfaceResponse.replaceFirst("我的朋友", extCustomerDo.getName());
                    }
                }
                // 获取当前字符串的信息
                if (stringBuilder.length() + interfaceResponse.length() > 900) {
                    //进行重置.
                    sendMessagetList.add(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                }
                stringBuilder.append(interfaceResponse);
            }

            // 如果当前用户是自己的话，
            if ("YueJianLi".equals(extCustomerDo.getWxId())) {
                // 感兴趣的城市
                List<String> loveCitys = Arrays.asList("410184");
                for (String city : loveCitys) {
                    redisUtil.set("city", city);
                    Map<String, String> weatherResponseList2 = getResponse(Collections.singletonList(ExtInterfaceTemplateType.WEATHER));
                    stringBuilder.append(weatherResponseList2.getOrDefault(ExtInterfaceTemplateType.WEATHER.getCode(), ""));
                }
            }


            //进行重置.
            sendMessagetList.add(stringBuilder.toString());
            //对响应的集合，进行处理.
            for (String sendMessage : sendMessagetList) {
                if (StringUtils.hasText(extCustomerDo.getWxId())) {
                    MessageInfo messageInfo = MessageInfo.buildBySignId(extCustomerDo.getWxId(), sendMessage, TopicType.EXTENDS);
                    messageInfo.setShow(false);
                    PublishMessageHelper.sendWxMessage(this, messageInfo);
                }
            }
        }
    }


    /**
     * 将其转换成用户昵称和 对应的接口 code码的形式
     *
     * @param customerInterfaceMap 源 用户id -->接口id 集合
     * @return 将其转换成用户昵称和 对应的接口 code码的形式
     */
    private Map<ExtCustomer, List<String>> convertCustomerMap(Map<Integer, List<ExtCustomerJob>> customerInterfaceMap) {
        //1. 先查询出所有的用户
        List<ExtCustomer> extCustomerDoList = extCustomerService.findAll();
        //转换成map
        Map<Integer, ExtCustomer> extCustomerDoMap = extCustomerDoList.stream()
                .collect(Collectors.toMap(ExtCustomer::getId, n -> n));
        //2. 查询出接口
        List<ExtInterfaceDo> extInterfaceDoList = extInterfaceDomainService.list();
        //转换成map
        Map<Integer, ExtInterfaceDo> extInterfaceDoMap = extInterfaceDoList.stream()
                .collect(Collectors.toMap(ExtInterfaceDo::getId, n -> n));


        Map<ExtCustomer, List<String>> result = new HashMap<>(customerInterfaceMap.size());
        for (Map.Entry<Integer, List<ExtCustomerJob>> entry : customerInterfaceMap.entrySet()) {
            Integer customerId = entry.getKey();
            List<Integer> interfaceIdList = entry.getValue().stream().map(ExtCustomerJob::getExtInterfaceId).collect(Collectors.toList());
            //客户信息
            ExtCustomer extCustomerDo = extCustomerDoMap.get(customerId);
            // 查询出对应的编码信息
            result.put(extCustomerDo, extInterfaceIdToCode(interfaceIdList, extInterfaceDoMap));
        }
        return result;
    }

    private List<String> extInterfaceIdToCode(List<Integer> integerIdList,
                                              Map<Integer, ExtInterfaceDo> extInterfaceDoMap) {
        //接下来，进行处理
        List<String> result = new ArrayList<>();
        for (Integer interfaceId : integerIdList) {
            ExtInterfaceDo tempDo = extInterfaceDoMap.get(interfaceId);
            if (null == tempDo) {
                continue;
            }
            ExtInterfaceTemplateType tempType = ExtInterfaceTemplateType.getInterfaceType(tempDo.getCode());
            if (null == tempType) {
                continue;
            }
            result.add(tempType.getCode());
        }
        return result;
    }

    private Map<String, String> getResponse(List<ExtInterfaceTemplateType> extJobInfoTypeList) {
        Map<String, String> responseMap = Collections.synchronizedMap(new HashMap<>(extJobInfoTypeList.size()));
        CountDownLatch countDownLatch = new CountDownLatch(extJobInfoTypeList.size());
        // 股票信息展示
        for (ExtInterfaceTemplateType templateType : extJobInfoTypeList) {
            executor.submit(
                    () -> {
                        try {
                            Map<String, String> typeMap = extTemplateTypeService.getResponseByType(templateType);
                            responseMap.putAll(typeMap);
                        } catch (Exception e) {

                        } finally {
                            countDownLatch.countDown();
                        }
                    }
            );
        }
        MyDateUtil.await(countDownLatch, 8, TimeUnit.SECONDS);
        return responseMap;
    }

    /**
     * 获取对应的列表信息
     *
     * @param interfaceIdList 接口id集合
     * @return 返回对应的列表信息
     */
    private List<ExtInterfaceTemplateType> convertByIdList(List<Integer> interfaceIdList) {
        List<ExtInterfaceDo> extInterfaceDoList = extInterfaceDomainService.list();
        //转换成map
        Map<Integer, ExtInterfaceDo> extInterfaceDoMap = extInterfaceDoList.stream()
                .collect(Collectors.toMap(ExtInterfaceDo::getId, n -> n));
        //接下来，进行处理
        List<ExtInterfaceTemplateType> result = new ArrayList<>();
        for (Integer interfaceId : interfaceIdList) {
            ExtInterfaceDo tempDo = extInterfaceDoMap.get(interfaceId);
            if (null == tempDo) {
                continue;
            }
            ExtInterfaceTemplateType tempType = ExtInterfaceTemplateType.getInterfaceType(tempDo.getCode());
            if (null == tempType) {
                continue;
            }
            result.add(tempType);
        }
        return result;
    }

    @Override
    public void execFasting(JobInfoType jobInfoType) {
        if (jobInfoType == null) {
            return ;
        }
        //查询任务
        JobInfoDto jobInfoDto = jobInfoService.getByCode(jobInfoType);
        if (jobInfoDto == null) {
            log.info("当前任务 {} 已经被删除", jobInfoType.getDesc());
            return ;
        }
        // 禁用状态，不执行。
        if (!DataFlagType.NORMAL.getCode().equals(jobInfoDto.getTriggerStatus())) {
            log.info(">>当前任务 {}是禁用状态，不执行", jobInfoType.getDesc());
            return ;
        }
        //当前的年
        //查询一下，当前任务所关联的用户对象列表.
        List<ExtCustomerJob> extCustomerJobList = extCustomerJobService.listByJobId(jobInfoDto.getId());
        if (CollectionUtils.isEmpty(extCustomerJobList)) {
            log.info(">>>>未查询出关联的用户");
            return ;
        }
        //获取所有的接口，并进行去重
        List<Integer> interfaceIdList = extCustomerJobList.stream()
                .map(ExtCustomerJob::getExtInterfaceId)
                .distinct().collect(Collectors.toList());
        // 只获取第一个接口即可.
        Integer jobRelationInterfaceId = interfaceIdList.get(0);
        String result =  extFastingBusiness.fastingJob(jobRelationInterfaceId);
        //不为空的话，注意发送消息
        if (!StringUtils.isEmpty(result)) {
            List<Integer> extCustomerIdList = extCustomerJobList.stream()
                    .map(ExtCustomerJob::getExtCustomerId)
                    .distinct().collect(Collectors.toList());
            //查询对应的客户信息
            //1. 先查询出所有的用户
            List<ExtCustomer> extCustomerDoList = extCustomerService.findAll();
            //转换成map
            Map<Integer, ExtCustomer> extCustomerDoMap = extCustomerDoList.stream()
                    .collect(Collectors.toMap(ExtCustomer::getId, n -> n));
            for (Integer customerId : extCustomerIdList) {
                ExtCustomer extCustomer = extCustomerDoMap.get(customerId);
                if (extCustomer != null) {
                    String content = "你好," + extCustomer.getName() + System.lineSeparator();
                    if (StringUtils.hasText(extCustomer.getWxId())){
                        MessageInfo messageInfo = MessageInfo.buildBySignId(extCustomer.getWxId(), content + result, TopicType.EXTENDS);
                        PublishMessageHelper.sendWxMessage(this, messageInfo);
                    }
                }
            }
        }
    }
}
