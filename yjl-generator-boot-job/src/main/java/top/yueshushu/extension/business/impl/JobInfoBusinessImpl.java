package top.yueshushu.extension.business.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.common.utils.ResultCode;
import top.yueshushu.extension.business.JobInfoBusiness;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.extension.model.assembler.JobInfoAssembler;
import top.yueshushu.extension.model.dto.JobInfoDto;
import top.yueshushu.extension.model.ro.JobInfoRo;
import top.yueshushu.extension.model.ro.OperateJobInfo;
import top.yueshushu.extension.service.JobInfoService;
import top.yueshushu.message.weixin.service.WeChatService;

import javax.annotation.Resource;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2022-06-02
 */
@Service
@Slf4j
public class JobInfoBusinessImpl implements JobInfoBusiness {
    public static final String JOB_PARAM_SPLIT = "\\,";
    @Resource
    private JobInfoService jobInfoService;
    @Resource
    private JobInfoAssembler jobInfoAssembler;

    private LocalTime STOCK_PRICE_START_TIME = LocalTime.parse("14:59:49");
    private LocalTime STOCK_PRICE_END_TIME = LocalTime.parse("15:01:00");

    private LocalTime STOCK_MORNING_START_TIME = LocalTime.parse("11:29:49");
    private LocalTime STOCK_MORNING_END_TIME = LocalTime.parse("11:31:00");

    @Override
    public OutputResult listJob(JobInfoRo jobInfoRo) {
        return jobInfoService.pageJob(jobInfoRo);
    }

    @Override
    public OutputResult changeStatus(Integer id, DataFlagType dataFlagType) {
        JobInfoDo jobInfoDo = jobInfoService.changeStatus(id, dataFlagType).getData();
        if (null == jobInfoDo) {
            return OutputResult.buildAlert(ResultCode.JOB_ID_NOT_EXIST);
        }

        return OutputResult.buildSucc();
    }


    @Override
    public OutputResult deleteById(Integer id) {
        //立即执行
        JobInfoDto job = jobInfoService.getById(id);
        if (null == job) {
            return OutputResult.buildAlert(ResultCode.JOB_ID_NOT_EXIST);
        }
        jobInfoService.deleteById(id);
        return OutputResult.buildSucc();
    }

    @Override
    public OutputResult handlerById(Integer id) {
        //立即执行
        JobInfoDto job = jobInfoService.getById(id);

        jobInfoService.handlerById(id);
        return OutputResult.buildSucc();
    }

    @Override
    public OutputResult changeCron(Integer id, String cron) {

        JobInfoDto job = jobInfoService.getById(id);
        if (null == job) {
            return OutputResult.buildAlert(ResultCode.JOB_ID_NOT_EXIST);
        }
        // 更新 cron 信息
        JobInfoDto jobInfoDto = new JobInfoDto();
        jobInfoDto.setId(id);
        jobInfoDto.setCron(cron);
        jobInfoService.updateInfoById(jobInfoDto);

        // 清理缓存信息
         return OutputResult.buildSucc();
    }

    @Override
    public OutputResult createInfo(OperateJobInfo operateJobInfo) {
        JobInfoDto jobInfoDto = jobInfoAssembler.operateToInfo(operateJobInfo);
        jobInfoDto.setTriggerType(0);
        jobInfoDto.setTriggerStatus(0);

        return jobInfoService.createInfo(jobInfoDto);
    }

    @Override
    public OutputResult updateInfo(OperateJobInfo operateJobInfo) {
        JobInfoDto job = jobInfoService.getById(operateJobInfo.getId());
        if (null == job) {
            return OutputResult.buildAlert(ResultCode.JOB_ID_NOT_EXIST);
        }
        // 清理缓存信息
        JobInfoDto jobInfoDto = jobInfoAssembler.operateToInfo(operateJobInfo);
        jobInfoService.updateInfoById(jobInfoDto);
        return OutputResult.buildSucc();
    }

    private void sleepTime(int milliSeconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliSeconds);
        } catch (Exception e) {

        }
    }

    /**
     * 是否是股票最后的特殊时间，
     * 即 14:59 到  15:01 期间
     */
    private boolean isEndStockPriceTime() {
        // 进行延迟， 如果时间在 14:59 之后，则睡眠 1分钟。
        LocalTime now = LocalTime.now();
        if (now.isAfter(STOCK_PRICE_START_TIME) && now.isBefore(STOCK_PRICE_END_TIME)) {
            return true;
        }
        return false;
    }

    /**
     * 是否是股票最后的特殊时间，
     * 即 11:29 到  11:31 期间
     */
    private boolean isEndStockMorningTime() {
        // 进行延迟， 如果时间在 14:59 之后，则睡眠 1分钟。
        LocalTime now = LocalTime.now();
        if (now.isAfter(STOCK_MORNING_START_TIME) && now.isBefore(STOCK_MORNING_END_TIME)) {
            return true;
        }
        return false;
    }
}
