package top.yueshushu.extension.common;

/**
 *  缓存基础Key
 * @author yuejianli
 * @date 2023/11/26 11:09
 **/
public interface BaseKey {
    String CACHE_PUBLIC_KEY_PREFIX = "stock_public:";
    String CACHE_PRIVATE_KEY_PREFIX = "stock_private:";
}
