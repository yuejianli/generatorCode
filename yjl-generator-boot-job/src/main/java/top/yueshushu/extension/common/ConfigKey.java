package top.yueshushu.extension.common;

import java.util.Arrays;
import java.util.List;

/**
 *  配置相关的Key
 * @author yuejianli
 * @date 2023/11/26 11:14
 **/
public interface ConfigKey extends BaseKey{
    String EXT_FASTING = CACHE_PUBLIC_KEY_PREFIX + "extFasting";

    String CACHE_WE_CHAT = CACHE_PUBLIC_KEY_PREFIX + "wechat";
    // 对消息是否发送进行配置
    String STOCK_MESSAGE_WX = CACHE_PRIVATE_KEY_PREFIX + "message:wx";
    String STOCK_MESSAGE_DD = CACHE_PRIVATE_KEY_PREFIX + "message:dd";


    String HTTP_HEADER_NAME = "Authorization";
    String X_REAL_IP = "x-real-ip";
    int TOKEN_EXPIRE_TIME = 7 * 24 * 3600;
    int STOCK_PRICE_EXPIRE_TIME = 3 * 24 * 3600;
    int STOCK_TODAY_PRICE_EXPIRE_TIME = 7 * 3600;
    int JOB_CRON_EXPIRE_TIME = 7 * 24 * 3600;

    int YZM_TIME = 60;
    //采用的是 md5 加密
    String FOR_ALL_TIME_TOKEN = "b91f1d0a2849df8430e13bd12f80287a";
    String FOR_ALL_TIME_TOKEN_TEST = "1b8947a6f2b048c284515d8bd209c83f";

    /**
     * 定义日期的格式
     */
    String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
    String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String STOCK_DATE_FORMAT = "yyyyMMdd";

    String ASYNC_SERVICE_EXECUTOR_BEAN_NAME = "asyncServiceExecutor";

    String SIMPLE_ASYNC_SERVICE_EXECUTOR_BEAN_NAME = "simpleAsyncServiceExecutor";

    String JOB_ASYNC_SERVICE_EXECUTOR_BEAN_NAME = "jobAsyncServiceExecutor";

    String WENCAI_ASYNC_SERVICE_EXECUTOR_BEAN_NAME = "wenCaiAsyncServiceExecutor";


    String HOLIDAY_CALENDAR_CACHE = CACHE_PUBLIC_KEY_PREFIX + "holiday_calendar:";
    /**
     * 使用的用户ID
     */
    String KEY_AUTH_USER_ID = "use_user_id";
    String DATE_WORKING = "work";
    String JOB_INFO_TYPE = "jobInfoType:";
    String RSA_PUBLIC_KEY = "publicKeyString";
    String RSA_PRIVATE_KEY = "privateKeyString";
    /**
     * 定义一些缓存处理
     */

    String JOB_BEAN_METHOD_KEY = CACHE_PUBLIC_KEY_PREFIX + "beanMethod";
    String JOB_INFO = CACHE_PUBLIC_KEY_PREFIX + "jobInfo:";
    String RULE_CONDITION = CACHE_PUBLIC_KEY_PREFIX + "rule_condition";
    String CONFIG = CACHE_PRIVATE_KEY_PREFIX + "config";

    String Jasypt_KEY = "enc_safe";

    List<String> IgnoreList = Arrays.asList("class", "userId", "method");
    String JOB_PARAM_SPLIT = "\\,";



}
