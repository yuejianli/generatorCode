package top.yueshushu.extension.component.event;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateUtil;
import lombok.Builder;
import lombok.Data;
import top.yueshushu.extension.enums.TopicType;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2023-04-14
 */
@Data
@Builder
public class MessageInfo implements Serializable {
    private Integer userId;
    private String message;
    private String signId;
    private String timestamp;
    private Integer topic;
    private Boolean show;
    private Boolean send;

    public static List<TopicType> sendTypeList = ListUtil.toList(
            TopicType.CHOOSE,
            TopicType.LOGIN,
            TopicType.TRADE,
            TopicType.DB,
            TopicType.EXTENDS,
            TopicType.HQ

    );

    public static MessageInfo buildByUserId(Integer userId, String message, TopicType topicType) {

        boolean send = true;

        return MessageInfo.builder()
                .userId(userId)
                .message(message)
                .topic(topicType.getCode())
                .send(send)
                .build();
    }

    public static MessageInfo buildBySignId(String signId, String message, TopicType topicType) {
        boolean send = true;
        return MessageInfo.builder()
                .signId(signId)
                .message(message)
                .topic(topicType.getCode())
                .send(send)
                .build();
    }


    public String getTimestamp() {
        return DateUtil.now();
    }

    public Boolean getShow() {
        return Optional.ofNullable(this.show).orElse(true);
    }

    public Boolean getSend() {
        return Optional.ofNullable(this.send).orElse(true);
    }

    public Integer getTopic() {
        return Optional.ofNullable(this.topic).orElse(TopicType.ALL.getCode());
    }
}
