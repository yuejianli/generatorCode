package top.yueshushu.extension.component.event;

import org.springframework.context.ApplicationEvent;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2023-04-13
 */

public class WxMessageEvent extends ApplicationEvent {
    private MessageInfo messageInfo;
    public WxMessageEvent(Object source, MessageInfo messageInfo) {
        super(source);
        this.messageInfo = messageInfo;
    }

    public WxMessageEvent(Object source) {
        super(source);
    }

    public MessageInfo getMessageInfo() {
        return messageInfo;
    }
}
