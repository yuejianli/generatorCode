package top.yueshushu.extension.component.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.extension.domain.ExtCustomerDo;
import top.yueshushu.extension.domainservice.ExtCustomerDomainService;
import top.yueshushu.message.weixin.service.WeChatService;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2023-04-13
 */
@Slf4j
@Component
public class WxMessageListener implements ApplicationListener<WxMessageEvent> {
    @Resource
    private WeChatService weChatService;
    @Resource
    private ExtCustomerDomainService extCustomerDomainService;
    @Override
    public void onApplicationEvent(WxMessageEvent event) {
        // 查询出所有的用户
        List<Integer> userIdList;
        MessageInfo messageInfo = event.getMessageInfo();

        if ( StringUtils.hasText(messageInfo.getSignId())){
            weChatService.sendTextMessageBySign(messageInfo.getSignId(), messageInfo.getMessage());
            return ;
        }
        Integer userId = messageInfo.getUserId();
        if (null == userId) {
            userIdList = extCustomerDomainService.listUseUserIds();
        } else {
            userIdList = Collections.singletonList(userId);
        }
        if (CollectionUtils.isEmpty(userIdList)) {
            return ;
        }
        for (Integer tempUserId : userIdList) {
            ExtCustomerDo userDo = extCustomerDomainService.getById(tempUserId);
            if (null == userDo) {
                return ;
            }
            String wxUserId = userDo.getWxId();
            if (!StringUtils.hasText(wxUserId)) {
                continue;
            }
            weChatService.sendTextMessageBySign(wxUserId, messageInfo.getMessage());
        }
    }
}
