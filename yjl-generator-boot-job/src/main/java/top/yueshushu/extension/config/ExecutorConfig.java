package top.yueshushu.extension.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import top.yueshushu.extension.common.ConfigKey;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
/**
 * @author yuejianli
 * 线程池配置
 */
@Slf4j
@Configuration
@EnableAsync
public class ExecutorConfig {

    @Value("${async.executor.thread.core_pool_size:20}")
    private int corePoolSize;
    @Value("${async.executor.thread.max_pool_size:20}")
    private int maxPoolSize;
    @Value("${async.executor.thread.queue_capacity:999}")
    private int queueCapacity;
    @Value("${async.executor.thread.name.prefix:stock-async-service-}")
    private String namePrefix;

    /**
     * 主要异步线程池
     */
    @Bean(name = ConfigKey.ASYNC_SERVICE_EXECUTOR_BEAN_NAME)
    public Executor asyncServiceExecutor() {
        log.info("start asyncServiceExecutor");
        return buildExecutor(corePoolSize, maxPoolSize, queueCapacity, namePrefix, Thread.NORM_PRIORITY);
    }

    /**
     * 简单的异步线程池
     */
    @Bean(name = ConfigKey.SIMPLE_ASYNC_SERVICE_EXECUTOR_BEAN_NAME)
    public Executor simpleAsyncServiceExecutor() {
        log.info("start simpleServiceExecutor");
        return buildExecutor(5, 5, 5, ConfigKey.SIMPLE_ASYNC_SERVICE_EXECUTOR_BEAN_NAME, Thread.NORM_PRIORITY);
    }

    @Bean(name = ConfigKey.JOB_ASYNC_SERVICE_EXECUTOR_BEAN_NAME)
    public Executor jobAsyncServiceExecutor() {
        log.info("start jobServiceExecutor");
        return buildExecutor(5, 5, 5, ConfigKey.JOB_ASYNC_SERVICE_EXECUTOR_BEAN_NAME, Thread.MAX_PRIORITY);
    }

    @Bean(name = ConfigKey.WENCAI_ASYNC_SERVICE_EXECUTOR_BEAN_NAME)
    public Executor wenCaiAsyncServiceExecutor() {
        log.info("start wenCaiAsyncServiceExecutor");
        return buildExecutor(2, 2, 0, ConfigKey.JOB_ASYNC_SERVICE_EXECUTOR_BEAN_NAME, Thread.MAX_PRIORITY);
    }

    private Executor buildExecutor(int corePoolSize, int maxPoolSize, int queueCapacity, String namePrefix, int threadPriority) {

        log.info("namePrefix:{}, corePoolSize:{}, maxPoolSize:{}, queueCapacity:{}",
                namePrefix, corePoolSize, maxPoolSize, queueCapacity);

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //配置核心线程数
        executor.setCorePoolSize(corePoolSize);
        //配置最大线程数
        executor.setMaxPoolSize(maxPoolSize);
        //配置队列大小
        executor.setQueueCapacity(queueCapacity);
        //配置线程池中的线程的名称前缀
        executor.setThreadNamePrefix(namePrefix);
        executor.setThreadPriority(threadPriority);

        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //执行初始化
        executor.initialize();
        return executor;
    }
}
