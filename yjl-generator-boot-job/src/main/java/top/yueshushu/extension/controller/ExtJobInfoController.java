package top.yueshushu.extension.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.common.utils.ResultCode;
import top.yueshushu.extension.business.JobInfoBusiness;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.extension.model.ro.ExtJobInfoRo;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 扩展定时任务处理
 * </p>
 *
 * @author 岳建立
 * @date 2022-01-02
 */
@RestController
@RequestMapping("/extjobInfo")
@Api("扩展定时任务处理")
@ApiIgnore
public class ExtJobInfoController {
    @Resource
    private JobInfoBusiness jobInfoBusiness;

    @PostMapping("/list")
    @ApiOperation("查询任务信息")
    public OutputResult list(@RequestBody ExtJobInfoRo extJobInfoRo) {

        List<String> codeList = Arrays.asList(JobInfoType.MORNING.getCode(),
                JobInfoType.NIGHT.getCode(),JobInfoType.FASTING.getCode());
        extJobInfoRo.setCodes(codeList);
        return jobInfoBusiness.listJob(extJobInfoRo);
    }

    @PostMapping("/disable")
    @ApiOperation("禁用")
    public OutputResult disable(@RequestBody ExtJobInfoRo extJobInfoRo) {
        if (extJobInfoRo.getId() == null) {
            return OutputResult.buildAlert(ResultCode.ID_IS_EMPTY);
        }
        return jobInfoBusiness.changeStatus(extJobInfoRo.getId(), DataFlagType.DELETE);
    }

    @PostMapping("/enable")
    @ApiOperation("启用")
    public OutputResult enable(@RequestBody ExtJobInfoRo extJobInfoRo) {
        if (extJobInfoRo.getId() == null) {
            return OutputResult.buildAlert(ResultCode.ID_IS_EMPTY);
        }
        return jobInfoBusiness.changeStatus(extJobInfoRo.getId(), DataFlagType.NORMAL);
    }

    @PostMapping("/delete")
    @ApiOperation("删除定时任务")
    public OutputResult delete(@RequestBody ExtJobInfoRo extJobInfoRo) {
        if (extJobInfoRo.getId() == null) {
            return OutputResult.buildAlert(ResultCode.ID_IS_EMPTY);
        }
        return jobInfoBusiness.deleteById(extJobInfoRo.getId());
    }

    @PostMapping("/handler")
    @ApiOperation("手动执行定时任务")
    public OutputResult handler(@RequestBody ExtJobInfoRo extJobInfoRo) {
        if (extJobInfoRo.getId() == null) {
            return OutputResult.buildAlert(ResultCode.ID_IS_EMPTY);
        }
        return jobInfoBusiness.handlerById(extJobInfoRo.getId());
    }
}
