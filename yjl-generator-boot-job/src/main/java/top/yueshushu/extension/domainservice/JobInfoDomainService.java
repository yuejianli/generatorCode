package top.yueshushu.extension.domainservice;


import com.baomidou.mybatisplus.extension.service.IService;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.model.queryparam.JobInfoQueryParam;

import java.util.List;

/**
 *  定时任务的操作
 * @author yuejianli
 * @date 2022/06/02 23:23
 **/
public interface JobInfoDomainService extends IService<JobInfoDo> {
    /**
     * 根据条件查询多个记录
     * @param jobInfoQueryParam 查询条件
     * @return 任务信息
     */
    List<JobInfoDo> listByCondition(JobInfoQueryParam jobInfoQueryParam);

    /**
     * 根据条件查询单个记录
     * @param jobInfoQueryParam 查询条件
     * @return 任务信息
     */
    JobInfoDo getByCondition(JobInfoQueryParam jobInfoQueryParam);
}
