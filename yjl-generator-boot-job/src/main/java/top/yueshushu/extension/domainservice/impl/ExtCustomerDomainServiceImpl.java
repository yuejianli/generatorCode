package top.yueshushu.extension.domainservice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yueshushu.extension.domain.ExtCustomerDo;
import top.yueshushu.extension.domainservice.ExtCustomerDomainService;
import top.yueshushu.extension.mapper.ExtCustomerDoMapper;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2022-06-09
 */
@Service
public class ExtCustomerDomainServiceImpl extends ServiceImpl<ExtCustomerDoMapper, ExtCustomerDo>
        implements ExtCustomerDomainService {
    @Resource
    private ExtCustomerDoMapper extCustomerDoMapper;

    @Override
    public List<ExtCustomerDo> listByKeyword(String keyword) {
        return this.lambdaQuery().list();
    }

    @Override
    public ExtCustomerDo getByAccount(String userAccount) {
        return this.lambdaQuery()
                .eq(ExtCustomerDo::getAccount, userAccount)
                .one();
    }

	@Override
	public List<Integer> listUseUserIds() {
		return this.lambdaQuery().list()
				.stream()
				.map(ExtCustomerDo::getId)
				.collect(Collectors.toList());

	}
}
