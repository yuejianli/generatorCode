package top.yueshushu.extension.domainservice.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.yueshushu.extension.domain.ExtFastingDo;
import top.yueshushu.extension.domainservice.ExtFastingDomainService;
import top.yueshushu.extension.mapper.ExtFastingDoMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 *  斋戒日期的处理
 * @author yuejianli
 * @date 2022/5/20 23:23
 **/
@Service
@Slf4j
public class ExtFastingDomainServiceImpl extends ServiceImpl<ExtFastingDoMapper, ExtFastingDo>
        implements ExtFastingDomainService {
    @Resource
    private ExtFastingDoMapper extFastingDoMapper;


    @Override
    public ExtFastingDo getByMonthAndDay(Integer month, Integer day, String term) {
        // 进行查询
        // 创建查询构造器
        LambdaQueryWrapper<ExtFastingDo> queryWrapper = new LambdaQueryWrapper<>();

        // 添加条件
        queryWrapper
                .and(month != null && day !=null,
                        wrapper -> wrapper.eq(ExtFastingDo::getFastingMonth, month).eq(ExtFastingDo::getFastingDay, day))
                .or()
                .eq(term != null && !term.isEmpty(), ExtFastingDo::getJieQi, term)
                .orderByAsc(ExtFastingDo::getId);

        // 执行查询并返回结果
        List<ExtFastingDo> extFastingDos = extFastingDoMapper.selectList(queryWrapper);
        if (!CollUtil.isEmpty(extFastingDos)) {
            return extFastingDos.get(0);
        }
        return null;
    }
}
