package top.yueshushu.extension.domainservice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import top.yueshushu.extension.domain.ExtInterfaceDo;
import top.yueshushu.extension.domainservice.ExtInterfaceDomainService;
import top.yueshushu.extension.mapper.ExtInterfaceDoMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 *  jobInfo的处理
 * @author yuejianli
 * @date 2022/5/20 23:23
 **/
@Service
@Slf4j
public class ExtInterfaceDomainServiceImpl extends ServiceImpl<ExtInterfaceDoMapper, ExtInterfaceDo>
        implements ExtInterfaceDomainService {
    @Resource
    private ExtInterfaceDoMapper extInterfaceDoMapper;

    @Override
    public List<ExtInterfaceDo> listByName(String keyword) {
        return this.lambdaQuery()
                .like(
                        !StringUtils.isEmpty(keyword), ExtInterfaceDo::getName, keyword
                ).list();
    }
}
