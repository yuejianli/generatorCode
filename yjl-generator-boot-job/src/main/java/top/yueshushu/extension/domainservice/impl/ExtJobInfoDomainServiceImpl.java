package top.yueshushu.extension.domainservice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.yueshushu.extension.domain.ExtJobInfoDo;
import top.yueshushu.extension.domainservice.ExtJobInfoDomainService;
import top.yueshushu.extension.mapper.ExtJobInfoDoMapper;

import javax.annotation.Resource;

/**
 *  jobInfo的处理
 * @author yuejianli
 * @date 2022/5/20 23:23
 **/
@Service
@Slf4j
public class ExtJobInfoDomainServiceImpl extends ServiceImpl<ExtJobInfoDoMapper, ExtJobInfoDo>
        implements ExtJobInfoDomainService {
    @Resource
    private ExtJobInfoDoMapper extJobInfoDoMapper;

    @Override
    public ExtJobInfoDo getByCode(String code) {
        return this.lambdaQuery()
                .eq(ExtJobInfoDo::getCode, code)
                .one();
    }
}
