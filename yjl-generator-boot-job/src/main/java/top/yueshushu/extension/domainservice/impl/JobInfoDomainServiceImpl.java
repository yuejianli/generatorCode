package top.yueshushu.extension.domainservice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.domainservice.JobInfoDomainService;
import top.yueshushu.extension.mapper.JobInfoDoMapper;
import top.yueshushu.extension.model.queryparam.JobInfoQueryParam;

import java.util.List;

/**
 *  jobInfo的处理
 * @author yuejianli
 * @date 2022/5/20 23:23
 **/
@Service
@Slf4j
public class JobInfoDomainServiceImpl extends ServiceImpl<JobInfoDoMapper, JobInfoDo>
        implements JobInfoDomainService {

    @Override
    public List<JobInfoDo> listByCondition(JobInfoQueryParam jobInfoQueryParam) {
        return this.lambdaQuery()
                .eq(StringUtils.hasText(jobInfoQueryParam.getCode()), JobInfoDo::getCode, jobInfoQueryParam.getCode())
                .eq(StringUtils.hasText(jobInfoQueryParam.getBeanName()), JobInfoDo::getBeanName, jobInfoQueryParam.getBeanName())
                .eq(StringUtils.hasText(jobInfoQueryParam.getMethodName()), JobInfoDo::getMethodName, jobInfoQueryParam.getMethodName())
                .eq(StringUtils.hasText(jobInfoQueryParam.getParams()), JobInfoDo::getParam, jobInfoQueryParam.getParams())
                .eq(jobInfoQueryParam.getTriggerStatus()!= null, JobInfoDo::getTriggerStatus, jobInfoQueryParam.getTriggerStatus())
                .in(!CollectionUtils.isEmpty(jobInfoQueryParam.getCodeList()), JobInfoDo::getCode, jobInfoQueryParam.getCodeList())
                .list();

    }

    @Override
    public JobInfoDo getByCondition(JobInfoQueryParam jobInfoQueryParam) {
        List<JobInfoDo> tempResultList = listByCondition(jobInfoQueryParam);
        if (CollectionUtils.isEmpty(tempResultList)) {
            return null;
        } else {
            return tempResultList.get(0);
        }
    }
}
