package top.yueshushu.extension.enums;

/**
 * 数据信息
 *
 * @author 两个蝴蝶飞
 */
public enum TopicType {
    ALL(0, "全部"),
    /**
     * 正常
     */
    YD(1, "异动"),
    /**
     * 删除
     */
    LOGIN(2, "登录失效"),
    QS(3, "强势"),
    DB(4, "打板"),
    HQ(5, "行情"),
    TRADE(6, "交易信息"),
    CHOOSE(7, "策略选股"),
    ERROR(8, "错误异常"),
    NEW(9, "申购"),
    EXTENDS(10, "扩展"),
    STOCK_UPDATE(11, "股票变量"),
    INDEX(12, "指标信息"),
    ALERT(13, "提示信息"),
    KZZ(14, "可转债信息"),

    ;

    private Integer code;

    private String desc;

    private TopicType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public static TopicType getByCode(Integer code) {
        for (TopicType topicType : TopicType.values()) {
            if (topicType.code.equals(code)) {
                return topicType;
            }
        }
        return ALL;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
