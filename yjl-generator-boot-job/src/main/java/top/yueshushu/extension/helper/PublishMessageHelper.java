package top.yueshushu.extension.helper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.spring.SpringUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Component;
import top.yueshushu.extension.component.event.MessageInfo;
import top.yueshushu.extension.component.event.WxMessageEvent;
import top.yueshushu.extension.util.RedisUtil;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 发布消息
 *
 * @author yuejianli
 * @date 2023-04-14
 */
@Component("publishMessageHelper")
public class PublishMessageHelper {
    private static final ApplicationContext applicationContext = SpringUtil.getApplicationContext();
    /**
     * 发送微信消息
     *
     * @param source 消息源对象
     * @param messageInfo 消息信息对象，包含要发送的消息内容、是否发送、是否显示等属性
     */
    public static void sendWxMessage(Object source, MessageInfo messageInfo) {
        applicationContext.publishEvent(new WxMessageEvent(source, messageInfo));
    }
}
