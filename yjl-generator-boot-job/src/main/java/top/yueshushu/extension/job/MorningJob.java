package top.yueshushu.extension.job;

import org.springframework.stereotype.Component;
import top.yueshushu.extension.business.ExtJobInfoBusiness;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.job.BaseJob;

import javax.annotation.Resource;

/**
 * 早安定时任务
 *
 * @author yuejianli
 * @date 2023-04-20
 */
@Component("morningJob")
public class MorningJob implements BaseJob {
    @Resource
    private ExtJobInfoBusiness extJobInfoBusiness;
    @Override
    public void execute(String param) {
        extJobInfoBusiness.execDay(JobInfoType.MORNING);
    }
}
