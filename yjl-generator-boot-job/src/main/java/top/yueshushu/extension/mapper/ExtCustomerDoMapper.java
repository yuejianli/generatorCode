package top.yueshushu.extension.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yueshushu.extension.domain.ExtCustomerDo;

import java.util.List;

/**
 * <p>
 * 扩展用户
 * </p>
 *
 * @author 岳建立  自定义的
 * @since 2022-01-02
 */
public interface ExtCustomerDoMapper extends BaseMapper<ExtCustomerDo> {
}
