package top.yueshushu.extension.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.yueshushu.extension.domain.ExtFastingDo;

/**
 * <p>
 * 斋戒日期
 * </p>
 *
 * @author 岳建立  自定义的
 * @since 2022-01-02
 */
public interface ExtFastingDoMapper extends BaseMapper<ExtFastingDo> {
}
