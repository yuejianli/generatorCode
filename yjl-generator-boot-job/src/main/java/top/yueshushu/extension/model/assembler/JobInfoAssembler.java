package top.yueshushu.extension.model.assembler;

import org.mapstruct.Mapper;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.model.dto.JobInfoDto;
import top.yueshushu.extension.model.ro.OperateJobInfo;
import top.yueshushu.extension.model.vo.JobInfoVo;

/**
 * 定时任务 Job 转换器
 * @author  yuejianli
 * @date 2022/5/20 23:01
 **/
@Mapper(componentModel = "spring")
public interface JobInfoAssembler {
    /**
     * 任务 domain 转换成实体entity
     *
     * @param jobInfoDo 任务Do
     * @return 任务 domain 转换成实体entity
     */
    JobInfoDto doToEntity(JobInfoDo jobInfoDo);

    /**
     * 任务 entity 转换成 domain
     *
     * @param jobInfoDto 任务
     * @return 任务 entity 转换成 domain
     */
    JobInfoDo entityToDo(JobInfoDto jobInfoDto);

    /**
     * 任务 entity 转换成 vo
     *
     * @param jobInfoDto 任务
     * @return 任务 entity 转换成 vo
     */
    JobInfoVo entityToVo(JobInfoDto jobInfoDto);

    /**
     * 操作任务Job 转换成 JobInfo
     * @param operateJobInfo 操作任务Job
     * @return 转换成 JobInfo
     */
    JobInfoDto operateToInfo(OperateJobInfo operateJobInfo);
}
