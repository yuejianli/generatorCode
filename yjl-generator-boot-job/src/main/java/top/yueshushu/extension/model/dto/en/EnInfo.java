package top.yueshushu.extension.model.dto.en;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 英语相关的
 * </p>
 *
 * @author yuejianli
 * @since 2023-11-07 9:01
 */
@Data
public class EnInfo implements Serializable {
    /**
     * 英文
     */
    private String en;
    /**
     * 中文
     */
    private String zh;
}
