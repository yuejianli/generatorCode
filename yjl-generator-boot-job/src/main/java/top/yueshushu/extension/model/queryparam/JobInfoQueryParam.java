package top.yueshushu.extension.model.queryparam;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description TODO
 * @Author yuejianli
 * @Date 2023/12/16 11:20
 **/
@Data
public class JobInfoQueryParam implements Serializable {
	private String code;
	private String beanName;
	private String methodName;
	private String params;
	private List<String> codeList;
	private Integer triggerStatus;
}
