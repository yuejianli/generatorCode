package top.yueshushu.extension.model.ro;

import lombok.Data;

import java.io.Serializable;

/**
 *  扩展任务Ro
 * @author yuejianli
 * @date 2022/6/11 15:20
 **/
@Data
public class ExtJobInfoRo extends JobInfoRo implements Serializable {

}
