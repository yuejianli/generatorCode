package top.yueshushu.extension.model.ro;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 定时任务信息处理
 * </p>
 *
 * @author 岳建立 自定义的
 * @since 2022-01-02
 */
@Data
public class OperateJobInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id自增
     */
    private Integer id;

    /**
     * 任务编码
     */
    private String code;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务描述
     */
    private String description;
    /**
     * bean名称
     */
    private String beanName;
    /**
     * 方法名称
     */
    private String methodName;
    /**
     * 任务参数
     */
    private String param;
    /**
     * 创建者
     */
    private String author;
    /**
     * 任务cron表达式
     */
    private String cron;

}
