package top.yueshushu.extension.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2023-08-09
 */
@Data
@ApiModel("斋戒日期展示")
public class ExtFastingPdfVo implements Serializable {
    @ApiModelProperty("斋戒日期")
    private String date;
    @ApiModelProperty("对应信息")
    private String message;
    @ApiModelProperty("是否是斋戒日期 1为是 0为否")
    private Integer show;
}
