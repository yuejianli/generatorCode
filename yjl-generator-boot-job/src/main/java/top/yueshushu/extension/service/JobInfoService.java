package top.yueshushu.extension.service;

import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.extension.model.dto.JobInfoDto;
import top.yueshushu.extension.model.ro.JobInfoRo;

import java.util.List;

/**
 * 定时任务的服务
 *
 * @author Yue Jianli
 * @date 2022-06-02
 */

public interface JobInfoService {
    /**
     * 分页查询任务信息
     *
     * @param jobInfoRo
     * @return top.yueshushu.learn.common.response.OutputResult
     * @date 2022/6/2 11:52
     * @author yuejianli
     */
    OutputResult pageJob(JobInfoRo jobInfoRo);

    /**
     * 改变任务的状态
     *
     * @param id 任务编号
     * @param dataFlagType 状态
     * @return top.yueshushu.learn.common.response.OutputResult
     */
    OutputResult<JobInfoDo> changeStatus(Integer id, DataFlagType dataFlagType);

    /**
     * 根据编码，获取相应的任务信息
     *
     * @param jobInfoType 任务类型
     * @return 根据编码，获取相应的任务信息
     */
    JobInfoDto getByCode(JobInfoType jobInfoType);

    /**
     * 更新定时任务
     * @param jobInfoDto 定时任务对象
     * @return 更新定时任务
     */
    OutputResult updateInfoById(JobInfoDto jobInfoDto);

    /**
     * 执行一次定时任务
     * @param id id任务值
     *
     */
    void handlerById(Integer id);

    /**
     * 删除定时任务
     *
     * @param id 定时任务编号
     * @return 删除定时任务
     */
    OutputResult deleteById(Integer id);

    /**
     * 根据任务编号，获取对应的任务信息
     *
     * @param id 傻编号
     * @return 根据任务编号，获取对应的任务信息
     */
    JobInfoDto getById(Integer id);

    /**
     * 创建定时任务
     *
     * @param jobInfoDto 定时任务对象
     * @return 创建定时任务
     */
    OutputResult createInfo(JobInfoDto jobInfoDto);


    /**
     * 查询所有的启动中的定时任务
     * @return 查询所有的启动中的定时任务
     */
    List<JobInfoDo> findAllRunJob();
    /**
     * 根据 bean 名称，方法名称， params 查询信息
     * @param beanName bean名称
     * @param methodName 方法名称
     * @param params 参数信息
     * @return 根据 bean 名称，方法名称， params 查询信息
     */
    JobInfoDto getByBeanAndMethodAndParams(String beanName, String methodName, String params);
}
