package top.yueshushu.extension.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.common.utils.PageResponse;
import top.yueshushu.common.utils.ResultCode;
import top.yueshushu.extension.assembler.ExtCustomerAssembler;
import top.yueshushu.extension.common.ConfigKey;
import top.yueshushu.extension.domain.ExtCustomerDo;
import top.yueshushu.extension.domainservice.ExtCustomerDomainService;
import top.yueshushu.extension.entity.ExtCustomer;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.model.ro.ExtCustomerRo;
import top.yueshushu.extension.model.vo.ExtCustomerVo;
import top.yueshushu.extension.service.ExtCustomerService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  TODO
 * @author yuejianli
 * @date 2022/6/11 15:07
 **/
@Service
public class ExtCustomerServiceImpl implements ExtCustomerService {
    @Resource
    private ExtCustomerDomainService extCustomerDomainService;
    @Resource
    private ExtCustomerAssembler extCustomerAssembler;

    @Override
    public OutputResult pageList(ExtCustomerRo extCustomerRo) {
        Page<Object> pageGithubResult = PageHelper.startPage(extCustomerRo.getPageNum(), extCustomerRo.getPageSize());
        List<ExtCustomerDo> extCustomerDOList = extCustomerDomainService.listByKeyword(extCustomerRo.getKeyword());
        if (CollectionUtils.isEmpty(extCustomerDOList)) {
            return OutputResult.buildSucc(
                    PageResponse.emptyPageResponse()
            );
        }

        List<ExtCustomerVo> pageResultList = new ArrayList<>(extCustomerDOList.size());
        extCustomerDOList.forEach(
                n -> {
                    pageResultList.add(extCustomerAssembler.entityToVo(extCustomerAssembler.doToEntity(n)));
                }
        );
        return OutputResult.buildSucc(new PageResponse<>(pageGithubResult.getTotal(), pageResultList));
    }

    @Override
    public OutputResult add(ExtCustomerRo extCustomerRo) {
        // 看账号是否存在
        ExtCustomerDo dbextCustomerDo = extCustomerDomainService.getByAccount(extCustomerRo.getUserAccount());
        if (dbextCustomerDo != null) {
            return OutputResult.buildFail(ResultCode.EXT_ACCOUNT_EXIST);
        }
        ExtCustomer extCustomer = extCustomerAssembler.roToEntity(extCustomerRo);
        extCustomer.setBirthday(DateUtil.parse(extCustomerRo.getBirthdayStr(), ConfigKey.SIMPLE_DATE_FORMAT));
        ExtCustomerDo extCustomerDo = extCustomerAssembler.entityToDo(extCustomer);
        extCustomerDo.setFlag(DataFlagType.NORMAL.getCode());
        extCustomerDomainService.save(extCustomerDo);
        return OutputResult.buildSucc(ResultCode.SUCCESS);
    }

    @Override
    public OutputResult update(ExtCustomerRo extCustomerRo) {
        ExtCustomerDo idDo = extCustomerDomainService.getById(extCustomerRo.getId());
        if (idDo == null) {
            return OutputResult.buildFail(ResultCode.EXT_ID_NOT_EXIST);
        }
        if (!idDo.getAccount().equals(extCustomerRo.getUserAccount())) {
            // 看账号是否存在
            ExtCustomerDo dbextCustomerDo = extCustomerDomainService.getByAccount(extCustomerRo.getUserAccount());
            if (dbextCustomerDo != null) {
                return OutputResult.buildFail(ResultCode.EXT_ACCOUNT_EXIST);
            }
        }
        ExtCustomer extCustomer = extCustomerAssembler.roToEntity(extCustomerRo);
        extCustomer.setBirthday(DateUtil.parse(extCustomerRo.getBirthdayStr(), ConfigKey.SIMPLE_DATE_FORMAT));
        ExtCustomerDo extCustomerDo = extCustomerAssembler.entityToDo(extCustomer);
        extCustomerDo.setFlag(DataFlagType.NORMAL.getCode());
        extCustomerDomainService.updateById(extCustomerDo);
        return OutputResult.buildSucc(ResultCode.SUCCESS);
    }

    @Override
    public OutputResult deleteById(Integer id) {
        ExtCustomerDo idDo = extCustomerDomainService.getById(id);
        if (idDo == null) {
            return OutputResult.buildFail(ResultCode.EXT_ID_NOT_EXIST);
        }
        extCustomerDomainService.removeById(id);
        return OutputResult.buildSucc(ResultCode.SUCCESS);
    }

    @Override
    public List<ExtCustomer> findAll() {
        List<ExtCustomerDo> extCustomerDOList = extCustomerDomainService.listByKeyword(null);
        if (CollectionUtils.isEmpty(extCustomerDOList)) {
            return Collections.emptyList();
        }

        List<ExtCustomer> resultList = new ArrayList<>(extCustomerDOList.size());
        extCustomerDOList.forEach(
                n -> {
                    resultList.add(extCustomerAssembler.doToEntity(n));
                }
        );
        return resultList;
    }
}
