package top.yueshushu.extension.service.impl;

import org.springframework.stereotype.Service;
import top.yueshushu.extension.assembler.ExtFastingAssembler;
import top.yueshushu.extension.domainservice.ExtFastingDomainService;
import top.yueshushu.extension.entity.ExtFasting;
import top.yueshushu.extension.service.ExtFastingService;

import javax.annotation.Resource;

/**
 *  接口应用实现
 * @author yuejianli
 * @date 2022/6/11 15:07
 **/
@Service
public class ExtFastingServiceImpl implements ExtFastingService {
    @Resource
    private ExtFastingDomainService extFastingDomainService;
    @Resource
    private ExtFastingAssembler extFastingAssembler;


    @Override
    public ExtFasting getById(Integer id) {
        return extFastingAssembler.doToEntity(extFastingDomainService.getById(id));
    }

    @Override
    public ExtFasting getByMonthAndDay(int month, int day, String term) {
        return extFastingAssembler.doToEntity(extFastingDomainService.
                getByMonthAndDay(month, day, term));
    }
}
