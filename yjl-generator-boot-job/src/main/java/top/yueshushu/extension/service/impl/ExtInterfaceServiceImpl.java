package top.yueshushu.extension.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.common.utils.PageResponse;
import top.yueshushu.extension.assembler.ExtInterfaceAssembler;
import top.yueshushu.extension.domain.ExtInterfaceDo;
import top.yueshushu.extension.domainservice.ExtInterfaceDomainService;
import top.yueshushu.extension.entity.ExtInterface;
import top.yueshushu.extension.model.ro.ExtInterfaceRo;
import top.yueshushu.extension.service.ExtInterfaceService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 *  接口应用实现
 * @author yuejianli
 * @date 2022/6/11 15:07
 **/
@Service
public class ExtInterfaceServiceImpl implements ExtInterfaceService {
    @Resource
    private ExtInterfaceDomainService extInterfaceDomainService;
    @Resource
    private ExtInterfaceAssembler extInterfaceAssembler;

    @Override
    public OutputResult pageList(ExtInterfaceRo extInterfaceRo) {
        Page<Object> pageGithubResult = PageHelper.startPage(extInterfaceRo.getPageNum(), extInterfaceRo.getPageSize());
        List<ExtInterfaceDo> extInterfaceDoList = extInterfaceDomainService.listByName(extInterfaceRo.getKeyword());
        if (CollectionUtils.isEmpty(extInterfaceDoList)) {
            return OutputResult.buildSucc(
                    PageResponse.emptyPageResponse()
            );
        }

        List<ExtInterface> pageResultList = new ArrayList<>(extInterfaceDoList.size());
        extInterfaceDoList.forEach(
                n -> {
                    pageResultList.add(extInterfaceAssembler.doToEntity(n));
                }
        );
        return OutputResult.buildSucc(new PageResponse<>(pageGithubResult.getTotal(), pageResultList));
    }
}
