package top.yueshushu.extension.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.common.utils.PageResponse;
import top.yueshushu.common.utils.ResultCode;
import top.yueshushu.extension.common.ConfigKey;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.domainservice.JobInfoDomainService;
import top.yueshushu.extension.enums.DataFlagType;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.extension.model.assembler.JobInfoAssembler;
import top.yueshushu.extension.model.dto.JobInfoDto;
import top.yueshushu.extension.model.queryparam.JobInfoQueryParam;
import top.yueshushu.extension.model.ro.JobInfoRo;
import top.yueshushu.extension.model.vo.JobInfoVo;
import top.yueshushu.extension.service.JobInfoService;
import top.yueshushu.extension.util.RedisUtil;
import top.yueshushu.job.CronTaskRegistrar;
import top.yueshushu.job.SchedulingRunnable;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务的service信息
 *
 * @author yuejianli
 * @date 2022-06-02
 */
@Service("jobInfoService")
@Slf4j
public class JobInfoServiceImpl implements JobInfoService {

    @Resource
    private CronTaskRegistrar cronTaskRegistrar;

    @Resource
    private JobInfoDomainService jobInfoDomainService;
    @Resource
    private JobInfoAssembler jobInfoAssembler;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public OutputResult pageJob(JobInfoRo jobInfoRo) {
        Page<Object> pageInfo = PageHelper.startPage(jobInfoRo.getPageNum(), jobInfoRo.getPageSize());

        JobInfoQueryParam jobInfoQueryParam = new JobInfoQueryParam();
        jobInfoQueryParam.setCodeList(jobInfoRo.getCodes());
        List<JobInfoDo> jobInfoList = jobInfoDomainService.listByCondition(jobInfoQueryParam);
        if (CollectionUtils.isEmpty(jobInfoList)) {
            return OutputResult.buildSucc(
                    PageResponse.emptyPageResponse()
            );
        }
        List<JobInfoVo> pageResultList = new ArrayList<>(jobInfoList.size());
        jobInfoList.forEach(
                n -> pageResultList.add(jobInfoAssembler.entityToVo(jobInfoAssembler.doToEntity(n)))
        );
        return OutputResult.buildSucc(new PageResponse<>(pageInfo.getTotal(), pageResultList));

    }

    @Override
    public OutputResult<JobInfoDo> changeStatus(Integer id, DataFlagType dataFlagType) {
        JobInfoDto jobInfoDto = getById(id);
        if (jobInfoDto == null) {
            return OutputResult.buildSucc();
        }

        JobInfoDo jobInfoDo = new JobInfoDo();
        jobInfoDo.setId(id);
        jobInfoDo.setUpdateTime(DateUtil.date());
        jobInfoDo.setTriggerStatus(dataFlagType.getCode());
        jobInfoDo.setCode(jobInfoDto.getCode());
        //进行更新
        jobInfoDomainService.updateById(jobInfoDo);
        changeJobStatus(id);
        return OutputResult.buildSucc(jobInfoDo);
    }


    private void changeJobStatus(Integer id) {
        // 查询修改后的任务信息
        JobInfoDto jobInfoDto = getById(id);
        // 如果状态是1则添加任务
        if (jobInfoDto.getTriggerStatus() == 1) {
            SchedulingRunnable task = new SchedulingRunnable(jobInfoDto.getBeanName(), jobInfoDto.getMethodName(), jobInfoDto.getParam());
            cronTaskRegistrar.addCronTask(task, jobInfoDto.getCron());
        } else {
            // 否则清除任务
            SchedulingRunnable task = new SchedulingRunnable(jobInfoDto.getBeanName(), jobInfoDto.getMethodName(), jobInfoDto.getParam());
            cronTaskRegistrar.removeCronTask(task);
        }
    }

    @Override
    public JobInfoDto getByCode(JobInfoType jobInfoType) {
        if (jobInfoType == null) {
            return null;
        }
        JobInfoQueryParam jobInfoQueryParam = new JobInfoQueryParam();
        jobInfoQueryParam.setCode(jobInfoType.getCode());
        JobInfoDo jobInfoDo = jobInfoDomainService.getByCondition(jobInfoQueryParam);
        return jobInfoAssembler.doToEntity(jobInfoDo);
    }

    @Override
    public OutputResult updateInfoById(JobInfoDto jobInfoDto) {
        JobInfoDto oldJobInfoDto = getById(jobInfoDto.getId());
        if (null == oldJobInfoDto) {
            return OutputResult.buildFail(ResultCode.JOB_ID_NOT_EXIST);
        }
        JobInfoDo dbJobInfoDO = jobInfoAssembler.entityToDo(oldJobInfoDto);

        boolean updateFlag = jobInfoDomainService.updateById(jobInfoAssembler.entityToDo(jobInfoDto));

        // 判断一下， beanName 和 methodName , param, cron 是否发生了改变。 如果未改变，则不需要处理信息.
        boolean updateJob = false;
        if (StringUtils.hasText(jobInfoDto.getBeanName()) && StringUtils.hasText(jobInfoDto.getMethodName()) &&
                StringUtils.hasText(jobInfoDto.getParam()) && StringUtils.hasText(jobInfoDto.getCron())) {
            // 进行处理，  判断是否一致.
            if (getJobUnique(dbJobInfoDO, null).equals(getJobUnique(null, jobInfoDto))) {
                updateJob = true;
            }
        }
        if (updateFlag && updateJob) {
            // 修改成功,则先删除任务器中的任务,并重新添加
            SchedulingRunnable task1 = new SchedulingRunnable(dbJobInfoDO.getBeanName(), dbJobInfoDO.getMethodName(), dbJobInfoDO.getParam());
            cronTaskRegistrar.removeCronTask(task1);
            // 如果修改后的任务状态是1就加入任务器
            if (dbJobInfoDO.getTriggerStatus() == 1) {
                SchedulingRunnable task = new SchedulingRunnable(jobInfoDto.getBeanName(), jobInfoDto.getMethodName(), jobInfoDto.getParam());
                cronTaskRegistrar.addCronTask(task, jobInfoDto.getCron());
            }
        }

        return updateFlag? OutputResult.buildSucc() : OutputResult.buildFail();
    }

    @Override
    public void handlerById(Integer id) {
        JobInfoDto jobInfoDto = getById(id);
        redisUtil.set(ConfigKey.JOB_INFO_TYPE + jobInfoDto.getCode(),true,5, TimeUnit.SECONDS);
        SchedulingRunnable task = new SchedulingRunnable(jobInfoDto.getBeanName(), jobInfoDto.getMethodName(), jobInfoDto.getParam(), 1);
        cronTaskRegistrar.delayCronTask(task, 500);
    }

    private String getJobUnique (JobInfoDo jobInfoDo , JobInfoDto jobInfoDto) {
        if ( jobInfoDo != null) {
            return jobInfoDo.getBeanName()+"," +jobInfoDo.getMethodName() +","+jobInfoDo.getParam()+","+jobInfoDo.getCron();
        }
        if ( jobInfoDto != null) {
            return jobInfoDto.getBeanName()+"," + jobInfoDto.getMethodName() +","+ jobInfoDto.getParam()+","+ jobInfoDto.getCron();
        }
        return "";
    }

    @Override
    public OutputResult createInfo(JobInfoDto jobInfoDto) {
        // 进行查询， 同一个 bean, 同一个方法， 不能同时有.
        JobInfoDto jobInfoDto1 = getByCode(JobInfoType.getJobInfoType(jobInfoDto.getCode()));
        if ( null != jobInfoDto1) {
            return OutputResult.buildFail(ResultCode.JOB_EXISTS);
        }
        JobInfoDo jobInfoDo = jobInfoAssembler.entityToDo(jobInfoDto);
        jobInfoDo.setUpdateTime(DateUtil.date());
        jobInfoDomainService.save(jobInfoDo);
        return  OutputResult.buildSucc();
    }

    @Override
    public OutputResult deleteById(Integer id) {
        JobInfoDto dbJobInfoDto = getById(id);
        if (null == dbJobInfoDto) {
            return OutputResult.buildFail(ResultCode.JOB_ID_NOT_EXIST);
        }

        jobInfoDomainService.removeById(id);
        // 删除成功时要清除定时任务器中的对应任务
        SchedulingRunnable task = new SchedulingRunnable(dbJobInfoDto.getBeanName(), dbJobInfoDto.getMethodName(), dbJobInfoDto.getParam());
        cronTaskRegistrar.removeCronTask(task);

        return OutputResult.buildSucc();
    }

    @Override
    @Cacheable(value = ConfigKey.JOB_INFO, key = "#id")
    public JobInfoDto getById(Integer id) {
        JobInfoDo jobInfoDo = jobInfoDomainService.getById(id);
        return jobInfoAssembler.doToEntity(jobInfoDo);
    }

    @Override
    public List<JobInfoDo> findAllRunJob() {
        JobInfoQueryParam jobInfoQueryParam = new JobInfoQueryParam();
        jobInfoQueryParam.setTriggerStatus(1);
        return jobInfoDomainService.listByCondition(jobInfoQueryParam);
    }

    @Override
    public JobInfoDto getByBeanAndMethodAndParams(String beanName, String methodName, String params) {
        // 定义 beanName 的缓存信息。 基本不在库里面修改。
        String field = beanName + "_" + methodName;
        Object obj = redisUtil.hGet(ConfigKey.JOB_BEAN_METHOD_KEY, field);
        if (null != obj) {
            return (JobInfoDto) obj;
        }

        JobInfoQueryParam jobInfoQueryParam = new JobInfoQueryParam();
        jobInfoQueryParam.setBeanName(beanName);
        jobInfoQueryParam.setMethodName(methodName);
        jobInfoQueryParam.setParams(params);

        JobInfoDto jobInfoDto = jobInfoAssembler.doToEntity(jobInfoDomainService.getByCondition(jobInfoQueryParam));
        redisUtil.hPut(ConfigKey.JOB_BEAN_METHOD_KEY, field, Optional.ofNullable(jobInfoDto).orElse(new JobInfoDto()));
        return jobInfoDto;
    }
}
