package top.yueshushu.extension.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @ClassName:MyDateUtil
 *  TODO
 * @author 岳建立
 * @date 2021/11/12 22:14
 * @Version 1.0
 **/
public class MyDateUtil {
    private static LocalTime MORNING_START_TIME = LocalTime.parse("09:20:00");
    private static LocalTime MORNING_PRICE_START_TIME = LocalTime.parse("09:15:00");
    private static LocalTime MORNING_START_DEAL_TIME = LocalTime.parse("09:30:00");
    private static LocalTime MORNING_END_TIME = LocalTime.parse("11:30:10");


    private static LocalTime AFTERNOON_START_TIME = LocalTime.parse("13:00:00");
    private static LocalTime AFTERNOON_END_TIME = LocalTime.parse("15:00:20");


    private static LocalTime EVENING_START_TIME = LocalTime.parse("20:00:00");
    private static LocalTime EVENING_END_TIME = LocalTime.parse("20:59:59");


    private static LocalTime STOCK_PRICE_START_TIME = LocalTime.parse("14:59:43");
    private static LocalTime STOCK_PRICE_END_TIME = LocalTime.parse("15:00:07");

    private static LocalTime STOCK_MORNING_START_TIME = LocalTime.parse("11:29:43");
    private static LocalTime STOCK_MORNING_END_TIME = LocalTime.parse("11:30:17");

    /**
     * 当前时间是否在下午3点之后
     *
     * @return
     */
    public static boolean after15Hour() {
        Date now = DateUtil.date();
        //组装一个下午3点的时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date hour15 = calendar.getTime();
        if (now.before(hour15)) {
            return false;
        }
        return true;
    }

    /**
     * 当前时间是否在下午7点之后
     *
     * @return
     */
    public static boolean after19Hour() {
        Date now = DateUtil.date();
        //组装一个下午3点的时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 19);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date hour15 = calendar.getTime();
        if (now.before(hour15)) {
            return false;
        }
        return true;
    }


    /**
     * 当前时间是否在下午3点之后
     *
     * @return
     */
    public static boolean before930(Date date) {
        Date now = null;
        if (date == null) {
            now = DateUtil.date();
        } else {
            now = date;
        }
        //组装一个下午3点的时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.SECOND, 0);
        Date hour930 = calendar.getTime();
        if (now.before(hour930)) {
            return true;
        }
        return false;
    }

    public static boolean before950() {
        Date now = DateUtil.date();
        //组装一个下午3点的时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 50);
        calendar.set(Calendar.SECOND, 1);
        Date hour930 = calendar.getTime();
        if (now.before(hour930)) {
            return true;
        }
        return false;
    }


    public static boolean after935() {
        Date now = DateUtil.date();
        //组装一个下午3点的时间
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 35);
        calendar.set(Calendar.SECOND, 30);
        Date hour949 = calendar.getTime();
        if (now.after(hour949)) {
            return true;
        }
        return false;
    }

    /**
     * 是否是 9点 20到 11:30 的时间
     */
    public static boolean isMorning() {
        LocalTime now = LocalTime.now();
        if (now.isAfter(MORNING_START_TIME) && now.isBefore(MORNING_END_TIME)) {
            return true;
        }
        return false;
    }


    /**
     * 是否是 9点 15到 11:30 的时间
     */
    public static boolean isPrice() {
        LocalTime now = LocalTime.now();
        if (now.isAfter(MORNING_PRICE_START_TIME) && now.isBefore(MORNING_END_TIME)) {
            return true;
        }
        return false;
    }

    /**
     * 是否是 9点 30 到 11:30 的时间
     */
    public static boolean isDealMorning() {
        LocalTime now = LocalTime.now();
        if (now.isAfter(MORNING_START_DEAL_TIME) && now.isBefore(MORNING_END_TIME)) {
            return true;
        }
        return false;
    }

    /**
     * 是否是 13 点 到 15:00 的时间
     */
    public static boolean isAfternoon() {
        LocalTime now = LocalTime.now();
        if (now.isAfter(AFTERNOON_START_TIME) && now.isBefore(AFTERNOON_END_TIME)) {
            return true;
        }
        return false;
    }


    /**
     * 当前时间是否 9点20 到 15点之间
     *
     * @return
     */
    public static boolean isWorkingTime() {
        return isMorning() || isAfternoon();
    }

    /**
     * 当前时间是否 9点20 到 15点之间
     *
     * @return
     */
    public static boolean isPriceTime() {
        return isPrice() || isAfternoon();
    }

    /**
     * 当前时间是否 9点半到 15点之间
     *
     * @return
     */
    public static boolean isDealTime() {
        return isDealMorning() || isAfternoon();
    }

    /**
     * 是否是在晚上 9点 到 10点之间
     */
    public static boolean isEveningStat() {
        LocalTime now = LocalTime.now();
        if (now.isAfter(EVENING_START_TIME) && now.isBefore(EVENING_END_TIME)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        // System.out.println(after15Hour());
        //  System.out.println(convertToTodayDate(null, "2500"));
        //  System.out.println(convertDateNum("20220210"));

        List<String> dateList = Arrays.asList(
                "2023-09-01",
                "2023-09-02",
                "2023-09-05",
                //     "2023-09-07",
                //   "2023-09-10",
                "2023-09-14",
                "2023-09-28",
                "2023-09-29",
                "2023-09-30"
        );
        System.out.println(containsDate(dateList, 10, 5));
    }

    public static Date convertToTodayDate(String dateStr, String timeStr) {
        if (StringUtils.hasText(timeStr)) {
            while (timeStr.length() < 6) {
                timeStr = "0" + timeStr;
            }
        }
        // String --> LocalDate
        LocalDate localDate = StringUtils.hasText(dateStr) ? LocalDate.parse(dateStr, DateTimeFormatter.ofPattern("yyyyMMdd")) : LocalDate.now();
        // String --> LocalTime
        LocalTime localTime = StringUtils.hasText(timeStr) ? LocalTime.parse(timeStr, DateTimeFormatter.ofPattern("HHmmss")) : LocalTime.now();
        return Date.from(LocalDateTime.of(localDate, localTime).atZone(ZoneId.systemDefault()).toInstant());
    }

    public static String convertDateNum(String date) {
        return date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);
    }

    public static boolean isWillEndStockPriceTime() {
        // 进行延迟， 如果时间在 14:59 之后，则睡眠 1分钟。
        LocalTime now = LocalTime.now();
        if (now.isAfter(STOCK_PRICE_START_TIME) && now.isBefore(STOCK_PRICE_END_TIME)) {
            return true;
        }
        return false;
    }

    public static boolean isWillEndStockMorningTime() {
        // 是否快结束了。
        LocalTime now = LocalTime.now();
        if (now.isAfter(STOCK_MORNING_START_TIME) && now.isBefore(STOCK_MORNING_END_TIME)) {
            return true;
        }
        return false;
    }

    public static String formatDb(Date date) {
        if (date == null) {
            return "";
        }
        return DateUtil.format(date, DatePattern.NORM_DATE_PATTERN);
    }

    public static void sleep(int milliSeconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliSeconds + RandomUtil.randomInt(milliSeconds / 4));
        } catch (InterruptedException e) {

        }
    }

    public static void realSleep(int milliSeconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliSeconds);
        } catch (InterruptedException e) {

        }
    }

    public static void await(CountDownLatch countDownLatch, Integer timeout, TimeUnit timeUnit) {
        try {
            if (timeout == null || timeout == 0) {
                countDownLatch.await();
            } else {
                countDownLatch.await(timeout, timeUnit);
            }
        } catch (InterruptedException e) {

        }
    }


    public static LocalDateTime toLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDateTime();
    }

    public static boolean afterTime(Date dateTime, int timeNum) {
        Integer time = DateUtil.minute(dateTime);
        String timeStr = time >= 10 ? time + "" : "0" + time;
        int nowTime = Integer.parseInt(DateUtil.hour(dateTime, true) + "" + timeStr);
        return nowTime > timeNum;
    }

    public static boolean beforeTime(Date dateTime, int timeNum) {
        Integer time = DateUtil.minute(dateTime);
        String timeStr = time >= 10 ? time + "" : "0" + time;
        int nowTime = Integer.parseInt(DateUtil.hour(dateTime, true) + "" + timeStr);
        return nowTime < timeNum;
    }

    /**
     * 判断日期内 是否至少有 leDays 落在 totalDays 天内
     *
     * @param dateList  日期集合
     * @param totalDays 天长度
     * @param leDays    至少天
     */
    public static String containsDate(List<String> dateList, int totalDays, Integer leDays) {
        if (CollectionUtils.isEmpty(dateList) || dateList.size() < leDays) {
            return null;
        }
        // 将按照进行排序。
        dateList = dateList.stream().sorted().collect(Collectors.toList());

        int handlerDay = 0;
        String handlerDate = dateList.get(0);

        for (int i = 1; i < dateList.size(); i++) {
            // 后面的日期 - 前面的日期 在 totalDays 以内。
            Date startDate = DateUtil.parse(dateList.get(i - 1));
            Date endDate = DateUtil.parse(dateList.get(i));
            if (DateUtil.betweenDay(startDate, endDate, true) > (totalDays - handlerDay)) {
                handlerDate = dateList.get(i);
                continue;
            }
            handlerDay += 1;

            if (handlerDay >= leDays) {
                return handlerDate;
            }
        }
        return null;
    }
}
