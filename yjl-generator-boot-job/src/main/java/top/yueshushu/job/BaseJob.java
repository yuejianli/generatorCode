package top.yueshushu.job;

/**
 * 定时任务基础接口
 *
 * @author Yue Jianli
 * @date 2023-04-13
 */

public interface BaseJob {
    /**
     * 执行任务
     * @param param 参数
     */
    void execute(String param);
}
