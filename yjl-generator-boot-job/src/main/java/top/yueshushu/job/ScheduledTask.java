package top.yueshushu.job;

import java.util.concurrent.ScheduledFuture;

/**
 * 线程任务处理
 *
 * @author yuejianli
 * @date 2023-04-13
 */

public final class ScheduledTask {

    volatile ScheduledFuture<?> future;
    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.future;
        if (future != null) {
            future.cancel(true);
        }
    }
}