package top.yueshushu.job;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.extension.component.event.MessageInfo;
import top.yueshushu.extension.enums.JobInfoType;
import top.yueshushu.extension.enums.TopicType;
import top.yueshushu.extension.helper.PublishMessageHelper;
import top.yueshushu.extension.model.dto.JobInfoDto;
import top.yueshushu.extension.service.JobInfoService;
import top.yueshushu.extension.util.CronExpression;
import top.yueshushu.message.MessageTextTemplate;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Objects;

/**
 * 被定时任务线程池调用，用来执行指定bean里面的方法。
 *
 * @author yuejianli
 * @date 2023-04-13
 */
@Slf4j
public class SchedulingRunnable implements Runnable {
    private String beanName;

    private String methodName;

    private String params;

    private Integer triggerType;

    public SchedulingRunnable(String beanName, String methodName) {
        this(beanName, methodName, null);
    }

    public SchedulingRunnable(String beanName, String methodName, String params) {
        this.beanName = beanName;
        this.methodName = methodName;
        this.params = params;
    }

    public SchedulingRunnable(String beanName, String methodName, String params, Integer triggerType) {
        this.beanName = beanName;
        this.methodName = methodName;
        this.params = params;
        this.triggerType = triggerType;
    }

    private JobInfoService jobInfoService = SpringUtil.getBean("jobInfoService");
    @Override
    public void run() {
        // 进行查询
        JobInfoDto jobInfoDto = jobInfoService.getByBeanAndMethodAndParams(beanName,methodName,params);
        jobInfoDto.setTriggerLastTime(DateUtil.date());
        try {
            Object target = SpringUtil.getBean(beanName);
            Method method;
            if (StringUtils.hasText(params)) {
                method = target.getClass().getDeclaredMethod(methodName, String.class);
            } else {
               // method = target.getClass().getDeclaredMethod(methodName);
                method = target.getClass().getDeclaredMethod(methodName, String.class);
            }
            ReflectionUtils.makeAccessible(method);
            if (StringUtils.hasText(params)) {
                method.invoke(target, params);
            } else {
               //  method.invoke(target);
                method.invoke(target, params);
            }
            jobInfoDto.setTriggerLastResult(1);
            jobInfoDto.setTriggerLastErrorMessage(null);
        } catch (Exception ex) {
            log.error("执行 {} 任务失败", jobInfoDto.getName(), ex);
            jobInfoDto.setTriggerLastResult(0);
            jobInfoDto.setTriggerLastErrorMessage(ex.getMessage());
            String errorWxMessage = StrUtil.format(MessageTextTemplate.JOB_FAIL_TEXT, jobInfoDto.getName(), ex.getMessage());
            PublishMessageHelper.sendWxMessage(this, MessageInfo.buildByUserId(1, errorWxMessage, TopicType.ERROR));
        }finally {
            //设置下次触发的时间
            if (triggerType != null && triggerType == 1) {
                jobInfoDto.setTriggerType(1);
            }else {
                jobInfoDto.setTriggerType(0);
            }
            jobInfoDto.setTriggerLastTime(DateUtil.date());
            try {
                CronExpression cronExpression = new CronExpression(jobInfoDto.getCron());
                Date date = cronExpression.getNextValidTimeAfter(new Date());
                jobInfoDto.setTriggerNextTime(date);
            } catch (Exception e) {
                log.error("获取下一次触发时间失败", e);
            }
            // 对于 价格等定时任务，不进行更新。
            // 不是频繁任务，才进行更新
            if (!JobInfoType.isFrequentJob(jobInfoDto.getCode())) {
                jobInfoService.updateInfoById(jobInfoDto);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SchedulingRunnable that = (SchedulingRunnable) o;
        if (params == null) {
            return beanName.equals(that.beanName) &&
                    methodName.equals(that.methodName) &&
                    that.params == null;
        }

        return beanName.equals(that.beanName) &&
                methodName.equals(that.methodName) &&
                params.equals(that.params);
    }

    @Override
    public int hashCode() {
        if (params == null) {
            return Objects.hash(beanName, methodName);
        }

        return Objects.hash(beanName, methodName, params);
    }
}

