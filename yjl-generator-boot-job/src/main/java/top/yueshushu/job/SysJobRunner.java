package top.yueshushu.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import top.yueshushu.extension.domain.JobInfoDo;
import top.yueshushu.extension.service.JobInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 服务启动时处理任务
 *
 * @author yuejianli
 * @date 2023-04-13
 */

@Slf4j
@Component
public class SysJobRunner implements CommandLineRunner {

    @Resource
    private CronTaskRegistrar cronTaskRegistrar;

    @Resource
    private JobInfoService jobInfoService;

    @Value("${enableJob:true}")
    private boolean enableJob;
    @Override
    public void run(String... args) {
        // 初始加载数据库里状态为正常的定时任务
        if (!enableJob) {
            log.info(">>>> 不启用定时任务功能");
            return ;
        }
        List<JobInfoDo> jobList = jobInfoService.findAllRunJob();
        if (!CollectionUtils.isEmpty(jobList)) {
            for (JobInfoDo job : jobList) {
                SchedulingRunnable task = new SchedulingRunnable(job.getBeanName(), job.getMethodName(), job.getParam());
                cronTaskRegistrar.addCronTask(task, job.getCron());
            }
            log.info("定时任务已加载完毕...");
        }else {
            log.info("启用了定时任务,但目前没有需要运行的定时任务...");
        }
    }
}
