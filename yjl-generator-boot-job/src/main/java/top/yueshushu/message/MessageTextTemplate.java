package top.yueshushu.message;

import java.io.Serializable;

/**
 * 消息模板
 * @author yuejianli
 * @date 2023-04-19
 */
public class MessageTextTemplate implements Serializable {
    public static final String JOB_FAIL_TEXT = "执行任务 {} 失败，失败原因是:{}";
}
