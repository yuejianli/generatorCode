package top.yueshushu.message.weixin.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 *  默认的微信配置
 * @author yuejianli
 * @date 2022/6/4 17:29
 **/
@Data
@Component
public class DefaultWXProperties implements Serializable {

    @Value("${weixin.corpId}")
    private String corpId;

    @Value("${weixin.coprsecret}")
    private String coprsecret;

    @Value("${weixin.agentId}")
    private Integer agentId;

}
