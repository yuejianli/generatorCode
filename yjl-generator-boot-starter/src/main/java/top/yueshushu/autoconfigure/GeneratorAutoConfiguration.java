package top.yueshushu.autoconfigure;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import top.yueshushu.config.template.CoreGeneratorConfig;

/**
 * @author 两个蝴蝶飞
 */
@Configuration
@AllArgsConstructor
@ComponentScan(basePackages = {"top.yueshushu"})
@EnableConfigurationProperties(GeneratorProperties.class)
public class GeneratorAutoConfiguration {
    private final GeneratorProperties properties;

    @Bean
    CoreGeneratorConfig generatorConfig() {
        return new CoreGeneratorConfig(properties.getTemplate(),properties.getScrew());
    }

}
