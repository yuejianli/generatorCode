package top.yueshushu.business;

import cn.hutool.http.server.HttpServerResponse;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.ro.ScrewDocumentRO;

import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>GenDatasourceBusiness此接口用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
public interface GenDatasourceBusiness {

    void testConnection(Long datasourceId) throws SQLException, ClassNotFoundException;

    List<GenTableDO> findTablesById(Long datasourceId);

    /**
     * 通过  screw 生成数据库文档
     * @param screwDocumentRO  生成文档对象
     * @param httpServerResponse 响应
     */
    void downloadDocument(ScrewDocumentRO screwDocumentRO, HttpServletResponse httpServerResponse) throws Exception;

    List<String> findAllDbNameList(String filePath);

}
