package top.yueshushu.business;

import top.yueshushu.dao.GenGroupTemplateDO;
import top.yueshushu.dto.TemplateRelationContentDto;

import java.util.List;

/**
 * <p>GenGroupBusiness此接口用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
public interface GenGroupBusiness {
    /**
     * 将组进行复制
     * @param groupId 组id
     */
    void copy(Long groupId);

    List<GenGroupTemplateDO> preview(Long groupId);

}
