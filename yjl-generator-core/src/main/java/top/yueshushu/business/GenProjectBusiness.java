package top.yueshushu.business;

import top.yueshushu.dao.GenProjectModifyDO;

import java.io.IOException;

/**
 * <p>ProjectBusiness此接口用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
public interface GenProjectBusiness {
    byte[] download(GenProjectModifyDO project) throws IOException;


}
