package top.yueshushu.business;

import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.vo.GenTableVO;

import java.util.List;

/**
 * <p>GenTableBusiness此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
public interface GenTableBusiness {

    GenTableVO getTableWithFieldById(Long id);

    void removeByIds(List<Long> idList);

    /**
     * 同步表结构信息从 对应的库里面
     * @param tableId 表id
     */
    void syncTableFromDB(Long tableId);


    void tableImport(Long datasourceId, String tableName);

    void updateTableField(Long tableId, List<GenTableFieldDO> tableFieldList);

    /**
     * 批量导入表结构信息
     * @param datasourceId 数据源id
     * @param tableNameList 表名集合
     */
    void batchTableImport(Long datasourceId, List<String> tableNameList);
}
