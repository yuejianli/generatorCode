package top.yueshushu.business;

import top.yueshushu.dto.TemplateRelationContentDto;

import java.util.List;
import java.util.zip.ZipOutputStream;

/**
 * <p>GeneratorBusiness此接口用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
public interface GeneratorBusiness {


    void downloadCode(long tableId, ZipOutputStream zip);


    void generatorCode(Long tableId);

    List<TemplateRelationContentDto> preview(Long tableId);


}
