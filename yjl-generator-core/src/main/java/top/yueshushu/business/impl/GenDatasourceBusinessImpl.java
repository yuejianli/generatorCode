package top.yueshushu.business.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.server.HttpServerResponse;
import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import cn.smallbun.screw.core.engine.EngineConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import top.yueshushu.business.GenDatasourceBusiness;
import top.yueshushu.common.exception.BusinessException;
import top.yueshushu.config.template.CoreGeneratorConfig;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dto.GenDatasourceDTO;
import top.yueshushu.ro.ScrewDocumentRO;
import top.yueshushu.service.GenDatasourceService;
import top.yueshushu.service.GenFieldTypeService;
import top.yueshushu.service.GenTableFieldService;
import top.yueshushu.service.GenTableService;
import top.yueshushu.util.DbUtils;
import top.yueshushu.util.GenUtils;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.file.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * <p>GenDatasourceBusinessImpl此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Service
@Slf4j
public class GenDatasourceBusinessImpl implements GenDatasourceBusiness {
    @Resource
    private GenDatasourceService genDatasourceService;
    @Resource
    private CoreGeneratorConfig coreGeneratorConfig;



    @Override
    public void testConnection(Long datasourceId) throws SQLException, ClassNotFoundException {
        GenDatasourceDTO genDatasourceDTO = genDatasourceService.getDtoById(datasourceId);

        DbUtils.getConnection(genDatasourceDTO);
    }

    @Override
    public List<GenTableDO> findTablesById(Long datasourceId) {
        // 获取数据源
        GenDatasourceDTO genDatasourceDTO = genDatasourceService.getDtoById(datasourceId);
        // 根据数据源，获取全部数据表
        return GenUtils.getTableList(genDatasourceDTO);
    }

    @Override
    public void downloadDocument(ScrewDocumentRO screwDocumentRO, HttpServletResponse httpServerResponse) throws Exception{

        GenDatasourceDO genDatasourceDO = genDatasourceService.getById(screwDocumentRO.getId());
        if (ObjectUtils.isEmpty(genDatasourceDO)) {
            throw new BusinessException("该数据库数据异常");
        }
        GenDatasourceDTO genDatasourceDTO = new GenDatasourceDTO(genDatasourceDO);

        try{
            //数据源
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setDriverClassName(genDatasourceDTO.getDbType().getDriverClass());
            hikariConfig.setJdbcUrl(genDatasourceDTO.getConnUrl());
            hikariConfig.setUsername(genDatasourceDTO.getUsername());
            hikariConfig.setPassword(genDatasourceDTO.getPassword());
            //设置可以获取tables remarks信息
            hikariConfig.addDataSourceProperty("useInformationSchema", "true");
            hikariConfig.setMinimumIdle(2);
            hikariConfig.setMaximumPoolSize(5);
            DataSource dataSource = new HikariDataSource(hikariConfig);

            String fileName;
            if (StringUtils.hasText(screwDocumentRO.getFileName())) {
                fileName = screwDocumentRO.getFileName();
            } else {
                fileName = genDatasourceDO.getConnName() + "_screw.md";
            }

            EngineConfig engineConfig = EngineConfig.builder()
                    //生成文件路径
                    .fileOutputDir(coreGeneratorConfig.getScrewUrl())
                    //打开目录
                    .openOutputDir(true)
                    //文件类型
                    .fileType(getEngineFileType(screwDocumentRO.getFileType()))
                    //生成模板实现
                    .produceType(EngineTemplateType.freemarker)
                    //自定义文件名称
                    .fileName(fileName).build();

            //忽略表
            List<String> ignoreTableName = convertToList(screwDocumentRO.getIgnoreTableName());
            //忽略表前缀
            List<String> ignorePrefix = convertToList(screwDocumentRO.getIgnorePrefix());
            ignorePrefix.add("test_");
            //忽略表后缀
            List<String> ignoreSuffix = convertToList(screwDocumentRO.getIgnoreSuffix());
            ignoreSuffix.add("_test");
            ProcessConfig processConfig ;

            if (CollectionUtils.isEmpty(screwDocumentRO.getTableNameList())) {
                processConfig = ProcessConfig.builder()
                        //指定生成逻辑、当存在指定表、指定表前缀、指定表后缀时，将生成指定表，其余表不生成、并跳过忽略表配置
                        //根据名称指定表生成
                       // .designatedTableName(screwDocumentRO.getTableNameList())
                        //根据表前缀生成
                           // .designatedTablePrefix(screwDocumentRO.getTablePrefixList())
                        //根据表后缀生成
                           // .designatedTableSuffix(screwDocumentRO.getTableSuffixList())
                        //忽略表名
                            .ignoreTableName(ignoreTableName)
                        //忽略表前缀
                            .ignoreTablePrefix(ignorePrefix)
                        //忽略表后缀
                           .ignoreTableSuffix(ignoreSuffix)
                        .build();
            }else {
                processConfig = ProcessConfig.builder()
                        //指定生成逻辑、当存在指定表、指定表前缀、指定表后缀时，将生成指定表，其余表不生成、并跳过忽略表配置
                        //根据名称指定表生成
                        .designatedTableName(screwDocumentRO.getTableNameList())
                        .build();
            }

            String description;
            if (StringUtils.hasText(screwDocumentRO.getDescription())) {
                description = screwDocumentRO.getDescription();
            } else {
                description = genDatasourceDO.getConnName() + "数据库文档";
            }


            //配置
            Configuration config = Configuration.builder()
                    //版本
                    .version(screwDocumentRO.getVersion())
                    //描述
                    .description(description)
                    //数据源
                    .dataSource(dataSource)
                    //生成配置
                    .engineConfig(engineConfig)
                    //生成配置
                    .produceConfig(processConfig)
                    .build();
            //执行生成
            new DocumentationExecute(config).execute();
            // 下载文件
            downloadLocal(coreGeneratorConfig.getScrewUrl() + screwDocumentRO.getFileName(), httpServerResponse);
        }catch (Exception e){
            log.error("下载文件出错{}", screwDocumentRO, e);
            throw e;
        }
    }

    @Override
    public List<String> findAllDbNameList(String directory) {
        List<String> result = new ArrayList<>();
        String contentKey = "@TableName";
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(directory))) {
            for (Path file: stream) {
                if (Files.isRegularFile(file) && file.getFileName().toString().endsWith(".java")) {
                    // 读取文件的内容
                    List<String> fileContentList = FileUtil.readUtf8Lines(file.toFile());
                    for (String line : fileContentList) {
                        if (line.contains(contentKey)) {
                            String packageName = line.substring(contentKey.length() +2, line.length() - 2);
                            result.add(packageName);
                            break;
                        }
                    }
                }
            }
        } catch (IOException | DirectoryIteratorException e) {
            e.printStackTrace();
        }
        return result;
    }

    private List<String> convertToList(String text) {
        if (!StringUtils.hasText(text)) {
            return new ArrayList<>();
        }
        return Arrays.stream(text.split("\\,")).collect(Collectors.toList());
    }

    private EngineFileType getEngineFileType(String fileType) {
        if (!StringUtils.hasText(fileType)) {
            return EngineFileType.MD;
        }
        fileType = fileType.toLowerCase(Locale.ROOT);
        if (fileType.contains("html")) {
            return EngineFileType.HTML;
        } else if (fileType.contains("doc")) {
            return EngineFileType.WORD;
        } else {
            return EngineFileType.MD;
        }
    }


    /**
     * @param path     指想要下载的文件的路径
     * @param response
     * @功能描述 下载文件:将输入流中的数据循环写入到响应输出流中，而不是一次性读取到内存
     */
    public void downloadLocal(String path, HttpServletResponse response) throws IOException {
        // 读到流中
        InputStream inputStream = new FileInputStream(path);// 文件的存放路径
        response.reset();
        response.setContentType("application/octet-stream");
        String filename = new File(path).getName();
        response.addHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(filename, "UTF-8"));
        ServletOutputStream outputStream = response.getOutputStream();
        byte[] b = new byte[1024];
        int len;
        //从输入流中读取一定数量的字节，并将其存储在缓冲区字节数组中，读到末尾返回-1
        while ((len = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, len);
        }
        inputStream.close();
    }

}
