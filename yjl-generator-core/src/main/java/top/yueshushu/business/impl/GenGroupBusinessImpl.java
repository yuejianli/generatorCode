package top.yueshushu.business.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import org.springframework.stereotype.Service;
import top.yueshushu.business.GenGroupBusiness;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenGroupTemplateDO;
import top.yueshushu.service.GenGroupService;
import top.yueshushu.service.GenGroupTemplateService;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>GenGroupBusinessImpl此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Service
public class GenGroupBusinessImpl implements GenGroupBusiness {
    @Resource
    private GenGroupService genGroupService;
    @Resource
    private GenGroupTemplateService genGroupTemplateService;

    @Override
    public void copy(Long groupId) {
        // 保存组
        GenGroupDO newDo = saveGroup(groupId);
        // 保存明细
        saveDetail(groupId, newDo);
    }

    @Override
    public List<GenGroupTemplateDO> preview(Long groupId) {
        List<GenGroupTemplateDO> genGroupTemplateDOS = genGroupTemplateService.listByGroupId(groupId);
        genGroupTemplateDOS.forEach(
                n-> n.setContent(n.getTemplateContent())
        );
        return genGroupTemplateDOS;
    }

    private GenGroupDO saveGroup(Long groupId) {
        GenGroupDO genGroupDO = genGroupService.getById(groupId);
        // 进行复制
        GenGroupDO newDo = new GenGroupDO();
        BeanUtil.copyProperties(genGroupDO,newDo);
        newDo.setId(null);
        newDo.setName(genGroupDO.getName()+"_copy");
        genGroupService.save(newDo);
        return newDo;
    }

    private void saveDetail(Long groupId, GenGroupDO newDo) {
        List<GenGroupTemplateDO> genGroupTemplateDOList = genGroupTemplateService.listByGroupId(groupId);
        List<GenGroupTemplateDO> copyGenGroupTemplateDOList = ObjectUtil.cloneByStream(genGroupTemplateDOList);
        copyGenGroupTemplateDOList.forEach(
                n-> {
                    n.setId(null);
                    n.setGroupId(newDo.getId());
                }
        );
        genGroupTemplateService.saveBatch(copyGenGroupTemplateDOList);
    }
}
