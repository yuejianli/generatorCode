package top.yueshushu.business.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.NamingCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import top.yueshushu.business.GenTableBusiness;
import top.yueshushu.common.exception.BusinessException;
import top.yueshushu.config.template.GeneratorInfo;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.dto.GenDatasourceDTO;
import top.yueshushu.enums.FormLayoutEnum;
import top.yueshushu.enums.GeneratorTypeEnum;
import top.yueshushu.helper.ModelHelper;
import top.yueshushu.service.GenDatasourceService;
import top.yueshushu.service.GenFieldTypeService;
import top.yueshushu.service.GenTableFieldService;
import top.yueshushu.service.GenTableService;
import top.yueshushu.util.GenUtils;
import top.yueshushu.vo.GenTableVO;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>GenTableBusinessImpl此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Service
@Slf4j
public class GenTableBusinessImpl implements GenTableBusiness {
    @Resource
    private GenTableService genTableService;
    @Resource
    private GenTableFieldService genTableFieldService;
    @Resource
    private GenDatasourceService genDatasourceService;
    @Resource
    private GenFieldTypeService genFieldTypeService;
    @Resource
    private ModelHelper modelHelper;

    @Override
    public GenTableVO getTableWithFieldById(Long id) {
        GenTableDO genTableDO = genTableService.getById(id);
        List<GenTableFieldDO> genTableFieldDOList = genTableFieldService.getByTableId(id);

        GenTableVO genTableVO = new GenTableVO();
        BeanUtil.copyProperties(genTableDO,genTableVO);
        genTableVO.setFieldList(genTableFieldDOList);
        return genTableVO;
    }

    @Override
    public void removeByIds(List<Long> idList) {
        genTableService.removeByIds(idList);
        genTableFieldService.removeByTableIds(idList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void syncTableFromDB(Long tableId) {

        GenTableDO genTableDO = genTableService.getById(tableId);

        // 初始化配置信息
        GenDatasourceDTO datasourceDtO = genDatasourceService.getDtoById(genTableDO.getDatasourceId());


        // 从数据库获取表信息
        updateTableCommentIfChange(genTableDO, datasourceDtO);
        // 增量更新表信息
        updateFieldIfChangeWithIncrement(tableId, genTableDO, datasourceDtO);
    }

    /**
     * 更新字段,如果发现改变, 是增量更新.
     * @param tableId 表id
     * @param genTableDO 目前库里面的老表
     * @param datasourceDtO 数据库信息
     */
    private void updateFieldIfChangeWithIncrement(Long tableId, GenTableDO genTableDO, GenDatasourceDTO datasourceDtO) {
        // 从数据库获取表字段列表
        List<GenTableFieldDO> dbTableFieldList = GenUtils.getTableFieldList(datasourceDtO, genTableDO.getTableName(), genTableDO.getId());
        if (CollectionUtils.isEmpty(dbTableFieldList)) {
            throw new BusinessException("同步失败，请检查数据库表：" + genTableDO.getTableName());
        }

        List<String> dbTableFieldNameList = dbTableFieldList.stream().map(GenTableFieldDO::getFieldName).collect(Collectors.toList());
        // 表字段列表
        List<GenTableFieldDO> tableFieldList = genTableFieldService.getByTableId(tableId);

        // 查询出目前已经存在的字段信息.
        Map<String, GenTableFieldDO> tableFieldMap = tableFieldList.stream().collect(Collectors.toMap(GenTableFieldDO::getFieldName, Function.identity()));

        // 初始化字段数据
        genTableFieldService.initFieldList(dbTableFieldList,genFieldTypeService.convertAllToMap());

        // 同步表结构字段
        dbTableFieldList.forEach(field -> {
            // 新增字段
            if (!tableFieldMap.containsKey(field.getFieldName())) {
                genTableFieldService.save(field);
                return;
            }

            // 修改字段
            GenTableFieldDO updateField = tableFieldMap.get(field.getFieldName());
            updateField.setPrimaryPk(field.getPrimaryPk());
            updateField.setFieldComment(field.getFieldComment());
            updateField.setFieldType(field.getFieldType());
            updateField.setAttrType(field.getAttrType());

            genTableFieldService.updateById(updateField);
        });

        // 删除数据库表中没有的字段
        List<GenTableFieldDO> delFieldList = tableFieldList.stream().filter(field -> !dbTableFieldNameList.contains(field.getFieldName())).collect(Collectors.toList());
        if (delFieldList.size() > 0) {
            List<Long> fieldIds = delFieldList.stream().map(GenTableFieldDO::getId).collect(Collectors.toList());
            genTableFieldService.removeByIds(fieldIds);
        }
    }

    private void updateTableCommentIfChange(GenTableDO genTableDO, GenDatasourceDTO datasourceDtO) {
        GenTableDO newGenTableDO = GenUtils.getTable(datasourceDtO, genTableDO.getTableName());
        if (newGenTableDO.getTableComment().equals(genTableDO.getTableComment())) {
            return ;
        }

        GenTableDO editGenTableDO = new GenTableDO();
        editGenTableDO.setId(genTableDO.getId());
        editGenTableDO.setTableComment(newGenTableDO.getTableComment());
        genTableService.updateById(editGenTableDO);
    }

    /**
     *
     * @param datasourceId 数据源id
     * @param tableName 单个表名
     *
     *  1. 根据表名 获取表信息, 进行存储.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void tableImport(Long datasourceId, String tableName) {
        // 初始化配置信息
        GenDatasourceDTO dataSource = genDatasourceService.getDtoById(datasourceId);
        // 查询表是否存在
        GenTableDO table = genTableService.getByTableName(datasourceId,tableName);
        // 表存在
        if (null != table) {
            return ;
        }

        // 从数据库获取表信息
        table = GenUtils.getTable(dataSource, tableName);
        table.setClassName(NamingCase.toPascalCase(tableName));
        // 代码生成器信息
        GeneratorInfo generator = modelHelper.getGeneratorInfoByDb(datasourceId, table);

        // 保存表信息
        table.setPackageName(generator.getProject().getPackageName());
        table.setVersion(generator.getProject().getVersion());
        table.setBackendPath(generator.getProject().getBackendPath());
        table.setFrontendPath(generator.getProject().getFrontendPath());
        table.setAuthor(generator.getDeveloper().getAuthor());
        table.setEmail(generator.getDeveloper().getEmail());
        table.setFormLayout(FormLayoutEnum.ONE.getValue());
        table.setGeneratorType(GeneratorTypeEnum.ZIP_DOWNLOAD.ordinal());

        table.setModuleName(GenUtils.getModuleName(table.getPackageName()));
        table.setFunctionName(GenUtils.getFunctionName(tableName));
        table.setCreateTime(LocalDateTime.now());
        genTableService.save(table);

        // 获取原生字段数据并将类型进行填充进去.
        List<GenTableFieldDO> tableFieldList = GenUtils.getTableFieldList(dataSource, table.getTableName(), table.getId());
        //
        genTableFieldService.initFieldList(tableFieldList,genFieldTypeService.convertAllToMap());

        // 保存列数据
        genTableFieldService.saveBatch(tableFieldList);
        try {
            //释放数据源
            dataSource.getConnection().close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void updateTableField(Long tableId, List<GenTableFieldDO> tableFieldList) {
        // 更新字段数据
        int sort = 0;
        for (GenTableFieldDO tableField : tableFieldList) {
            tableField.setSort(sort++);
        }

        genTableFieldService.updateBatchById(tableFieldList);
    }

    @Override
    public void batchTableImport(Long datasourceId, List<String> tableNameList) {
        for (String tableName : tableNameList) {
            tableImport(datasourceId, tableName);
        }

    }
}
