package top.yueshushu.business.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.yueshushu.business.GeneratorBusiness;
import top.yueshushu.common.exception.BusinessException;
import top.yueshushu.config.template.CoreGeneratorConfig;
import top.yueshushu.dto.TemplateInfoDto;
import top.yueshushu.dto.TemplateRelationContentDto;
import top.yueshushu.helper.ModelHelper;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * <p>GeneratorBusinessImpl此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Service
@Slf4j
public class GeneratorBusinessImpl implements GeneratorBusiness {
    @Resource
    private CoreGeneratorConfig coreGeneratorConfig;
    @Resource
    private ModelHelper modelHelper;

    @Override
    public void downloadCode(long tableId, ZipOutputStream zip) {

        TemplateInfoDto templateInfoDto = modelHelper.getDetailByTableId(tableId);
        // 渲染模板并输出
        for (TemplateRelationContentDto templateRelationContentDto : templateInfoDto.getRelationContentDtoList()) {
            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(templateRelationContentDto.getGeneratorPath()));
                IoUtil.writeUtf8(zip, false, templateRelationContentDto.getContent());
                zip.flush();
                zip.closeEntry();
            } catch (IOException e) {
                throw new BusinessException("模板写入失败：" + templateRelationContentDto.getGeneratorPath(), e);
            }
        }
    }

    @Override
    public void generatorCode(Long tableId) {
        TemplateInfoDto templateInfoDto = modelHelper.getDetailByTableId(tableId);
        // 渲染模板并输出
        for (TemplateRelationContentDto templateRelationContentDto : templateInfoDto.getRelationContentDtoList()) {
            try {
                FileUtil.writeUtf8String(templateRelationContentDto.getContent(), templateRelationContentDto.getGeneratorPath());
            } catch (Exception e) {
                throw new BusinessException("模板写入失败：" + templateRelationContentDto.getGeneratorPath(), e);
            }
        }
    }

    @Override
    public List<TemplateRelationContentDto> preview(Long tableId) {

        TemplateInfoDto templateInfoDto = modelHelper.getDetailByTableId(tableId);
        return templateInfoDto.getRelationContentDtoList();
    }



}
