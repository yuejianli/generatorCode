package top.yueshushu.common.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 基础Dao
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
public interface BaseDao<T> extends BaseMapper<T> {

}
