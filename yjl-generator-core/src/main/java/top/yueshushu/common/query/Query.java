package top.yueshushu.common.query;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

/**
 * 查询公共参数
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Data
public class Query {
    String code;
    String tableName;
    String attrType;
    String columnType;
    String connName;
    String dbType;
    String projectName;

    Integer page;
    Integer limit;
    String tableLikeName;
}
