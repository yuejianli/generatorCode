package top.yueshushu.common.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 基础服务接口，所有Service接口都要继承
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
public interface BaseService<T> extends IService<T> {


}