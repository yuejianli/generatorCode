package top.yueshushu.common.utils;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.yueshushu.common.exception.ErrorCode;

import java.io.Serializable;

/**
 * 响应数据
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Data
public class OutputResult<T>  implements Serializable {
    // 编码 0表示成功，其他值表示失败
    private int code = 0;
    // 消息内容
    private String msg = "success";
    // 响应数据
    private T data;

    @ApiModelProperty("是否成功  true 为成功  false 为不成功")
    private Boolean success;
    @ApiModelProperty("响应信息")
    private String message;
    @ApiModelProperty("当前时间戳")
    private long timestamp;
    @ApiModelProperty("当前时间戳")
    private String timestampStr;
    @ApiModelProperty("异常信息")
    private String exceptionMessage;

    /**
     * 构造方法 私有。 避免外部构造
     */
    public OutputResult() {

    }

    public OutputResult(ResultCode ResultCode) {
        this(ResultCode, null);
    }

    public OutputResult(ResultCode resultCode, T data) {
        this(resultCode.isSuccess(), resultCode.getCode(), resultCode.getMessage(), data);
    }

    public OutputResult(boolean success, int code, String message, T data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
        this.timestamp = System.currentTimeMillis();
        this.timestampStr = DateUtil.now();
    }

    public static <T> OutputResult<T> ok() {
        return ok(null);
    }

    public static <T> OutputResult<T> ok(T data) {
        OutputResult<T> outputResult = new OutputResult<>();
        outputResult.setData(data);
        return outputResult;
    }

    public static <T> OutputResult<T> error() {
        return error(ErrorCode.INTERNAL_SERVER_ERROR);
    }

    public static <T> OutputResult<T> error(String msg) {
        return error(ErrorCode.INTERNAL_SERVER_ERROR.getCode(), msg);
    }

    public static <T> OutputResult<T> error(ErrorCode errorCode) {
        return error(errorCode.getCode(), errorCode.getMsg());
    }

    public static <T> OutputResult<T> error(int code, String msg) {
        OutputResult<T> outputResult = new OutputResult<>();
        outputResult.setCode(code);
        outputResult.setMsg(msg);
        return outputResult;
    }

    public static <T> OutputResult<T> buildFail() {
        return buildFail(ResultCode.FAIL);
    }

    public static <T> OutputResult<T> buildFail(String message) {
        return buildFail(ResultCode.FAIL.getCode(), message);
    }

    public static <T> OutputResult<T> buildFail(ResultCode resultCode) {
        return buildFail(resultCode.getCode(), resultCode.getMessage());
    }

    public static <T> OutputResult<T> buildFail(int code, String message) {
        return build(false, code, message, null);
    }

    public static <T> OutputResult<T> buildFail(ResultCode resultCode, T data) {
        return buildFail(resultCode, null, data);
    }

    public static <T> OutputResult<T> buildFail(ResultCode resultCode, String message) {
        return new OutputResult<>(resultCode.isSuccess(), resultCode.getCode(), message, null);
    }

    public static <T> OutputResult<T> buildFail(ResultCode resultCode, String message, T data) {
        return buildFail(resultCode.getCode(), message, data);
    }

    public static <T> OutputResult<T> buildFail(int code, String message, T data) {
        return build(false, code, message, data);
    }


    public static <T> OutputResult<T> buildAlert() {
        return buildAlert(ResultCode.ALERT);
    }

    public static <T> OutputResult<T> buildAlert(String message) {
        return buildAlert(ResultCode.ALERT.getCode(), message);
    }

    public static <T> OutputResult<T> buildAlert(ResultCode resultCode) {
        return buildAlert(resultCode.getCode(), resultCode.getMessage());
    }

    public static <T> OutputResult<T> buildAlert(int code, String message) {
        return build(false, code, message, null);
    }

    public static <T> OutputResult<T> buildAlert(ResultCode resultCode, T data) {
        return buildAlert(resultCode, null, data);
    }

    public static <T> OutputResult<T> buildAlert(ResultCode resultCode, String message) {
        return new OutputResult<>(resultCode.isSuccess(), resultCode.getCode(), message, null);
    }

    public static <T> OutputResult<T> buildAlert(ResultCode resultCode, String message, T data) {
        return buildFail(resultCode.getCode(), message, data);
    }

    public static <T> OutputResult<T> buildAlert(int code, String message, T data) {
        return build(false, code, message, data);
    }


    public static <T> OutputResult<T> buildSucc() {
        return buildSucc(ResultCode.SUCCESS);
    }

    public static <T> OutputResult<T> buildSucc(T data) {
        return buildSucc(ResultCode.SUCCESS, data);
    }

    public static <T> OutputResult<T> buildSucc(ResultCode resultCode) {
        return buildSucc(resultCode.getCode(), resultCode.getMessage());
    }

    public static <T> OutputResult<T> buildSucc(int code, String message) {
        return buildSucc(code, message, null);
    }

    public static <T> OutputResult<T> buildSucc(ResultCode resultCode, T data) {
        return buildSucc(resultCode, null, data);
    }

    public static <T> OutputResult<T> buildSucc(ResultCode resultCode, String message, T data) {
        return buildSucc(resultCode.getCode(), message, data);
    }

    public static <T> OutputResult<T> buildSucc(int code, String message, T data) {
        return build(true, code, message, data);
    }

    private static <T> OutputResult<T> build(boolean success, int code, String message, T data) {
        return new OutputResult<>(success, code, message, data);
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
