package top.yueshushu.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @ClassName:PageResponse
 *  分页响应的信息
 * @author 岳建立
 * @date 2021/11/19 21:06
 * @Version 1.0
 **/
@Data
@ApiModel("分页展示")
public class PageResponse<T> {
    @ApiModelProperty("总数")
    private Long total;
    @ApiModelProperty("list数据")
    private List<T> list;
    public PageResponse(Long total, List<T> list) {
        this.total = Optional.ofNullable(total).orElse(0L);
        this.list = Optional.ofNullable(list).orElse(Collections.emptyList());
    }
    public static PageResponse emptyPageResponse(){
        return new PageResponse(0L,
                Collections.EMPTY_LIST);
    }
}
