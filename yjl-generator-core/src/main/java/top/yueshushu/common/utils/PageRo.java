package top.yueshushu.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName:PageRo
 *  分页参数
 * @author 岳建立
 * @date 2021/11/19 19:15
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@ApiModel("分页参数对象")
public class PageRo implements Serializable {

    /**
     * @param pageNum 页数
     */
    @ApiModelProperty(value = "页数", required = true)
    private Integer pageNum = 1;

    @ApiModelProperty(value = "每页显示最大数量", required = true)
    private Integer pageSize = 10;

    @ApiModelProperty(value = "起始位置", hidden = true)
    private Integer startNum;

    public Integer getStartNum() {
        return (pageNum - 1) * pageSize;
    }
}