package top.yueshushu.config.template;

import cn.hutool.core.util.StrUtil;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import top.yueshushu.common.exception.BusinessException;
import top.yueshushu.common.utils.JsonUtils;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenGroupTemplateDO;
import top.yueshushu.dao.GenTableDO;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * 代码生成配置内容
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
public class CoreGeneratorConfig {
    private String template;
    private String screw;

    public CoreGeneratorConfig() {

    }

    public CoreGeneratorConfig(String template,String screw) {
        this.template = template;
        this.screw = screw;
    }

    public String getScrewUrl() {
        if (!StrUtil.endWith(screw, '/')) {
            screw = screw + "/";
        }
        return screw;
    }

    public GeneratorInfo getGeneratorConfig() {
        // 模板路径，如果不是以/结尾，则添加/
        if (!StrUtil.endWith(template, '/')) {
            template = template + "/";
        }

        // 模板配置文件
        InputStream isConfig = this.getClass().getResourceAsStream(template + "config.json");
        if (isConfig == null) {
            throw new BusinessException("模板配置文件，config.json不存在");
        }

        try {
            // 读取模板配置文件
            String configContent = StreamUtils.copyToString(isConfig, StandardCharsets.UTF_8);
            GeneratorInfo generator = JsonUtils.parseObject(configContent, GeneratorInfo.class);
            for (TemplateInfo templateInfo : generator.getTemplates()) {
                // 模板文件
                InputStream isTemplate = this.getClass().getResourceAsStream(template + templateInfo.getTemplateName());
                if (isTemplate == null) {
                    throw new BusinessException("模板文件 " + templateInfo.getTemplateName() + " 不存在");
                }
                // 读取模板内容
                String templateContent = StreamUtils.copyToString(isTemplate, StandardCharsets.UTF_8);

                templateInfo.setTemplateContent(templateContent);
            }
            return generator;
        } catch (IOException e) {
            throw new BusinessException("读取config.json配置文件失败");
        }
    }

    public GeneratorInfo generatorConfigByDb(GenGroupDO genGroupDO, List<GenGroupTemplateDO> genGroupTemplateDOList, GenTableDO genTableDO) {
        if (genGroupDO.getEnable() == 0 || CollectionUtils.isEmpty(genGroupTemplateDOList)) {
            return getGeneratorConfig();
        }
        // 查询出数据
        GeneratorInfo generatorInfo = new GeneratorInfo();
        DeveloperInfo developerInfo = new DeveloperInfo();
        developerInfo.setAuthor(genGroupDO.getAuthor());
        developerInfo.setEmail(genGroupDO.getEmail());
        generatorInfo.setDeveloper(developerInfo);
        generatorInfo.setOtherParams(genGroupDO.getParams());
        generatorInfo.setGroupId(genGroupDO.getId());
        ProjectInfo projectInfo = new ProjectInfo();
        projectInfo.setProjectName(genGroupDO.getProjectName());
        projectInfo.setPackageName(genGroupDO.getPackageName());
        projectInfo.setModuleName(genGroupDO.getModuleName());
        projectInfo.setVersion(genGroupDO.getVersion());
        projectInfo.setBackendPath(genGroupDO.getBackPath());
        projectInfo.setFrontendPath(genGroupDO.getFrontPath());

        if (genGroupDO.getSwagger2() == null || genGroupDO.getSwagger2() == 1) {
            projectInfo.setSwagger2(true);
        } else {
            projectInfo.setSwagger2(false);
        }

        // 填充对应的名称.
        generatorInfo.setProject(projectInfo);

        // 设置模板信息
        List<TemplateInfo> templates = new ArrayList<>();

        genGroupTemplateDOList.forEach(
                n-> {
                    TemplateInfo templateInfo = new TemplateInfo();
                    templateInfo.setTemplateContent(n.getTemplateContent());
                    templateInfo.setGeneratorPath(n.getGeneratorPath());
                    templateInfo.setTemplateName(n.getTemplateName());
                    templateInfo.setOriginContent(n.getTemplateContent());
                    templateInfo.setFullName(n.getFullName());
                    templateInfo.setPackageName(n.getPackageName());
                    templateInfo.setSignName(n.getSignName());
                    templates.add(templateInfo);
                }
        );
        generatorInfo.setTemplates(templates);
        return fillPackageNameWithTableId(genTableDO, generatorInfo);

    }

    /**
     *  填充包相应的信息.
     * @param tableDO 表信息
     * @param generatorInfo 之前生成的对象信息
     * @return 填充包相关的信息
     */
    public GeneratorInfo fillPackageNameWithTableId(GenTableDO tableDO, GeneratorInfo generatorInfo) {
        List<TemplateInfo> templates = generatorInfo.getTemplates();
        String lowerClassName = StrUtil.lowerFirst(tableDO.getClassName());
        String upperClassName = StrUtil.upperFirst(tableDO.getClassName());

        ProjectInfo projectInfo = generatorInfo.getProject();
        templates.forEach(
                n-> {
                    if (StringUtils.hasText(n.getSignName())) {
                        //
                        switch (n.getSignName().toLowerCase(Locale.ROOT)) {
                            case "controller": {
                                projectInfo.setControllerPackageName(n.getPackageName());
                                projectInfo.setControllerClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLcontrollerClassName(StrUtil.lowerFirst(projectInfo.getControllerClassName()));
                                break;
                            }
                            case "service": {
                                projectInfo.setServicePackageName(n.getPackageName());
                                projectInfo.setServiceClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLserviceClassName(removeIWithPerfix(StrUtil.lowerFirst(projectInfo.getServiceClassName()),n.getFullName()));
                                break;
                            }
                            case "serviceimpl": {
                                projectInfo.setServiceImplPackageName(n.getPackageName());
                                projectInfo.setServiceImplClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLserviceImplClassName(StrUtil.lowerFirst(projectInfo.getServiceImplClassName()));
                                break;
                            }

                            case "business": {
                                projectInfo.setBusinessPackageName(n.getPackageName());
                                projectInfo.setBusinessClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLbusinessClassName(removeIWithPerfix(StrUtil.lowerFirst(projectInfo.getBusinessClassName()),n.getFullName()));
                                break;
                            }
                            case "businessimpl": {
                                projectInfo.setBusinessImplPackageName(n.getPackageName());
                                projectInfo.setBusinessImplClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLbusinessImplClassName(StrUtil.lowerFirst(projectInfo.getBusinessImplClassName()));
                                break;
                            }


                            case "do": {
                                projectInfo.setDoPackageName(n.getPackageName());
                                projectInfo.setDoClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLdoClassName(StrUtil.lowerFirst(projectInfo.getDoClassName()));
                                break;
                            }
                            case "vo": {
                                projectInfo.setVoPackageName(n.getPackageName());
                                projectInfo.setVoClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLvoClassName(StrUtil.lowerFirst(projectInfo.getVoClassName()));
                                break;
                            }
                            case "ro": {
                                projectInfo.setRoPackageName(n.getPackageName());
                                projectInfo.setRoClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLroClassName(StrUtil.lowerFirst(projectInfo.getRoClassName()));
                                break;
                            }
                            case "param": {
                                projectInfo.setParamPackageName(n.getPackageName());
                                projectInfo.setParamClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLparamClassName(StrUtil.lowerFirst(projectInfo.getParamClassName()));
                                break;
                            }
                            case "convert": {
                                projectInfo.setConvertPackageName(n.getPackageName());
                                projectInfo.setConvertClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLconvertClassName(StrUtil.lowerFirst(projectInfo.getConvertClassName()));
                                break;
                            }
                            case "mapper": {
                                projectInfo.setMapperPackageName(n.getPackageName());
                                projectInfo.setMapperClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLmapperClassName(StrUtil.lowerFirst(projectInfo.getMapperClassName()));
                                break;
                            }
                            case "mapperxml": {
                                projectInfo.setMapperXmlPackageName(n.getPackageName());
                                projectInfo.setMapperXmlClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLmapperXmlClassName(StrUtil.lowerFirst(projectInfo.getMapperXmlClassName()));
                                break;
                            }
                            case "dto": {
                                projectInfo.setDtoPackageName(n.getPackageName());
                                projectInfo.setDtoClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLdtoClassName(StrUtil.lowerFirst(projectInfo.getDtoClassName()));
                                break;
                            }
                            default: {
                                projectInfo.setOtherPackageName(n.getPackageName());
                                projectInfo.setOtherClassName(replaceName(lowerClassName,upperClassName,n.getFullName()));
                                projectInfo.setLotherClassName(StrUtil.lowerFirst(projectInfo.getOtherClassName()));
                                break;
                            }
                        }
                    }

                }
        );
        generatorInfo.setProject(projectInfo);
        return generatorInfo;
    }

    private String replaceName(String lowerClassName, String upperClassName, String fullName) {
        fullName = Optional.ofNullable(fullName).orElse("");
        if (fullName.contains("%s")) {
            return fullName.replaceAll("%s",lowerClassName);
        } {
            return fullName.replaceFirst("%S", upperClassName);
        }
    }

    private String removeIWithPerfix(String lowerName, String fullName) {
        if (fullName.startsWith("I%")) {
            return StrUtil.lowerFirst(lowerName.substring(1));
        }

        return StrUtil.lowerFirst(lowerName);
    }
}
