package top.yueshushu.config.template;

import lombok.Data;

/**
 * 开发者信息
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Data
public class DeveloperInfo {
    /**
     * 作者
     */
    private String author;
    /**
     * 邮箱
     */
    private String email;
}
