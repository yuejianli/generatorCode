package top.yueshushu.config.template;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 代码生成信息
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Data
public class GeneratorInfo implements Serializable {
    private ProjectInfo project;
    private DeveloperInfo developer;
    private List<TemplateInfo> templates;
    private String otherParams;
    private Long groupId;
}
