package top.yueshushu.config.template;

import lombok.Data;

/**
 * 项目信息
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Data
public class ProjectInfo {
    /**
     * 项目名
     */
    private String projectName;
    /**
     * 项目包名
     */
    private String packageName;

    /**
     * 项目模块名
     */
    private String moduleName;


    /**
     * 项目版本号
     */
    private String version;
    /**
     * 后端生成路径
     */
    private String backendPath;
    /**
     * 前端生成路径
     */
    private String frontendPath;

    /**
     * 是否支持swagger2
     */
    private Boolean swagger2;




    private String doPackageName;

    private String doClassName;
    private String ldoClassName;

    private String dtoPackageName;
    private String dtoClassName;
    private String ldtoClassName;


    private String mapperPackageName;
    private String mapperClassName;
    private String lmapperClassName;

    private String mapperXmlPackageName;
    private String mapperXmlClassName;
    private String lmapperXmlClassName;



    private String servicePackageName;
    private String serviceClassName;
    private String lserviceClassName;

    private String serviceImplPackageName;
    private String serviceImplClassName;
    private String lserviceImplClassName;


    private String businessPackageName;
    private String businessClassName;
    private String lbusinessClassName;

    private String businessImplPackageName;
    private String businessImplClassName;
    private String lbusinessImplClassName;




    private String controllerPackageName;
    private String controllerClassName;
    private String lcontrollerClassName;


    private String convertPackageName;
    private String convertClassName;
    private String lconvertClassName;


    private String voPackageName;
    private String voClassName;
    private String lvoClassName;


    private String roPackageName;
    private String roClassName;
    private String lroClassName;


    private String paramPackageName;
    private String paramClassName;
    private String lparamClassName;

    private String otherPackageName;
    private String otherClassName;
    private String lotherClassName;
}