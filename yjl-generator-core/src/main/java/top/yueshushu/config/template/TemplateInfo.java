package top.yueshushu.config.template;

import lombok.Data;

/**
 * 模板信息
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Data
public class TemplateInfo {
    /**
     * 模板名称
     */
    private String templateName;
    /**
     * 模板内容
     */
    private String templateContent;
    /**
     * 生成代码的路径
     */
    private String generatorPath;
    /**
     * 原始内容
     */
    private String originContent;

    /**
     * 标识的 fullName
     */
    private String fullName;

    private String packageName;

    private String signName;
}
