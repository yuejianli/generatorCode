package top.yueshushu.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenBaseClassDO;
import top.yueshushu.service.GenBaseClassService;

import javax.annotation.Resource;
import javax.xml.transform.Result;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 基类管理 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/baseclass")
@Api(tags = "自动生成代码_基础类")
public class GenBaseClassController {

    @Resource
    private GenBaseClassService genBaseClassService;


    @ApiOperation(value = "分页查询基础类", notes = "分页查询基础类")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenBaseClassDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenBaseClassDO>> page(Query query) {
        return OutputResult.ok(genBaseClassService.pageByQuery(query));
    }

    @ApiOperation(value = "查询基础类", notes = "全部查询基础类")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenBaseClassDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenBaseClassDO>> list() {
        return OutputResult.ok(genBaseClassService.list());
    }

    @ApiOperation(value = "根据id查询基础类", notes = "根据id查询基础类")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "基础类id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenBaseClassDO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenBaseClassDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genBaseClassService.getById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenBaseClassDO genBaseClassDO) {
        genBaseClassDO.setCreateTime(LocalDateTime.now());
        genBaseClassService.save(genBaseClassDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenBaseClassDO genBaseClassDO) {

        genBaseClassService.updateById(genBaseClassDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "删除", notes = "根据 ids[] 批量删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genBaseClassService.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }

}
