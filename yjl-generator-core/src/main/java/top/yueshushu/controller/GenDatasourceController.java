package top.yueshushu.controller;


import cn.hutool.http.server.HttpServerResponse;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.business.GenDatasourceBusiness;
import top.yueshushu.common.GenerateCommon;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenBaseClassDO;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.ro.ScrewDocumentRO;
import top.yueshushu.service.GenDatasourceService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 数据源管理 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/datasource")
@Slf4j
@Api(tags = "自动生成代码_数据库")
public class GenDatasourceController {
    @Resource
    private GenDatasourceService genDatasourceService;
    @Resource
    private GenDatasourceBusiness genDatasourceBusiness;

    /**
     * 根据数据库id ,测试连接该库是否目前是可连接状态
     * @param id 数据库id
     * @return 返回是否可以连接状态
     */
    @GetMapping("test/{id}")
    @ApiOperation(value = "根据id测试是否能正常连接")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "数据库id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    public OutputResult<String> test(@PathVariable("id") Long id) {
        try {
            genDatasourceBusiness.testConnection(id);
            return OutputResult.ok("连接成功");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return OutputResult.error("连接失败，请检查配置信息");
        }
    }


    /**
     * 根据数据源ID，获取返回全部数据表
     * 直接写 根据库查询全部表的sql语句, 然后将数据进行封装.
     * @param id 数据源ID
     */
    @GetMapping("table/list/{id}")
    @ApiOperation(value = "根据id查询所有的表")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "数据库id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    public OutputResult<List<GenTableDO>> tableList(@PathVariable("id") Long id) {
        try {
            List<GenTableDO> genTableDOList = genDatasourceBusiness.findTablesById(id);
            return OutputResult.ok(genTableDOList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return OutputResult.error("数据源配置错误，请检查数据源配置！");
        }
    }

    @ApiOperation(value = "配置组", notes = "根据 DO修改")
    @PutMapping("/configTemplateGroup")
    public OutputResult<String> configTemplateGroup(@RequestBody GenDatasourceDO genDatasourceDO) {

        GenDatasourceDO editGenDatasourceDO = new GenDatasourceDO();
        editGenDatasourceDO.setGroupId(genDatasourceDO.getGroupId());
        editGenDatasourceDO.setId(genDatasourceDO.getId());
        genDatasourceService.updateById(editGenDatasourceDO);


        return OutputResult.ok();
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenDatasourceDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenDatasourceDO>> page(Query query) {
        return OutputResult.ok(genDatasourceService.pageByQuery(query));
    }
    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenDatasourceDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenDatasourceDO>> list() {
        return OutputResult.ok(genDatasourceService.list());
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenDatasourceDO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenDatasourceDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genDatasourceService.getById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenDatasourceDO genDatasourceDO) {
        genDatasourceDO.setCreateTime(LocalDateTime.now());
        genDatasourceDO.setGroupId(GenerateCommon.DEFAULT_GROUP_ID);
        genDatasourceService.save(genDatasourceDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenDatasourceDO genDatasourceDO) {

        genDatasourceService.updateById(genDatasourceDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "删除", notes = "根据 ids[] 批量删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genDatasourceService.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }
}
