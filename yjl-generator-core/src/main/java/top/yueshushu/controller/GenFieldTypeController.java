package top.yueshushu.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.service.GenFieldTypeService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 字段类型管理 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/fieldtype")
@Api(tags = "自动生成代码_文件类型")
public class GenFieldTypeController {

    @Resource
    private GenFieldTypeService genFieldTypeService;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenFieldTypeDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenFieldTypeDO>> page(Query query) {
        return OutputResult.ok(genFieldTypeService.pageByQuery(query));
    }

    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenFieldTypeDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenFieldTypeDO>> list() {
        return OutputResult.ok(genFieldTypeService.list());
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenFieldTypeDO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenFieldTypeDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genFieldTypeService.getById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenFieldTypeDO genFieldTypeDO) {
        genFieldTypeDO.setCreateTime(LocalDateTime.now());
        genFieldTypeService.save(genFieldTypeDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenFieldTypeDO genFieldTypeDO) {

        genFieldTypeService.updateById(genFieldTypeDO);

        return OutputResult.ok();
    }


    @ApiOperation(value = "删除", notes = "根据 ids[] 批量删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genFieldTypeService.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }
}
