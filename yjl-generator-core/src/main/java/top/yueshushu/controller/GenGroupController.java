package top.yueshushu.controller;


import io.swagger.annotations.*;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.business.GenGroupBusiness;
import top.yueshushu.common.GenerateCommon;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenGroupTemplateDO;
import top.yueshushu.dto.TemplateRelationContentDto;
import top.yueshushu.ro.GroupRO;
import top.yueshushu.service.GenGroupService;
import top.yueshushu.util.ProjectUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 模板组 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/group")
@Api(tags = "自动生成代码_模板组")
public class GenGroupController {

    @Resource
    private GenGroupService genGroupService;
    @Resource
    private GenGroupBusiness genGroupBusiness;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenGroupDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenGroupDO>> page(GroupRO groupRO) {
        return OutputResult.ok(genGroupService.pageByQuery(groupRO));
    }

    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenGroupDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenGroupDO>> list() {
        return OutputResult.ok(genGroupService.list());
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenGroupDO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenGroupDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genGroupService.getById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenGroupDO genGroupDO) {
        genGroupService.save(genGroupDO);
        return OutputResult.ok();
    }

    @ApiOperation(value = "复制", notes = "根据 id进行复制")
    @PostMapping("/copy/{id}")
    public OutputResult<String> copy(@PathVariable("id") Long id) {
        genGroupBusiness.copy(id);
        return OutputResult.ok();
    }




    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenGroupDO genGroupDO) {

        genGroupService.updateById(genGroupDO);

        return OutputResult.ok();
    }


    @ApiOperation(value = "启用", notes = "根据 id进行启用")
    @PutMapping("/enable/{id}")
    public OutputResult<String> enable(@PathVariable Long id) {

        List<GenGroupDO> genGroupDOS = genGroupService.listByIds(Arrays.asList(id));
        genGroupDOS.forEach(
                n-> n.setEnable(1)
        );
        genGroupService.updateBatchById(genGroupDOS);
        return OutputResult.ok();
    }
    @ApiOperation(value = "禁用", notes = "根据 id进行禁用")
    @PutMapping("/disable/{id}")
    public OutputResult<String> disable(@PathVariable Long id) {
        Long[] ids = new Long[]{id};
        List<Long> genGroupIdList = removeDefaultGroupId(ids);
        if (CollectionUtils.isEmpty(genGroupIdList)) {
            return OutputResult.ok();
        }

        List<GenGroupDO> genGroupDOS = genGroupService.listByIds(genGroupIdList);
        if (CollectionUtils.isEmpty(genGroupDOS)) {
            return OutputResult.ok();
        }
        genGroupDOS.forEach(
                n-> {
                    n.setEnable(0);
                }
        );
        genGroupService.updateBatchById(genGroupDOS);
        return OutputResult.ok();
    }


    @ApiOperation(value = "删除", notes = "根据 id进行删除")
    @PutMapping("/{id}")
    public OutputResult<String> delete(@PathVariable Long id) {
        Long[] ids = new Long[]{id};
        List<Long> genGroupIdList = removeDefaultGroupId(ids);
        if (CollectionUtils.isEmpty(genGroupIdList)) {
            return OutputResult.ok();
        }

        genGroupService.removeByIds(genGroupIdList);
        return OutputResult.ok();
    }


    public List<Long> removeDefaultGroupId (Long[] ids) {
        List<Long> chooseIdList = Arrays.asList(ids);
        chooseIdList.remove(GenerateCommon.DEFAULT_GROUP_ID);
        return chooseIdList;
    }


    /**
     * 预览代码
     */
    @GetMapping("/preview")
    @ApiOperation(value = "根据模板组id进行预览", notes = "根据 id进行预览")
    public OutputResult<List<GenGroupTemplateDO>> preview(@RequestParam Long groupId) throws Exception {
        return OutputResult.ok(genGroupBusiness.preview(groupId));
    }

}
