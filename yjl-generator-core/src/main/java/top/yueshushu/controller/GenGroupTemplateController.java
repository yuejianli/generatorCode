package top.yueshushu.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenGroupTemplateDO;
import top.yueshushu.service.GenGroupTemplateService;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 模板组名称 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/groupTemplate")
@Api(tags = "自动生成代码_模板组明细")
public class GenGroupTemplateController {

    @Resource
    private GenGroupTemplateService genGroupTemplateService;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenGroupTemplateDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenGroupTemplateDO>> page(Query query) {
        return OutputResult.ok(genGroupTemplateService.pageByQuery(query));
    }

    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenGroupTemplateDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenGroupTemplateDO>> list() {
        return OutputResult.ok(genGroupTemplateService.list());
    }


    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenGroupTemplateDO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenGroupTemplateDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genGroupTemplateService.getById(id));
    }

    /**
     * 预览代码
     */
    @ApiOperation(value = "组id进行预览代码查询", notes = "根据组id预览代码查询")
    @GetMapping("/preview")
    public OutputResult<List<GenGroupTemplateDO>> preview(@RequestParam Long groupId) throws Exception {
        return OutputResult.ok(genGroupTemplateService.listByGroupId(groupId));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenGroupTemplateDO genGroupDO) {
        genGroupTemplateService.save(genGroupDO);
        return OutputResult.ok();
    }

    @ApiOperation(value = "启用", notes = "根据 id进行启用")
    @PutMapping("/enable")
    public OutputResult<String> enable(@RequestBody Long[] ids) {

        List<GenGroupTemplateDO> genGroupDOS = genGroupTemplateService.listByIds(Arrays.asList(ids));
        genGroupDOS.forEach(
                n-> n.setEnable(1)
        );
        genGroupTemplateService.updateBatchById(genGroupDOS);
        return OutputResult.ok();
    }

    @ApiOperation(value = "禁用", notes = "根据 id进行禁用")
    @PutMapping("/disable")
    public OutputResult<String> disable(@RequestBody Long[] ids) {

        List<GenGroupTemplateDO> genGroupDOS = genGroupTemplateService.listByIds(Arrays.asList(ids));
        genGroupDOS.forEach(
                n-> n.setEnable(0)
        );
        genGroupTemplateService.updateBatchById(genGroupDOS);
        return OutputResult.ok();
    }


    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenGroupTemplateDO genGroupDO) {

        genGroupDO.setTemplateContent(genGroupDO.getContent());
        genGroupTemplateService.updateById(genGroupDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "删除", notes = "根据 id集合进行批量删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genGroupTemplateService.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }

}
