package top.yueshushu.controller;


import cn.hutool.core.io.IoUtil;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.business.GenProjectBusiness;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenProjectModifyDO;
import top.yueshushu.service.GenProjectModifyService;
import top.yueshushu.util.ProjectUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 项目名变更 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/project")
@Api(tags = "自动生成代码_项目名修改")
public class GenProjectModifyController {

    @Resource
    private GenProjectModifyService genProjectModifyService;
    @Resource
    private GenProjectBusiness genProjectBusiness;

    /**
     * 源码下载
     */
    @ApiOperation(value = "下载", notes = "根据 Id进行下载")
    @GetMapping("download/{id}")
    public void download(@PathVariable("id") Long id, HttpServletResponse response) throws Exception {
        // 项目信息
        GenProjectModifyDO project = genProjectModifyService.getById(id);

        byte[] data = genProjectBusiness.download(project);

        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"" + project.getModifyProjectName() + ".zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");

        IoUtil.write(response.getOutputStream(), false, data);
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenProjectModifyDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenProjectModifyDO>> page(Query query) {
        return OutputResult.ok(genProjectModifyService.pageByQuery(query));
    }

    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenProjectModifyDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenProjectModifyDO>> list() {
        return OutputResult.ok(genProjectModifyService.list());
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenProjectModifyDO.class
            )
    )

    @GetMapping("/{id}")
    public OutputResult<GenProjectModifyDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genProjectModifyService.getById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenProjectModifyDO genProjectModifyDO) {
        genProjectModifyDO.setCreateTime(LocalDateTime.now());
        genProjectModifyDO.setExclusions(ProjectUtils.EXCLUSIONS);
        genProjectModifyDO.setModifySuffix(ProjectUtils.MODIFY_SUFFIX);
        genProjectModifyService.save(genProjectModifyDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenProjectModifyDO genProjectModifyDO) {

        genProjectModifyService.updateById(genProjectModifyDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "删除", notes = "根据 id集合进行批量删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genProjectModifyService.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }

}
