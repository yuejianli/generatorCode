package top.yueshushu.controller;


import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import top.yueshushu.business.GenDatasourceBusiness;
import top.yueshushu.common.GenerateCommon;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.ro.ScrewDocumentRO;
import top.yueshushu.service.GenDatasourceService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 数据源管理 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/screw")
@Slf4j
@Api(tags = "自动生成文档")
public class GenScrewController {
    @Resource
    private GenDatasourceService genDatasourceService;
    @Resource
    private GenDatasourceBusiness genDatasourceBusiness;


    @ApiOperation(value = "根据id生成数据库文档")
    @GetMapping("/downloadDocument/{id}")
    public void downloadDocument(@PathVariable("id") Long id, HttpServletResponse httpServerResponse) throws Exception{
        ScrewDocumentRO screwDocumentRO = new ScrewDocumentRO();
        screwDocumentRO.setId(id);
        genDatasourceBusiness.downloadDocument(screwDocumentRO,httpServerResponse);
    }

    @ApiOperation(value = "根据id生成数据库文档")
    @PostMapping("/downloadSelfDocument")
    public void downloadSelfDocument(@RequestBody ScrewDocumentRO screwDocumentRO,HttpServletResponse httpServerResponse) throws Exception{
        genDatasourceBusiness.downloadDocument(screwDocumentRO,httpServerResponse);
    }

    @ApiOperation(value = "根据id生成数据库文档")
    @PostMapping("/findAllDbNameList")
    public OutputResult<List<String>> findAllDbNameList(@RequestBody ScrewDocumentRO screwDocumentRO) throws Exception{
        return OutputResult.ok(genDatasourceBusiness.findAllDbNameList(screwDocumentRO.getFilePath()));
    }
}
