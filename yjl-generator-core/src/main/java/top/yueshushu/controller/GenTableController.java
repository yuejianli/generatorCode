package top.yueshushu.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.business.GenDatasourceBusiness;
import top.yueshushu.business.GenTableBusiness;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.service.GenTableService;
import top.yueshushu.vo.GenTableVO;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 代码生成表 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/table")
@Api(tags = "自动生成代码_表")
public class GenTableController {

    @Resource
    private GenTableService genTableService;
    @Resource
    private GenTableBusiness genTableBusiness;
    @Resource
    private GenDatasourceBusiness genDatasourceBusiness;


    /**
     * 导入数据源中的表
     *
     * @param datasourceId  数据源ID
     * @param tableNameList 表名列表
     */
    @ApiOperation(value = "导入库里面的表", notes = "导入库里面的表")
    @PostMapping("import/{datasourceId}")
    public OutputResult<String> tableImport(@PathVariable("datasourceId") Long datasourceId, @RequestBody List<String> tableNameList) {
        genTableBusiness.batchTableImport(datasourceId,tableNameList);
        return OutputResult.ok();
    }

    @ApiOperation(value = "全部导入库里面的表", notes = "导入库里面的表")
    @PostMapping("syncImport/{datasourceId}")
    public OutputResult<String> syncImport(@PathVariable("datasourceId") Long datasourceId) {
        genDatasourceBusiness.findTablesById(datasourceId).stream().map(GenTableDO::getTableName).forEach(tableName -> genTableBusiness.tableImport(datasourceId, tableName));
        return OutputResult.ok();
    }

    /**
     * 同步表结构
     *
     * @param id 表ID
     */
    @PostMapping("/sync/{id}")
    @ApiOperation(value = "同步某个表结构", notes = "同步某个表结构")
    public OutputResult<String> sync(@PathVariable("id") Long id) {
        genTableBusiness.syncTableFromDB(id);

        return OutputResult.ok();
    }



    /**
     * 修改表字段数据
     *
     * @param tableId        表ID
     * @param tableFieldList 字段列表
     */
    @PutMapping("field/{tableId}")
    @ApiOperation(value = "修改表属性信息", notes = "修改表属性信息")
    public OutputResult<String> updateTableField(@PathVariable("tableId") Long tableId, @RequestBody List<GenTableFieldDO> tableFieldList) {
        genTableBusiness.updateTableField(tableId, tableFieldList);

        return OutputResult.ok();
    }





    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenTableDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenTableDO>> page(Query query) {
        return OutputResult.ok(genTableService.pageByQuery(query));
    }


    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenTableDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenTableDO>> list() {
        return OutputResult.ok(genTableService.list());
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenTableVO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenTableVO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genTableBusiness.getTableWithFieldById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenTableDO genTableDO) {
        genTableDO.setCreateTime(LocalDateTime.now());
        genTableService.save(genTableDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenTableDO genTableDO) {

        genTableService.updateById(genTableDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "删除", notes = "根据 id 集合进行删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genTableBusiness.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }
    
}
