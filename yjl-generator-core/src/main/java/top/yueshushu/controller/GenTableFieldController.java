package top.yueshushu.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.service.GenTableFieldService;
import top.yueshushu.vo.GenTableVO;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 代码生成表字段 前端控制器
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/gen/TableField")
@Api(tags = "自动生成代码_表属性")
public class GenTableFieldController {

    @Resource
    private GenTableFieldService genTableFieldService;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenTableFieldDO.class
            )
    )
    @GetMapping("/page")
    public OutputResult<PageResult<GenTableFieldDO>> page(Query query) {
        return OutputResult.ok(genTableFieldService.pageByQuery(query));
    }

    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenTableFieldDO.class
            )
    )
    @GetMapping("/list")
    public OutputResult<List<GenTableFieldDO>> list() {
        return OutputResult.ok(genTableFieldService.list());
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = GenTableFieldDO.class
            )
    )
    @GetMapping("/{id}")
    public OutputResult<GenTableFieldDO> get(@PathVariable("id") Long id) {
        return OutputResult.ok(genTableFieldService.getById(id));
    }

    @ApiOperation(value = "添加", notes = "根据 DO添加")
    @PostMapping
    public OutputResult<String> save(@RequestBody GenTableFieldDO genTableFieldDO) {
        genTableFieldService.save(genTableFieldDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "修改", notes = "根据 DO修改")
    @PutMapping
    public OutputResult<String> update(@RequestBody GenTableFieldDO genTableFieldDO) {

        genTableFieldService.updateById(genTableFieldDO);

        return OutputResult.ok();
    }

    @ApiOperation(value = "删除", notes = "根据 id 集合进行删除")
    @DeleteMapping
    public OutputResult<String> delete(@RequestBody Long[] ids) {

        genTableFieldService.removeByIds(Arrays.asList(ids));

        return OutputResult.ok();
    }
}
