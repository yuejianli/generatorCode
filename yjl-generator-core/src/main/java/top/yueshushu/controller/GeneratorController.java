package top.yueshushu.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import top.yueshushu.business.GeneratorBusiness;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dto.TemplateRelationContentDto;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@RestController
@RequestMapping("/gen/generator")
@Api(tags = "自动生成代码_代码生成")
public class GeneratorController {
    @Resource
    private  GeneratorBusiness generatorBusiness;

    /**
     * 生成代码（zip压缩包）
     */
    @ApiOperation(value = "下载生成的文档", notes = "根据 表ids 下载生成的文档")
    @GetMapping("download")
    public void download(String tableIds, HttpServletResponse response) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        // 生成代码
        for (String tableId : tableIds.split(",")) {
            generatorBusiness.downloadCode(Long.parseLong(tableId), zip);
        }

        IoUtil.close(zip);

        // zip压缩包数据
        byte[] data = outputStream.toByteArray();

        response.reset();

        String fileName = "yjl_code_"+ DateUtil.format(new Date(),"yyyyMMddHHmmss")+".zip";
        response.setHeader("Content-Disposition", "attachment; filename="+fileName);
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");

        IoUtil.write(response.getOutputStream(), false, data);
    }

    /**
     * 生成代码（自定义目录）
     */
    @ResponseBody
    @PostMapping("code")
    @ApiOperation(value = "生成代码,自定义目录", notes = "根据 tableIds [] 生成代码")
    public OutputResult<String> code(@RequestBody Long[] tableIds) throws Exception {
        // 生成代码
        for (Long tableId : tableIds) {
            generatorBusiness.generatorCode(tableId);
        }

        return OutputResult.ok();
    }


    /**
     * 预览代码
     */
    @GetMapping("/preview")

    @ApiOperation(value = "根据id预览生成的代码", notes = "根据id预览生成的代码")
    @ApiImplicitParams(
            @ApiImplicitParam(
                    name = "tableId", value = "tableId",required = true, dataType = "Integer", paramType = "query"
            )
    )
    @ApiResponses(
            @ApiResponse(
                    code = 200,
                    message = "查询成功",
                    response = TemplateRelationContentDto.class
            )
    )
    public OutputResult<List<TemplateRelationContentDto>> preview(@RequestParam Long tableId) throws Exception {
        return OutputResult.ok(generatorBusiness.preview(tableId));
    }
}