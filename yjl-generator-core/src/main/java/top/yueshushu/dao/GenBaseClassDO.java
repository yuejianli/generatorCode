package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 基类管理
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_base_class")
public class GenBaseClassDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 基类包名
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 基类编码
     */
    @TableField("code")
    private String code;

    /**
     * 基类字段，多个用英文逗号分隔
     */
    @TableField("fields")
    private String fields;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;


}
