package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字段类型管理
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_field_type")
public class GenFieldTypeDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 字段类型
     */
    @TableField("column_type")
    private String columnType;

    /**
     * 属性类型
     */
    @TableField("attr_type")
    private String attrType;

    /**
     * 属性包名
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;


}
