package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 模板组
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_group")
public class GenGroupDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组id主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 组名
     */
    @TableField("name")
    private String name;

    /**
     * 项目名
     */
    @TableField("project_name")
    private String projectName;

    /**
     * 包名
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 模块名
     */
    @TableField("module_name")
    private String moduleName;

    /**
     * 版本号
     */
    @TableField("version")
    private String version;

    /**
     * 是否启用 1为启用 0为禁用
     */
    @TableField("enable")
    private Integer enable;

    /**
     * 后端路径
     */
    @TableField("back_path")
    private String backPath;

    /**
     * 前端路径
     */
    @TableField("front_path")
    private String frontPath;

    /**
     * 作者
     */
    @TableField("author")
    private String author;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 功能备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 是否启用swagger2,  1为启用, 0为禁用.
     */
    @TableField("swagger2")
    private Integer swagger2;

    /**
     * 额外参数,map对应的json形式
     */
    @TableField("params")
    private String params;

}
