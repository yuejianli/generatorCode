package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 模板组名称
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_group_template")
public class GenGroupTemplateDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组分类明细id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 关联组id
     */
    @TableField(value = "group_id")
    private Long groupId;


    /**
     * 标识名字
     */
    @TableField(value = "sign_name")
    private String signName;

    /**
     * 模板名称
     */
    @TableField("template_name")
    private String templateName;

    /**
     * 生成路径
     */
    @TableField("generator_path")
    private String generatorPath;

    /**
     * 模板内容
     */
    @TableField("template_content")
    private String templateContent;

    /**
     * 类完整名称
     */
    @TableField("full_name")
    private String fullName;

    /**
     * 包名
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 是否启用 1为启用 0为不启用
     */
    @TableField("enable")
    private Integer enable;


    /**
     * 排序字段
     */
    @TableField("order_num")
    private Integer orderNum;


    /**
     * 模板内容
     */
    @TableField(exist = false)
    private String content;

}
