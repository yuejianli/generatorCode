package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 项目名变更
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_project_modify")
public class GenProjectModifyDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 项目名
     */
    @TableField("project_name")
    private String projectName;

    /**
     * 项目标识
     */
    @TableField("project_code")
    private String projectCode;

    /**
     * 项目包名
     */
    @TableField("project_package")
    private String projectPackage;

    /**
     * 项目路径
     */
    @TableField("project_path")
    private String projectPath;

    /**
     * 变更项目名
     */
    @TableField("modify_project_name")
    private String modifyProjectName;

    /**
     * 变更标识
     */
    @TableField("modify_project_code")
    private String modifyProjectCode;

    /**
     * 变更包名
     */
    @TableField("modify_project_package")
    private String modifyProjectPackage;

    /**
     * 排除文件
     */
    @TableField("exclusions")
    private String exclusions;

    /**
     * 变更文件
     */
    @TableField("modify_suffix")
    private String modifySuffix;

    /**
     * 变更临时路径
     */
    @TableField("modify_tmp_path")
    private String modifyTmpPath;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;


}
