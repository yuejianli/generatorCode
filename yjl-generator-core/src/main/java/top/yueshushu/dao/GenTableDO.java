package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 代码生成表
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_table")
public class GenTableDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 表名
     */
    @TableField("table_name")
    private String tableName;

    /**
     * 类名
     */
    @TableField("class_name")
    private String className;

    /**
     * 说明
     */
    @TableField("table_comment")
    private String tableComment;

    /**
     * 作者
     */
    @TableField("author")
    private String author;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 项目包名
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 项目版本号
     */
    @TableField("version")
    private String version;

    /**
     * 生成方式  0：zip压缩包   1：自定义目录
     */
    @TableField("generator_type")
    private Integer generatorType;

    /**
     * 后端生成路径
     */
    @TableField("backend_path")
    private String backendPath;

    /**
     * 前端生成路径
     */
    @TableField("frontend_path")
    private String frontendPath;

    /**
     * 模块名
     */
    @TableField("module_name")
    private String moduleName;

    /**
     * 功能名
     */
    @TableField("function_name")
    private String functionName;

    /**
     * 表单布局  1：一列   2：两列
     */
    @TableField("form_layout")
    private Integer formLayout;

    /**
     * 数据源ID
     */
    @TableField("datasource_id")
    private Long datasourceId;

    /**
     * 基类ID
     */
    @TableField("baseclass_id")
    private Long baseclassId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;


}
