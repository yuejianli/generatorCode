package top.yueshushu.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 代码生成表字段
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("gen_table_field")
public class GenTableFieldDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 表ID
     */
    @TableField("table_id")
    private Long tableId;

    /**
     * 字段名称
     */
    @TableField("field_name")
    private String fieldName;

    /**
     * 字段类型
     */
    @TableField("field_type")
    private String fieldType;

    /**
     * 字段说明
     */
    @TableField("field_comment")
    private String fieldComment;

    /**
     * 属性名
     */
    @TableField("attr_name")
    private String attrName;

    /**
     * 属性类型
     */
    @TableField("attr_type")
    private String attrType;

    /**
     * 属性包名
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE
     */
    @TableField("auto_fill")
    private String autoFill;

    /**
     * 主键 0：否  1：是
     */
    @TableField("primary_pk")
    private Integer primaryPk;

    /**
     * 基类字段 0：否  1：是
     */
    @TableField("base_field")
    private Integer baseField;

    /**
     * 表单项 0：否  1：是
     */
    @TableField("form_item")
    private Integer formItem;

    /**
     * 表单必填 0：否  1：是
     */
    @TableField("form_required")
    private Integer formRequired;

    /**
     * 表单类型
     */
    @TableField("form_type")
    private String formType;

    /**
     * 表单字典类型
     */
    @TableField("form_dict")
    private String formDict;

    /**
     * 表单效验
     */
    @TableField("form_validator")
    private String formValidator;

    /**
     * 列表项 0：否  1：是
     */
    @TableField("grid_item")
    private Integer gridItem;

    /**
     * 列表排序 0：否  1：是
     */
    @TableField("grid_sort")
    private Integer gridSort;

    /**
     * 查询项 0：否  1：是
     */
    @TableField("query_item")
    private Integer queryItem;

    /**
     * 查询方式
     */
    @TableField("query_type")
    private String queryType;

    /**
     * 查询表单类型
     */
    @TableField("query_form_type")
    private String queryFormType;


    @TableField(exist = false)
    private Boolean primaryPkb;

    @TableField(exist = false)
    private Boolean baseFieldb;

    @TableField(exist = false)
    private Boolean formItemb;

    @TableField(exist = false)
    private Boolean formRequiredb;

    @TableField(exist = false)
    private Boolean gridSortb;

    @TableField(exist = false)
    private Boolean queryItemb;

    public Boolean getPrimaryPkb() {
        return convertBoolean(primaryPk);
    }

    public Boolean getBaseFieldb() {
        return convertBoolean(baseField);
    }

    public Boolean getFormItemb() {
        return convertBoolean(formItem);
    }

    public Boolean getFormRequiredb() {
        return convertBoolean(formRequired);
    }

    public Boolean getGridSortb() {
        return convertBoolean(gridSort);
    }

    public Boolean getQueryItemb() {
        return convertBoolean(queryItem);
    }

    private Boolean convertBoolean(Integer value) {
        return value != null && value == 1;
    }
}
