package top.yueshushu.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import top.yueshushu.config.DbType;
import top.yueshushu.config.query.*;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.util.DbUtils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * <p>GenDatasourceDTO此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Data
@Slf4j
public class GenDatasourceDTO implements Serializable {
    /**
     * 数据源ID
     */
    private Long id;
    /**
     * 数据库类型
     */
    private DbType dbType;
    /**
     * 数据库URL
     */
    private String connUrl;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    private AbstractQuery dbQuery;

    private Connection connection;

    public GenDatasourceDTO(GenDatasourceDO genDatasourceDO) {
        this.id = genDatasourceDO.getId();
        this.dbType = DbType.getValue(genDatasourceDO.getDbType());
        this.connUrl = genDatasourceDO.getConnUrl();
        this.username = genDatasourceDO.getUsername();
        this.password = genDatasourceDO.getPassword();

        if (dbType == DbType.MySQL) {
            this.dbQuery = new MySqlQuery();
        } else if (dbType == DbType.Oracle) {
            this.dbQuery = new OracleQuery();
        } else if (dbType == DbType.PostgreSQL) {
            this.dbQuery = new PostgreSqlQuery();
        } else if (dbType == DbType.SQLServer) {
            this.dbQuery = new SQLServerQuery();
        } else if (dbType == DbType.DM) {
            this.dbQuery = new DmQuery();
        } else if (dbType == DbType.Clickhouse) {
            this.dbQuery = new ClickHouseQuery();
        }else if (dbType == DbType.KingBase) {
            this.dbQuery = new KingBaseSqlQuery();
        }

        try {
            this.connection = DbUtils.getConnection(this);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public GenDatasourceDTO(Connection connection) throws SQLException {
        this.id = 0L;
        this.dbType = DbType.getValue(connection.getMetaData().getDatabaseProductName());

        if (dbType == DbType.MySQL) {
            this.dbQuery = new MySqlQuery();
        } else if (dbType == DbType.Oracle) {
            this.dbQuery = new OracleQuery();
        } else if (dbType == DbType.PostgreSQL) {
            this.dbQuery = new PostgreSqlQuery();
        } else if (dbType == DbType.SQLServer) {
            this.dbQuery = new SQLServerQuery();
        } else if (dbType == DbType.DM) {
            this.dbQuery = new DmQuery();
        } else if (dbType == DbType.Clickhouse) {
            this.dbQuery = new ClickHouseQuery();
        }else if (dbType == DbType.KingBase) {
            this.dbQuery = new KingBaseSqlQuery();
        }

        this.connection = connection;
    }
}
