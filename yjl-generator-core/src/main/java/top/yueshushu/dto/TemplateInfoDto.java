package top.yueshushu.dto;

import lombok.Data;
import top.yueshushu.config.template.GeneratorInfo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>TemplateInfoDto此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Data
public class TemplateInfoDto  extends GeneratorInfo implements Serializable {
    /**
     * 关联的内容信息
     */
    private List<TemplateRelationContentDto> relationContentDtoList;

    private Map<String, Object> dataModel;
}
