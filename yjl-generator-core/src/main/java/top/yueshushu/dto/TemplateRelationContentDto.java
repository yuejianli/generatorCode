package top.yueshushu.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>TemplateRelationContentDto此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Data
public class TemplateRelationContentDto implements Serializable {
    private Long groupId;
    private String fileName;
    private String content;
    private String templateContent;
    private String originContent;
    private String generatorPath;
}
