package top.yueshushu.enums;

/**
 * 字段自动填充 枚举
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
public enum AutoFillEnum {
    DEFAULT, INSERT, UPDATE, INSERT_UPDATE;
}