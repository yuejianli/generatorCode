package top.yueshushu.enums;

/**
 * 代码生成方式 枚举
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
public enum GeneratorTypeEnum {
    /**
     * zip压缩包
     */
    ZIP_DOWNLOAD,
    /**
     * 自定义目录
     */
    CUSTOM_DIRECTORY;
}
