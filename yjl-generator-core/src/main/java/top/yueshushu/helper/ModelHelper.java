package top.yueshushu.helper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import top.yueshushu.common.GenerateCommon;
import top.yueshushu.common.utils.DateUtils;
import top.yueshushu.config.template.*;
import top.yueshushu.dao.*;
import top.yueshushu.dto.TemplateInfoDto;
import top.yueshushu.dto.TemplateRelationContentDto;
import top.yueshushu.service.*;
import top.yueshushu.util.TemplateUtils;
import top.yueshushu.vo.GenTableVO;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ModelHelper此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Component
@Slf4j
public class ModelHelper {

    @Resource
    private GenTableService genTableService;
    @Resource
    private GenTableFieldService genTableFieldService;
    @Resource
    private GenDatasourceService genDatasourceService;
    @Resource
    private GenFieldTypeService genFieldTypeService;
    @Resource
    private GenBaseClassService genBaseClassService;
    @Resource
    private CoreGeneratorConfig coreGeneratorConfig;
    @Resource
    private GenGroupService genGroupService;
    @Resource
    private GenGroupTemplateService genGroupTemplateService;

    public GeneratorInfo getGeneratorInfoByDb(Long datasourceId, Long tableId) {
        // 查询配置信息
        Long groupId = getGroupId(datasourceId);

        // 查询对应的组信息
        GenGroupDO genGroupDO = genGroupService.getById(groupId);
        // 查询出对应的明细
        List<GenGroupTemplateDO> genGroupTemplateDOList = genGroupTemplateService.listByGroupId(genGroupDO.getId());
        genGroupTemplateDOList = genGroupTemplateDOList.stream().filter(n-> n.getEnable() == 1).collect(Collectors.toList());
        // 封装成 GeneratorInfo
        GenTableDO tableDO = genTableService.getById(tableId);
        return coreGeneratorConfig.generatorConfigByDb(genGroupDO,genGroupTemplateDOList, tableDO);
    }

    public GeneratorInfo getGeneratorInfoByDb(Long datasourceId, GenTableDO tableDO) {
        // 查询配置信息
        Long groupId = getGroupId(datasourceId);

        // 查询对应的组信息
        GenGroupDO genGroupDO = genGroupService.getById(groupId);
        // 查询出对应的明细
        List<GenGroupTemplateDO> genGroupTemplateDOList = genGroupTemplateService.listByGroupId(genGroupDO.getId());
        genGroupTemplateDOList = genGroupTemplateDOList.stream().filter(n-> n.getEnable() == 1).collect(Collectors.toList());
        // 封装成 GeneratorInfo
        return coreGeneratorConfig.generatorConfigByDb(genGroupDO,genGroupTemplateDOList, tableDO);
    }
    /**
     * 获取组id
     * @param datasourceId 数据库id
     * @return 返回组id
     */
    private Long getGroupId(Long datasourceId) {
        Long groupId = null;
        if (datasourceId == null) {
            groupId = getDefaultGroupId();
        } else {
            // 查询对应的库关联的模板信息
            GenDatasourceDO genDatasourceDO = genDatasourceService.getById(datasourceId);
            if (genDatasourceDO == null || genDatasourceDO.getGroupId() == null) {
                groupId = getDefaultGroupId();
            } else {
                groupId = genDatasourceDO.getGroupId();
            }
        }
        return groupId;
    }


    private Long getDefaultGroupId () {
        return GenerateCommon.DEFAULT_GROUP_ID;
    }


    public TemplateInfoDto getDetailByTableId(Long tableId) {


        // 数据模型
        Map<String, Object> dataModel = getDataModel(tableId);

        // 代码生成器信息
        GenTableDO genTableDO = genTableService.getById(tableId);
        GeneratorInfo generator = getGeneratorInfoByDb(genTableDO.getDatasourceId(),tableId);

        // 更新信息
        ProjectInfo projectInfo = generator.getProject();
        Map<String, Object> projectInfoMap = BeanUtil.beanToMap(projectInfo);
       if (StringUtils.hasText(projectInfo.getPackageName())) {
           projectInfoMap.put("package", projectInfo.getPackageName());
           projectInfoMap.put("packagePath", projectInfo.getPackageName());
       }
        dataModel.putAll(projectInfoMap);

        // 根据表名, 进行填充对应的额外数据信息.

        if (StrUtil.isNotBlank(generator.getOtherParams())) {
            // 其他参数, 转换成 map
            Map<String, Object> map = JSONUtil.parseObj(generator.getOtherParams());
            dataModel.putAll(map);
        }

        // 对一些额外的默认配置进行处理.
        if (!dataModel.containsKey("view")) {
            dataModel.put("view","Response");
        }

        List<TemplateRelationContentDto> relationContentDtoList = new ArrayList<>();
        // 渲染模板并输出
        for (TemplateInfo template : generator.getTemplates()) {
           try {
               dataModel.put("templateName", template.getTemplateName());
               // 处理一下 fullName 的信息值.
               String fileType = template.getTemplateName().split("\\.")[1];
               String fullName = getFullName(template.getFullName(), dataModel,fileType).toString();
               dataModel.put("fullName",fullName);

               String content = TemplateUtils.getContent(template.getTemplateContent(), dataModel);
               String path = TemplateUtils.getContent(template.getGeneratorPath(), dataModel);

               TemplateRelationContentDto templateRelationContentDto = new TemplateRelationContentDto();
               templateRelationContentDto.setContent(content);
               templateRelationContentDto.setTemplateContent(content);
               templateRelationContentDto.setFileName(fullName+ "." + fileType);
               templateRelationContentDto.setOriginContent(template.getTemplateContent());
               templateRelationContentDto.setGeneratorPath(convertFilePath(path));
               templateRelationContentDto.setGroupId(generator.getGroupId());
               relationContentDtoList.add(templateRelationContentDto);
           } catch (Exception e) {
               log.error("模板{} 有异常 {}", template, e.getMessage(),e);
           }
        }

        TemplateInfoDto templateInfoDto = new TemplateInfoDto();
        BeanUtil.copyProperties(generator,templateInfoDto);
        templateInfoDto.setDataModel(dataModel);
        templateInfoDto.setRelationContentDtoList(relationContentDtoList);

        return templateInfoDto;
    }

    /**
     * 转换文件的路径
     * @param path 路径
     * @return
     */
    private String convertFilePath(String path) {
        StringBuilder sb = new StringBuilder(path);
        int lastIndex = path.lastIndexOf('.');
        for (int i = 0; i < lastIndex; i++) {
            if (path.charAt(i) == '.') {
                // 你可以替换为你想要的字符
                sb.setCharAt(i, '/');
            }
        }
        return sb.toString();
    }

    private Object getFullName(String fullName, Map<String, Object> dataModel,String fileType) {
        String className = dataModel.get("className").toString();
        String upperClassName = dataModel.get("ClassName").toString();
        String templateName = dataModel.get("templateName").toString();
        fullName = Optional.ofNullable(fullName).orElse("");

        if (fullName.contains("%s")) {
            return fullName.replaceAll("%s",className);
        } else if (fullName.contains("%S")) {
            return fullName.replaceFirst("%S", upperClassName);
        } else {
           if (fileType.contains("java") || fileType.contains("xml")) {
               return upperClassName + templateName.split("\\.")[0];
           } else {
               return fullName;
           }
        }
    }


    /**
     * 获取渲染的数据模型, 这是主要的方法.
     * 其他的,都需要在这里面进行配置的.
     *
     * @param tableId 表ID
     */
    public Map<String, Object> getDataModel(Long tableId) {
        // 表信息
        GenTableVO table = genTableService.getVOById(tableId);
        List<GenTableFieldDO> fieldList = genTableFieldService.getByTableId(tableId);


        table.setFieldList(fieldList);

        // 数据模型
        Map<String, Object> dataModel = new HashMap<>();

        // 获取数据库类型
        String dbType = genDatasourceService.getDatabaseProductName(table.getDatasourceId());
        dataModel.put("dbType", dbType);

        // 项目信息
        dataModel.put("package", table.getPackageName());
        dataModel.put("packagePath", table.getPackageName().replace(".", "/"));
        dataModel.put("version", table.getVersion());
        dataModel.put("moduleName", table.getModuleName());
        dataModel.put("ModuleName", StrUtil.upperFirst(table.getModuleName()));
        dataModel.put("functionName", table.getFunctionName());
        dataModel.put("FunctionName", StrUtil.upperFirst(table.getFunctionName()));
        dataModel.put("formLayout", table.getFormLayout());

        // 开发者信息
        dataModel.put("author", table.getAuthor());
        dataModel.put("email", table.getEmail());
        dataModel.put("datetime", DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN));
        dataModel.put("date", DateUtils.format(new Date(), DateUtils.DATE_PATTERN));

        // 设置字段分类
        setFieldTypeList(dataModel, table);

        // 设置基类信息
        setBaseClass(dataModel, table);

        // 导入的包列表
        List<String> importList = genFieldTypeService.getPackageByTableId(table.getId());
        dataModel.put("importList", importList);

        // 表信息
        dataModel.put("tableName", table.getTableName());
        dataModel.put("tableComment", table.getTableComment());
        dataModel.put("className", StrUtil.lowerFirst(table.getClassName()));
        dataModel.put("ClassName", StrUtil.upperFirst(table.getClassName()));
        dataModel.put("fieldList", table.getFieldList());
        // 生成路径
        dataModel.put("backendPath", table.getBackendPath());
        dataModel.put("frontendPath", table.getFrontendPath());

        dataModel.put("tableNameHump",StrUtil.toCamelCase(StrUtil.lowerFirst(table.getClassName())));

        return dataModel;
    }


    /**
     * 设置基类信息
     *
     * @param dataModel 数据模型
     * @param table     表
     */
    public void setBaseClass(Map<String, Object> dataModel, GenTableVO table) {
        if (table.getBaseclassId() == null) {
            return;
        }

        // 基类
        GenBaseClassDO baseClass = genBaseClassService.getById(table.getBaseclassId());
        baseClass.setPackageName(baseClass.getPackageName());
        dataModel.put("baseClass", baseClass);

        // 基类字段
        String[] fields = baseClass.getFields().split(",");

        // 标注为基类字段
        for (GenTableFieldDO field : table.getFieldList()) {
            if (ArrayUtil.contains(fields, field.getFieldName())) {
                field.setBaseField(1);
            } else {
                field.setBaseField(0);
            }
        }
    }

    /**
     * 设置字段分类信息
     *
     * @param dataModel 数据模型
     * @param table     表
     */
    public void setFieldTypeList(Map<String, Object> dataModel, GenTableVO table) {
        // 主键列表 (支持多主键)
        List<GenTableFieldDO> primaryList = new ArrayList<>();
        // 表单列表
        List<GenTableFieldDO> formList = new ArrayList<>();
        // 网格列表
        List<GenTableFieldDO> gridList = new ArrayList<>();
        // 查询列表
        List<GenTableFieldDO> queryList = new ArrayList<>();

        for (GenTableFieldDO field : table.getFieldList()) {
            if (field.getPrimaryPk()!= null && field.getPrimaryPk() == 1) {
                primaryList.add(field);
            }
            if (field.getFormItem() != null && field.getFormItem() == 1) {
                formList.add(field);
            }
            if (field.getGridItem() != null && field.getGridItem() == 1) {
                gridList.add(field);
            }
            if (field.getQueryItem() != null && field.getQueryItem() == 1) {
                queryList.add(field);
            }
        }
        dataModel.put("primaryList", primaryList);
        dataModel.put("formList", formList);
        dataModel.put("gridList", gridList);
        dataModel.put("queryList", queryList);
    }
}
