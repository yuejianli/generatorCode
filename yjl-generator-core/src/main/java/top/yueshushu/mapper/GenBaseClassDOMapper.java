package top.yueshushu.mapper;

import top.yueshushu.dao.GenBaseClassDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 基类管理 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenBaseClassDOMapper extends BaseMapper<GenBaseClassDO> {

}
