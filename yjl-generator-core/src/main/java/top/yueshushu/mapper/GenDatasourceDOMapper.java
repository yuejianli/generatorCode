package top.yueshushu.mapper;

import top.yueshushu.dao.GenDatasourceDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 数据源管理 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenDatasourceDOMapper extends BaseMapper<GenDatasourceDO> {

}
