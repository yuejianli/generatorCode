package top.yueshushu.mapper;

import org.apache.ibatis.annotations.Param;
import top.yueshushu.dao.GenFieldTypeDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 字段类型管理 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenFieldTypeDOMapper extends BaseMapper<GenFieldTypeDO> {

    List<String> getPackageByTableId(@Param("tableId") Long tableId);

}
