package top.yueshushu.mapper;

import top.yueshushu.dao.GenGroupDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模板组 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenGroupDOMapper extends BaseMapper<GenGroupDO> {

}
