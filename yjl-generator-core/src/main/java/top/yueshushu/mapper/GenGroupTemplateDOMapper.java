package top.yueshushu.mapper;

import top.yueshushu.dao.GenGroupTemplateDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模板组名称 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenGroupTemplateDOMapper extends BaseMapper<GenGroupTemplateDO> {

}
