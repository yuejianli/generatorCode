package top.yueshushu.mapper;

import top.yueshushu.dao.GenProjectModifyDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目名变更 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenProjectModifyDOMapper extends BaseMapper<GenProjectModifyDO> {

}
