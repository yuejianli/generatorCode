package top.yueshushu.mapper;

import top.yueshushu.dao.GenTableDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 代码生成表 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenTableDOMapper extends BaseMapper<GenTableDO> {

}
