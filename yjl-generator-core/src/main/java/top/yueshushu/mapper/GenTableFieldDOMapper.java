package top.yueshushu.mapper;

import top.yueshushu.dao.GenTableFieldDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 代码生成表字段 Mapper 接口
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenTableFieldDOMapper extends BaseMapper<GenTableFieldDO> {

}
