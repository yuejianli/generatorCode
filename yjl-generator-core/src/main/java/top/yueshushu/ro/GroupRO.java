package top.yueshushu.ro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.yueshushu.common.query.Query;

import java.io.Serializable;

/**
 * <p>GroupRO此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Data
@ApiModel("组查询对象")
public class GroupRO extends Query implements Serializable {
    @ApiModelProperty(name = "name", value = "名称")
    private String name;

    @ApiModelProperty(name = "enableType", value = "是否启用")
    private Integer enableType;
}
