package top.yueshushu.ro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>ScrewDocumentRO此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@remark:</p>
 */
@Data
@ApiModel("screw 生成文档对象")
public class ScrewDocumentRO implements Serializable {
    @ApiModelProperty(name = "id", value = "数据库id")
    private Long id;
    @ApiModelProperty(name = "fileName", value = "文件名称")
    private String fileName;
    @ApiModelProperty(name = "fileType", value = "生成文件类型,支持 md html doc")
    private String fileType = "md";
    @ApiModelProperty(name = "ignoreTableName", value = "忽略表名,字符串,号拼接")
    private String ignoreTableName;
    @ApiModelProperty(name = "ignorePrefix", value = "忽略前缀,字符串,号拼接")
    private String ignorePrefix;
    @ApiModelProperty(name = "ignoreSuffix", value = "忽略后续,字符串,号拼接")
    private String ignoreSuffix;
    @ApiModelProperty(name = "version", value = "版本号")
    private String version = "1.0.0";
    @ApiModelProperty(name = "description", value = "描述")
    private String description;

    private List<String> tableNameList;
    private List<String> tablePrefixList;
    private List<String> tableSuffixList;

    private String filePath;

}
