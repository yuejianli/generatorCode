package top.yueshushu.service;

import cn.hutool.http.server.HttpServerResponse;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenDatasourceDO;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yueshushu.dto.GenDatasourceDTO;
import top.yueshushu.ro.ScrewDocumentRO;

/**
 * <p>
 * 数据源管理 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenDatasourceService extends IService<GenDatasourceDO> {

    PageResult<GenDatasourceDO> pageByQuery(Query query);

    GenDatasourceDTO getDtoById(Long datasourceId);


    /**
     * 获取数据库产品名，如：MySQL
     *
     * @param datasourceId 数据源ID
     * @return 返回产品名
     */
    String getDatabaseProductName(Long datasourceId);

}
