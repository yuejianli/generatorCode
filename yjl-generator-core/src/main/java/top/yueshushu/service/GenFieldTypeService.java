package top.yueshushu.service;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenFieldTypeDO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 字段类型管理 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenFieldTypeService extends IService<GenFieldTypeDO> {

    PageResult<GenFieldTypeDO> pageByQuery(Query query);

    Map<String, GenFieldTypeDO> convertAllToMap();

    /**
     * 根据tableId，获取包列表
     */
    List<String> getPackageByTableId(Long tableId);
}
