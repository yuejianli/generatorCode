package top.yueshushu.service;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenGroupDO;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.ro.GroupRO;

/**
 * <p>
 * 模板组 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenGroupService extends IService<GenGroupDO> {

    PageResult<GenGroupDO> pageByQuery(GroupRO groupRO);


}
