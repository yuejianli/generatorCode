package top.yueshushu.service;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenGroupTemplateDO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 模板组名称 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenGroupTemplateService extends IService<GenGroupTemplateDO> {

    PageResult<GenGroupTemplateDO> pageByQuery(Query query);


    List<GenGroupTemplateDO> listByGroupId(Long groupId);

}
