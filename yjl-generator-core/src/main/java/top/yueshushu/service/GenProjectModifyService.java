package top.yueshushu.service;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.dao.GenProjectModifyDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目名变更 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenProjectModifyService extends IService<GenProjectModifyDO> {

    PageResult<GenProjectModifyDO> pageByQuery(Query query);
}
