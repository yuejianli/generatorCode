package top.yueshushu.service;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dao.GenTableFieldDO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成表字段 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenTableFieldService extends IService<GenTableFieldDO> {

    PageResult<GenTableFieldDO> pageByQuery(Query query);

    List<GenTableFieldDO> getByTableId(Long tableId);

    void removeByTableIds(List<Long> tableIds);

    void initFieldList(List<GenTableFieldDO> dbTableFieldList, Map<String, GenFieldTypeDO> fieldTypeDOMap);

}
