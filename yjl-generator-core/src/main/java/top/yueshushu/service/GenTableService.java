package top.yueshushu.service;

import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenTableDO;
import com.baomidou.mybatisplus.extension.service.IService;
import top.yueshushu.vo.GenTableVO;

/**
 * <p>
 * 代码生成表 服务类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
public interface GenTableService extends IService<GenTableDO> {

    PageResult<GenTableDO> pageByQuery(Query query);

    GenTableDO getByTableName(Long datasourceId, String tableName);

    GenTableVO getVOById(Long tableId);


}
