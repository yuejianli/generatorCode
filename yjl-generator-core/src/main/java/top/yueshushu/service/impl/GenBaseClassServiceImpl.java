package top.yueshushu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenBaseClassDO;
import top.yueshushu.mapper.GenBaseClassDOMapper;
import top.yueshushu.service.GenBaseClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基类管理 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenBaseClassServiceImpl extends BaseServiceImpl<GenBaseClassDOMapper, GenBaseClassDO> implements GenBaseClassService {

    @Override
    public PageResult<GenBaseClassDO> pageByQuery(Query query) {

        IPage<GenBaseClassDO> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );

        return new PageResult<>(page.getRecords(), page.getTotal());
    }
}
