package top.yueshushu.service.impl;

import cn.hutool.http.server.HttpServerResponse;
import com.baomidou.mybatisplus.core.metadata.IPage;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.config.DbType;
import top.yueshushu.dao.GenBaseClassDO;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dto.GenDatasourceDTO;
import top.yueshushu.mapper.GenDatasourceDOMapper;
import top.yueshushu.ro.ScrewDocumentRO;
import top.yueshushu.service.GenDatasourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据源管理 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenDatasourceServiceImpl extends BaseServiceImpl<GenDatasourceDOMapper, GenDatasourceDO> implements GenDatasourceService {

    @Override
    public PageResult<GenDatasourceDO> pageByQuery(Query query) {
        IPage<GenDatasourceDO> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );

        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public GenDatasourceDTO getDtoById(Long datasourceId) {
        return new GenDatasourceDTO(getById(datasourceId));
    }

    @Override
    public String getDatabaseProductName(Long datasourceId) {
        if (datasourceId.intValue() == 0) {
            return DbType.MySQL.name();
        } else {
            return getById(datasourceId).getDbType();
        }
    }
}
