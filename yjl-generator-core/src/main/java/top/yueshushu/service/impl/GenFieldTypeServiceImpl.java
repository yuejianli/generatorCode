package top.yueshushu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenDatasourceDO;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.mapper.GenFieldTypeDOMapper;
import top.yueshushu.service.GenFieldTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 字段类型管理 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenFieldTypeServiceImpl extends BaseServiceImpl<GenFieldTypeDOMapper, GenFieldTypeDO> implements GenFieldTypeService {
    @Resource
    private GenFieldTypeDOMapper genFieldTypeDOMapper;

    @Override
    public PageResult<GenFieldTypeDO> pageByQuery(Query query) {
        IPage<GenFieldTypeDO> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public Map<String, GenFieldTypeDO> convertAllToMap() {
        List<GenFieldTypeDO> doList = list();
        Map<String, GenFieldTypeDO> map = new LinkedHashMap<>(doList.size());
        for (GenFieldTypeDO entity : doList) {
            map.put(entity.getColumnType().toLowerCase(), entity);
        }
        return map;
    }

    @Override
    public List<String> getPackageByTableId(Long tableId) {
        return genFieldTypeDOMapper.getPackageByTableId(tableId);
    }
}
