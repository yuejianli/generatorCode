package top.yueshushu.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.mapper.GenGroupDOMapper;
import top.yueshushu.ro.GroupRO;
import top.yueshushu.service.GenGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模板组 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenGroupServiceImpl extends BaseServiceImpl<GenGroupDOMapper, GenGroupDO> implements GenGroupService {

    @Override
    public PageResult<GenGroupDO> pageByQuery(GroupRO query) {
        QueryWrapper<GenGroupDO> wrapper = getWrapper(query);
        wrapper.like(StrUtil.isNotBlank(query.getName()), "name", query.getConnName());
        wrapper.eq(query.getEnableType() != null, "enable", query.getEnableType());

        IPage<GenGroupDO> page = baseMapper.selectPage(
                getPage(query), wrapper
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }
}
