package top.yueshushu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenGroupDO;
import top.yueshushu.dao.GenGroupTemplateDO;
import top.yueshushu.mapper.GenGroupTemplateDOMapper;
import top.yueshushu.service.GenGroupTemplateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 模板组名称 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenGroupTemplateServiceImpl extends BaseServiceImpl<GenGroupTemplateDOMapper, GenGroupTemplateDO> implements GenGroupTemplateService {

    @Override
    public PageResult<GenGroupTemplateDO> pageByQuery(Query query) {
        IPage<GenGroupTemplateDO> page = baseMapper.selectPage(
                getPage(query), null
        );

        List<GenGroupTemplateDO> sortNumList = page.getRecords().stream().sorted(Comparator.comparing(GenGroupTemplateDO::getOrderNum)).collect(Collectors.toList());

        return new PageResult<>(sortNumList, page.getTotal());
    }

    @Override
    public List<GenGroupTemplateDO> listByGroupId(Long groupId) {
        return this.lambdaQuery()
                .eq(groupId != null , GenGroupTemplateDO::getGroupId, groupId)
                .orderByAsc(GenGroupTemplateDO::getOrderNum)
                .list();

    }

}
