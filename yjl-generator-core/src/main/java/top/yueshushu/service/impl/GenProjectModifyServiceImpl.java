package top.yueshushu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenProjectModifyDO;
import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.mapper.GenProjectModifyDOMapper;
import top.yueshushu.service.GenProjectModifyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目名变更 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenProjectModifyServiceImpl extends BaseServiceImpl<GenProjectModifyDOMapper, GenProjectModifyDO> implements GenProjectModifyService {

    @Override
    public PageResult<GenProjectModifyDO> pageByQuery(Query query) {

        IPage<GenProjectModifyDO> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }
}
