package top.yueshushu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dao.GenTableFieldDO;
import top.yueshushu.enums.AutoFillEnum;
import top.yueshushu.mapper.GenTableFieldDOMapper;
import top.yueshushu.service.GenTableFieldService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 * 代码生成表字段 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenTableFieldServiceImpl extends BaseServiceImpl<GenTableFieldDOMapper, GenTableFieldDO> implements GenTableFieldService {

    @Override
    public PageResult<GenTableFieldDO> pageByQuery(Query query) {

        IPage<GenTableFieldDO> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public List<GenTableFieldDO> getByTableId(Long tableId) {
        return this.lambdaQuery()
                .eq(GenTableFieldDO::getTableId, tableId)
                .list();
    }

    @Override
    public void removeByTableIds(List<Long> tableIds) {
         this.lambdaUpdate()
                .in(GenTableFieldDO::getTableId, tableIds)
                .remove();
    }

    @Override
    public void initFieldList(List<GenTableFieldDO> dbTableFieldList, Map<String, GenFieldTypeDO> fieldTypeDOMap) {

        AtomicInteger atomicInteger = new AtomicInteger(0);

        for (GenTableFieldDO field : dbTableFieldList) {
            field.setAttrName(StringUtils.underlineToCamel(field.getFieldName()));
            // 获取字段对应的类型
            GenFieldTypeDO fieldTypeMapping = fieldTypeDOMap.get(field.getFieldType().toLowerCase());
            if (fieldTypeMapping == null) {
                // 没找到对应的类型，则为Object类型
                field.setAttrType("Object");
            } else {
                field.setAttrType(fieldTypeMapping.getAttrType());
                field.setPackageName(fieldTypeMapping.getPackageName());
            }

            field.setAutoFill(AutoFillEnum.DEFAULT.name());
            field.setFormItem(1);
            field.setGridItem(1);
            field.setQueryType("=");
            field.setQueryFormType("text");
            field.setFormType("text");
            field.setSort( atomicInteger.incrementAndGet());
        }
    }

}
