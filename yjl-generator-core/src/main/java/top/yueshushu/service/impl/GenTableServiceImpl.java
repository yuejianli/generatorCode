package top.yueshushu.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.util.CollectionUtils;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.query.Query;
import top.yueshushu.common.service.impl.BaseServiceImpl;
import top.yueshushu.dao.GenFieldTypeDO;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.mapper.GenTableDOMapper;
import top.yueshushu.service.GenTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.yueshushu.vo.GenTableVO;

import java.util.List;

/**
 * <p>
 * 代码生成表 服务实现类
 * </p>
 *
 * @author 两个蝴蝶飞
 * @since 2024-03-28
 */
@Service
public class GenTableServiceImpl extends BaseServiceImpl<GenTableDOMapper, GenTableDO> implements GenTableService {

    @Override
    public PageResult<GenTableDO> pageByQuery(Query query) {

        query.setTableLikeName(query.getTableName());
        query.setTableName(null);
        IPage<GenTableDO> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public GenTableDO getByTableName(Long datasourceId,String tableName) {
        Query query = new Query();
        query.setTableName(tableName);
        QueryWrapper<GenTableDO> wrapper = getWrapper(query);
        wrapper.eq(datasourceId != null, "datasource_id", datasourceId);
        List<GenTableDO> genTableDOList = baseMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(genTableDOList)) {
            return null;
        }
        return genTableDOList.get(0);

    }

    @Override
    public GenTableVO getVOById(Long tableId) {

        GenTableDO genTableDO = getById(tableId);

        GenTableVO genTableVO = new GenTableVO();

        BeanUtil.copyProperties(genTableDO, genTableVO);

        return genTableVO;

    }
}
