package top.yueshushu.util;
import top.yueshushu.config.DbType;
import top.yueshushu.dto.GenDatasourceDTO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DB工具类
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
public class DbUtils {
    private static final int CONNECTION_TIMEOUTS_SECONDS = 6;

    /**
     * 获得数据库连接
     */
    public static Connection getConnection(GenDatasourceDTO dataSource) throws ClassNotFoundException, SQLException {
        DriverManager.setLoginTimeout(CONNECTION_TIMEOUTS_SECONDS);
        Class.forName(dataSource.getDbType().getDriverClass());

        Connection connection = DriverManager.getConnection(dataSource.getConnUrl(), dataSource.getUsername(), dataSource.getPassword());
        //todo yjl 对oralce 数据库的处理
        //        if (dataSource.getDbType() == DbType.Oracle) {
//            ((OracleConnection) connection).setRemarksReporting(true);
//        }

        return connection;
    }


}