package top.yueshushu.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.yueshushu.dao.GenTableDO;
import top.yueshushu.dao.GenTableFieldDO;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("表携带结构响应对象")
public class GenTableVO extends GenTableDO implements Serializable {

    @ApiModelProperty(name = "fieldList", value = "结构响应对象")
    private List<GenTableFieldDO> fieldList;


}
