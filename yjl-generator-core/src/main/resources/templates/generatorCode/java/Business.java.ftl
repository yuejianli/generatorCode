package ${package}.${moduleName}.${businessPackageName};

import java.util.List;
import ${package}.${moduleName}.${roPackageName}.${roClassName};
import ${package}.${moduleName}.${voPackageName}.${voClassName};
import com.uiotsoft.framework.core.response.PageResult;
/**
* ${tableComment} Business 业务编排层接口
*
* @author ${author} ${email}
* @since ${version} ${date}
*/

public interface ${fullName}{

        /**
        查询符合条件的分页数据信息
        @param ${lroClassName} 前端传入参数封装的对象
        @return 返回符合条件的分页数据信息
        */
        PageResult<${voClassName}>  pageByCondition( ${roClassName} ${lroClassName});


        /**
        查询符合条件的全部数据信息
        @param ${lroClassName} 前端传入参数封装的对象
        @return 返回符合条件的全部数据信息
        */
       List<${voClassName}>  findList( ${roClassName} ${lroClassName});

        /**
           查询单个数据信息
           @param ${lroClassName} 前端传入参数封装的对象
           @return 返回单个对象
        */
        ${voClassName} getVO( ${roClassName} ${lroClassName});


        /**
            保存数据到数据库里面
            @param ${lroClassName} 前端传入参数封装的对象
            @return 保存数据到数据库里面
        */
       boolean  save( ${roClassName} ${lroClassName});

        /**
        修改数据到数据库里面
        @param ${lroClassName} 前端传入参数封装的对象
        @return 修改数据到数据库里面
        */
        boolean  update( ${roClassName} ${lroClassName});

        /**
        批量删除数据
        @param idList idList
        @return 根据id List 批量删除数据
        */
        Integer  delete( List<Integer> idList);

}