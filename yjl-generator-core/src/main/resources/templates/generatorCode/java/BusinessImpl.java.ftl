package top.yueshushu.demo.business.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import top.yueshushu.common.page.PageResult;
import javax.annotation.Resource;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import ${package}.${moduleName}.${businessPackageName}.${businessClassName};
import ${package}.${moduleName}.${roPackageName}.${roClassName};
import ${package}.${moduleName}.${voPackageName}.${voClassName};

import ${package}.${moduleName}.${convertPackageName}.${convertClassName};
import ${package}.${moduleName}.${doPackageName}.${doClassName};
import ${package}.${moduleName}.${paramPackageName}.${paramClassName};
import ${package}.${moduleName}.${servicePackageName}.${serviceClassName};

/**
* ${tableComment} Business 业务编排层实现
*
* @author ${author} ${email}
* @since ${version} ${date}
*/

@Service
@Slf4j
public class ${fullName} implements ${businessClassName} {

    @Resource
    private ${serviceClassName} ${lserviceClassName};
    @Resource
    private ${convertClassName} ${lconvertClassName};


    @Override
    public PageResult<${voClassName}> pageByCondition(${roClassName} ${lroClassName}) {
        Page<Object> pageInfo = PageHelper.startPage(1, 10);
        ${paramClassName} ${lparamClassName} = ${lconvertClassName}.convert${view}(${lroClassName});
        List<${doClassName}> ${ldoClassName}List = ${lserviceClassName}.listByCondition(${lparamClassName});
        List<${voClassName}> voList = ${lconvertClassName}.convert${view}List(${lconvertClassName}.convertDTOList(${ldoClassName}List));
        return new PageResult<>(voList, pageInfo.size());
    }

    @Override
    public List<${voClassName}> findList(${roClassName} ${lroClassName}) {
        ${paramClassName} ${lparamClassName} = ${lconvertClassName}.convert${view}(${lroClassName});
        List<${doClassName}> ${ldoClassName}List = ${lserviceClassName}.listByCondition(${lparamClassName});
        return ${lconvertClassName}.convert${view}List(${lconvertClassName}.convertDTOList(${ldoClassName}List));
    }

    @Override
    public ${voClassName} getVO(${roClassName} ${lroClassName}) {
        ${paramClassName} ${lparamClassName} = ${lconvertClassName}.convert${view}(${lroClassName});
        ${doClassName} ${ldoClassName} = ${lserviceClassName}.getByCondition(${lparamClassName});
        return ${lconvertClassName}.convert${view}(${lconvertClassName}.convertDTO(${ldoClassName}));
    }

    @Override
    public boolean save(${roClassName} ${lroClassName}) {
        ${doClassName} ${ldoClassName} = ${lconvertClassName}.convertDO(${lroClassName});
        return ${lserviceClassName}.save(${ldoClassName});
    }

    @Override
    public boolean update(${roClassName} ${lroClassName}) {
        ${doClassName} ${ldoClassName} = ${lconvertClassName}.convertDO(${lroClassName});
        return ${lserviceClassName}.updateById(${ldoClassName});
    }

    @Override
    public boolean delete(List<Integer> idList) {
        return ${lserviceClassName}.removeByIds(idList);
    }
}
