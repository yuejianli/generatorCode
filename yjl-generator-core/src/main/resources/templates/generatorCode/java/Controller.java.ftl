package top.yueshushu.demo.controller;


import org.springframework.web.bind.annotation.*;
import top.yueshushu.common.page.PageResult;
import top.yueshushu.common.utils.OutputResult;

import ${package}.${moduleName}.${businessPackageName}.${businessClassName};
import ${package}.${moduleName}.${roPackageName}.${roClassName};
import ${package}.${moduleName}.${voPackageName}.${voClassName};

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
<#if swagger2>
import io.swagger.annotations.*;
</#if>

/**
* ${tableComment} Controller 前端控制器
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
<#if swagger2>
    @Api(value = "${tableNameHump}", tags = "${tableComment!} 相关接口")
</#if>
@RestController
@RequestMapping("/${tableNameHump}")
public class ${fullName} {

    @Resource
    private ${businessClassName} ${lbusinessClassName};

    <#if swagger2>
        @ApiOperation(value = "分页查询", notes = "分页查询")
        @ApiResponses(
        @ApiResponse(
        code = 200,
        message = "查询成功",
        response = ${voClassName}.class
        )
        )
    <#else>
    /**
        分页查询
    */
    </#if>

    @PostMapping("/page")
    public OutputResult<PageResult<${voClassName}>> page(GenTestStudentRO ${lroClassName}) {
        return OutputResult.ok(${lbusinessClassName}.pageByCondition(${lroClassName}));
    }


<#if swagger2>
    @ApiOperation(value = "全部查询", notes = "全部查询")
    @ApiResponses(
    @ApiResponse(
    code = 200,
    message = "查询成功",
    response = ${voClassName}.class
    )
    )
<#else>
    /**
    全部查询
    */
</#if>

    @PostMapping("/list")
    public OutputResult<List<${voClassName}>> list(@RequestBody ${roClassName} ${lroClassName}) {
        return OutputResult.ok(${lbusinessClassName}.findList(${lroClassName}));

    }


<#if swagger2>
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @ApiImplicitParams(
    @ApiImplicitParam(
    name = "id", value = "id",required = true, dataType = "Integer", paramType = "query" )
    )
    @ApiResponses(
    @ApiResponse(
    code = 200,
    message = "查询成功",
    response = ${voClassName}.class
    )
    )
<#else>
    /**
    根据id查询
    */
</#if>
    @GetMapping("/{id}")
    public OutputResult<${voClassName}> get(@PathVariable("id") Long id) {
        ${roClassName} ${lroClassName} = new ${roClassName}();
        ${lroClassName}.setId(id);
        return OutputResult.ok(${lbusinessClassName}.getVO(${lroClassName}));
    }

<#if swagger2>
    @ApiOperation(value = "新增 ${tableComment!} 表数据")
<#else>
    /**
    新增 ${tableComment!} 表数据
    */
</#if>
    @PostMapping
    public OutputResult save(@RequestBody ${roClassName} ${lroClassName}) {
        boolean flagResult  = ${lbusinessClassName}.save(${lroClassName});
        return flagResult ? OutputResult.ok(): OutputResult.error();
    }

<#if swagger2>
    @ApiOperation(value = "修改 ${tableComment!} 表数据")
<#else>
    /**
    修改 ${tableComment!} 表数据
    */
</#if>
    @PutMapping
    public OutputResult update(@RequestBody ${roClassName} ${lroClassName}){
        boolean flagResult  = ${lbusinessClassName}.update(${lroClassName});
        return flagResult ? OutputResult.ok(): OutputResult.error();
    }

<#if swagger2>
    @ApiOperation(value = "根据id删除 ${tableComment!} 表数据")
<#else>
    /**
    根据id删除 ${tableComment!} 表数据
    */
</#if>
    @DeleteMapping("/{id}")
    public OutputResult delete(@PathVariable Integer id){
        boolean flagResult = ${lbusinessClassName}.delete(Collections.singletonList(id));
        return flagResult ? OutputResult.ok(): OutputResult.error();
    }
}