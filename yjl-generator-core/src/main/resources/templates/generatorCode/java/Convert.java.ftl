package ${package}.${moduleName}.${convertPackageName};

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

import ${package}.${moduleName}.${doPackageName}.${doClassName};
import ${package}.${moduleName}.${dtoPackageName}.${dtoClassName};
import ${package}.${moduleName}.${voPackageName}.${voClassName};

/**
* ${tableComment} Convert 转换器
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Mapper
public interface ${fullName} {

    ${convertClassName} INSTANCE = Mappers.getMapper(${convertClassName}.class);

    /**
    dto 转换DO
    @param ${ldtoClassName} dto对象
    @return dto 转换成 DO
    */
    ${doClassName} convertDO(${dtoClassName} ${ldtoClassName});

    /**
    dto list 转换成 DO list
    @param ${ldtoClassName}List dto对象集合
    @return dto list 转换成 DO list
    */
    List<${doClassName}> convertDOList(List<${dtoClassName}> ${ldtoClassName}List);


    /**
    do 转换DTO
    @param ${ldoClassName} do对象
    @return do 转换成 DTO
    */
    ${dtoClassName} convertDTO(${doClassName} ${ldoClassName});

    /**
    do list 转换成 DTO list
    @param ${ldoClassName}List do对象集合
    @return do list 转换成 DTO list
    */
    List<${dtoClassName}> convertDTOList(List<${doClassName}> ${ldoClassName}List);



    /**
    dto 转换${view}
    @param ${ldtoClassName} dto对象
    @return do转换成 ${view}
    */
    ${voClassName} convert${view}(${dtoClassName} ${ldtoClassName});

    /**
    dto list 转换成 ${view} list
    @param ${ldtoClassName}List do对象集合
    @return dto list 转换成 ${view} list
    */
    List<${voClassName}> convert${view}List(List<${dtoClassName}> ${ldtoClassName}List);


}