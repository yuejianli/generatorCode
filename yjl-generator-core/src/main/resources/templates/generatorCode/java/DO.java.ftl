package ${package}.${moduleName}.${doPackageName};

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;
<#list importList as i>
import ${i!};
</#list>
<#if baseClass??>
import ${baseClass.packageName}.${baseClass.code};
</#if>
<#if swagger2>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>

/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Data
@TableName("${tableName}")
<#if swagger2>
@ApiModel(value="${tableName}对象", description="${tableComment!}")
</#if>
public class ${fullName} <#if baseClass??> extends ${baseClass.code}</#if> implements Serializable {
    private static final long serialVersionUID = 1L;

<#list fieldList as field>
    <#if !field.baseFieldb>
        <#if field.fieldComment!?length gt 0>
            <#if swagger2>
    @ApiModelProperty(value = "${field.fieldComment}")
            <#else>
    /**
    * ${field.fieldComment}
    */
            </#if>
        </#if>
        <#if field.primaryPkb>
    @TableId(value = "${field.attrName}", type = IdType.AUTO)
    private ${field.attrType} ${field.attrName};
        <#else>
    @TableField("${field.attrName}")
    private ${field.attrType} ${field.attrName};
        </#if>
    </#if>

</#list>
}