package ${package}.${moduleName}.${mapperPackageName};

import ${package}.framework.mybatis.dao.BaseDao;
import ${package}.${moduleName}.${doPackageName}.${doClassName};
import org.apache.ibatis.annotations.Mapper;

/**
* ${tableComment} Mapper
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
public interface ${fullName} extends BaseDao<${doClassName}> {
	
}