package ${package}.${moduleName}.${paramPackageName};

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
<#list importList as i>
import ${i!};
</#list>
<#if swagger2>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
/**
* ${tableComment}属性组装成对象查询
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
<#if swagger2>
@ApiModel(value="${tableName}对象查询", description="${tableComment!}条件查询对象")
</#if>
public class ${fullName} implements Serializable  {
<#list queryList as field>
    <#if field.fieldComment!?length gt 0>
        <#if swagger2>
    @ApiModelProperty(value = "${field.fieldComment}")
        <#else>
    /**
    * ${field.fieldComment}
    */
        </#if>
    </#if>
    <#if field.queryFormType == 'date'>
    @DateTimeFormat(pattern="yyyy-MM-dd")
    <#elseif field.queryFormType == 'datetime'>
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    </#if>
    private ${field.attrType}<#if field.queryFormType == 'date' || field.queryFormType == 'datetime'>[]</#if> ${field.attrName};

</#list>
}