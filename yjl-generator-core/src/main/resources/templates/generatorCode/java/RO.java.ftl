package ${package}.${moduleName}.${roPackageName};

import lombok.Data;
import java.io.Serializable;

<#if swagger2>
	import io.swagger.annotations.ApiModel;
	import io.swagger.annotations.ApiModelProperty;
</#if>

/**
* ${tableComment} RO 接收请求参数对象
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Data
<#if swagger2>
	@ApiModel(value="${tableName}接收请求参数对象", description="${tableComment!}")
</#if>
public class ${fullName}  implements Serializable {
private static final long serialVersionUID = 1L;

<#list fieldList as field>
	<#if field.fieldComment!?length gt 0>
		<#if swagger2>
			@ApiModelProperty(value = "${field.fieldComment}")
		<#else>
			/**
			* ${field.fieldComment}
			*/
		</#if>
	</#if>
	<#if field.attrType == 'Date'>
		private String ${field.attrName};
	<#else>
		private ${field.attrType} ${field.attrName};
	</#if>
</#list>
}