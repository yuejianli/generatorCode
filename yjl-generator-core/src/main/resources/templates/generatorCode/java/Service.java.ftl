package ${package}.${moduleName}.${servicePackageName};

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import ${package}.${moduleName}.${doPackageName}.${doClassName};
import ${package}.${moduleName}.${paramPackageName}.${paramClassName};


/**
* ${tableComment} Service接口
*
* @author ${author} ${email}
* @since ${version} ${date}
*/

public interface ${fullName} extends IService<${doClassName}> {

        /**
        * 根据查询参数对象,查询出多个符合条件的数据
        * @param ${lparamClassName} ${tableComment}查询参数对象
        * @return 返回符合条件的多个数据
        */
        List<${doClassName}> listByCondition(${paramClassName} ${lparamClassName});

        /**
        * 根据查询参数对象,查询出符合条件的一个数据
        * @param ${lparamClassName} ${tableComment}查询参数对象
        * @return 返回符合条件的一个数据
        */
        ${doClassName} getByCondition(${paramClassName} ${lparamClassName});
}