package ${package}.${moduleName}.${serviceImplPackageName};

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import ${package}.${moduleName}.${doPackageName}.${doClassName};
import ${package}.${moduleName}.${paramPackageName}.${paramClassName};
import ${package}.${moduleName}.${mapperPackageName}.${mapperClassName};
import ${package}.${moduleName}.${servicePackageName}.${serviceClassName};

/**
* ${tableComment} Service 接口实现
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Service
public class ${fullName} extends ServiceImpl<${mapperClassName}, ${doClassName}> implements ${serviceClassName} {

    @Override
    public List<${doClassName}> listByCondition(${paramClassName} ${lparamClassName}) {
        return this.lambdaQuery()
        <#list queryList as field>
            <#if field.attrType == 'Date' || field.attrType == 'DateTime'>
                .between(ArrayUtils.hasText(${lparamClassName}.get${field.attrName?cap_first}()), ${doClassName}::get${field.attrName?cap_first},
                ArrayUtils.isNotEmpty(${lparamClassName}.get${field.attrName?cap_first}()) ? ${lparamClassName}.get${field.attrName?cap_first}()[0] : null, ArrayUtils.isNotEmpty(${lparamClassName}.get${field.attrName?cap_first}()) ? ${lparamClassName}.get${field.attrName?cap_first}()[1] : null).
            <#elseif field.queryType == '=' && field.attrType == 'String'>
                .eq(StringUtils.hasText(${lparamClassName}.get${field.attrName?cap_first}()), ${doClassName}::get${field.attrName?cap_first},${lparamClassName}.get${field.attrName?cap_first}())
            <#elseif field.queryType == '=' && field.attrType != 'String'>
                .eq(${lparamClassName}.get${field.attrName?cap_first}() != null, ${doClassName}::get${field.attrName?cap_first},${lparamClassName}.get${field.attrName?cap_first}())
            <#elseif field.queryType == 'like'>
                .like(StringUtils.hasText(${lparamClassName}.get${field.attrName?cap_first}()), ${doClassName}::get${field.attrName?cap_first}, ${lparamClassName}.get${field.attrName?cap_first}())
            <#elseif field.queryType == 'left like'>
                .likeLeft(StringUtils.hasText(${lparamClassName}.get${field.attrName?cap_first}()), ${doClassName}::get${field.attrName?cap_first}, ${lparamClassName}.get${field.attrName?cap_first}())
            <#elseif field.queryType == 'right like'>
                .likeRight(StringUtils.hasText(${lparamClassName}.get${field.attrName?cap_first}()), ${doClassName}::get${field.attrName?cap_first}, ${lparamClassName}.get${field.attrName?cap_first}())
            </#if>
        </#list>
        .list();
    }

    @Override
    public ${doClassName} getByCondition(${paramClassName} ${lparamClassName}) {
        List<${doClassName}> resultList = listByCondition(${lparamClassName});
        if (CollectionUtils.isEmpty(resultList)) {
            return null;
        }
        return resultList.get(0);
    }



}