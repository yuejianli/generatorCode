package ${package}.${moduleName}.${voPackageName};

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
<#list importList as i>
	import ${i!};
</#list>
<#if swagger2>
	import io.swagger.annotations.ApiModel;
	import io.swagger.annotations.ApiModelProperty;
</#if>

/**
* ${tableComment} VO 对外展示视图对象
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Data
<#if swagger2>
	@ApiModel(value="${tableName}对象", description="${tableComment!}")
</#if>
public class ${fullName}  implements Serializable {
private static final long serialVersionUID = 1L;

<#list fieldList as field>
	<#if field.fieldComment!?length gt 0>
		<#if swagger2>
			@ApiModelProperty(value = "${field.fieldComment}")
		<#else>
			/**
			* ${field.fieldComment}
			*/
		</#if>
	</#if>
	private ${field.attrType} ${field.attrName};
</#list>
}