<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.${moduleName}.${mapperPackageName}.${mapperClassName}">
    <!-- 通用查询结果列 -->

    <#assign index = 0>
    <sql id="Base_Column_List">
    <#if fieldList?exists>
        <#list fieldList?chunk(5) as fieldListTemp>
            <#list fieldListTemp as field> ${field.fieldName} as ${field.attrName}   <#if fieldListTemp_has_next>,</#if></#list>
        </#list>
    </#if>
    </sql>

    <resultMap id="BaseResultMap" type="${package}.${moduleName}.${doPackageName}.${doClassName}">
        <#list fieldList as field>
            <#if field.primaryPkb>
                <id column="${field.fieldName}" property="${field.attrName}"/>
            <#else>
                <result column="${field.fieldName}" property="${field.attrName}" />
            </#if>
        </#list>
    </resultMap>

</mapper>