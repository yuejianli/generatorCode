package top.yueshushu.business;

import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.model.tool.ToolRo;

/**
 * TODO 用途描述
 *
 * @author Yue Jianli
 * @date 2023-10-08
 */

public interface ToolBusiness {

    OutputResult<String> removeSwagger(ToolRo toolRo);


    OutputResult<String> removeMyBatis(ToolRo toolRo);

    OutputResult<String> removeEasyExcel(ToolRo toolRo);

    OutputResult<String> findCha(ToolRo toolRo);


    OutputResult<String> findJiao(ToolRo toolRo);


    OutputResult<String> getKey(ToolRo toolRo);

    OutputResult<String> removeNull(ToolRo toolRo);

    OutputResult<String> concat(ToolRo toolRo);

    OutputResult<String> removePrefixLine(ToolRo toolRo);

    /**
     * 转换为Excel
     * @param toolRo 对象
     * @return 转换为Excel
     */
	OutputResult<String> convertToExcel(ToolRo toolRo);

    /**
     * 转换成json
     * @param toolRo 对象
     * @return 转换成 json
     */
    OutputResult<String> pojoToJson(ToolRo toolRo);


    /**
     * 从数据库中移除指定的ID
     *
     * @param toolRo 包含要移除ID的ToolRo对象
     * @return 返回移除操作的结果，其中包含了操作的状态和可能返回的错误信息（如果有的话）
     * @throws Exception 如果在移除过程中发生任何异常，将抛出此异常
     */
	OutputResult<String> removeDbId(ToolRo toolRo);

    OutputResult<String> addOemFirm(ToolRo toolRo);

    OutputResult<String> prodOemFirm(ToolRo toolRo);

    OutputResult<String> excelToSql(ToolRo toolRo);

}
