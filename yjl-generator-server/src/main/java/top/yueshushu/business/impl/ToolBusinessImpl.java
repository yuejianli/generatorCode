package top.yueshushu.business.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import top.yueshushu.business.ToolBusiness;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.model.tool.ToolRo;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 用途描述
 *
 * @author yuejianli
 * @date 2023-10-08
 */
@Service
@Slf4j
public class ToolBusinessImpl implements ToolBusiness {
    private static final List<String> swaggerCodeList = Arrays.asList("import io.swagger.annotations", "@Api", "@ApiOperation", "@ApiModel", "@ApiModelProperty",
            "@ApiParam", "@ApiResponse");

    private static final List<String> mybatisCodeList = Arrays.asList("import com.baomidou.mybatisplus.annotation", "@TableName", "@TableId", "@TableField", "@TableLogicDelete",
            "@TableRealDelete", "@Select");

    private static final List<String> easyExcelCodeList = Arrays.asList("import com.alibaba.excel.annotation", "import org.apache.poi", "@HeadStyle", "@HeadFontStyle", "@ColumnWidth",
            "@ContentStyle", "@ExcelProperty", "@ExcelIgnore");

    private static Pattern pojoToJsonPattern = Pattern.compile("(?<![=\\s])(.*?)(?=\\()");

    @Override
    public OutputResult<String> removeSwagger(ToolRo toolRo) {
        return OutputResult.ok(removeContent(toolRo.getContent(), swaggerCodeList));
    }

    @Override
    public OutputResult<String> removeMyBatis(ToolRo toolRo) {
        return OutputResult.ok(removeContent(toolRo.getContent(), mybatisCodeList));
    }

    @Override
    public OutputResult<String> removeEasyExcel(ToolRo toolRo) {
        return OutputResult.ok(removeContent(toolRo.getContent(), easyExcelCodeList));
    }

    @Override
    public OutputResult<String> removePrefixLine(ToolRo toolRo) {
        return OutputResult.ok(removeContent(toolRo.getContent(), Collections.singletonList(toolRo.getContent2())));
    }

	@Override
	public OutputResult<String> convertToExcel(ToolRo toolRo) {
        List<String> list = convertToList(toolRo.getContent(), toolRo.getSplit());
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            String lineContent = list.get(i).trim();
            if (!StringUtils.hasText(lineContent)) {
                continue;
            }
            // 将单个内容进行转换
            String[] lineSplitArr = lineContent.split("\\:");
            for (String line : lineSplitArr) {
                stringBuilder.append(line.replaceAll("\"","")).append("\t");
            }
            stringBuilder.append("\n");
        }
        return OutputResult.ok(stringBuilder.toString());

	}

	@Override
	public OutputResult<String> pojoToJson(ToolRo toolRo) {
        //1. 字符串找出 ( 号前面 非 = 号 的内容
        String convertContent = toolRo.getContent().replace(")","}");
        String patternContent = convertContent;
        while (StringUtils.hasText(patternContent)) {
            patternContent = findContentBeforeParenthesis(convertContent);
            convertContent = convertContent.replaceFirst(patternContent+"\\(", "{");
        }
        // 内容进行转换  将 = 号 替换成 : 号
        convertContent = convertContent.replaceAll("\\=", ":");
        // 取出 单个 : 号里面所有的值， 如果不是字符串 则要加上 双引号了。

        String[] split = convertContent.split("\\:");
        // 对所有的内容进行循环处理
        List<String> willReplaceStrList = new ArrayList<>();

        for (int i = 0; i < split.length; i++) {
            String content = split[i].trim();
            // 如果不是字符串，则加上双引号
            if (!StringUtils.hasText(content) || content.contains("\"")) {
                continue;
            }
            content= content.replace("{","").replace("}", "").trim();
            // 看是否包含 ,号:
            if (content.contains(",")) {
                // 如果包含 ,号，则进行拆分
                String[] contentSplitArr = content.split("\\,");
                for (int j = 0; j < contentSplitArr.length; j++) {
                    String contentSplit = contentSplitArr[j];
                    if (!NumberUtil.isInteger(contentSplit)) {
                        willReplaceStrList.add(contentSplit);
                    }
                }

            } else {
                if (!NumberUtil.isInteger(content)) {
                    willReplaceStrList.add(content);
                }
            }
        }

        // 进行替换
        if (CollectionUtil.isNotEmpty(willReplaceStrList)) {
            for (String willReplaceStr : willReplaceStrList) {
                convertContent = convertContent.replaceFirst(willReplaceStr, "\"" + willReplaceStr + "\"");
            }
        }
        return OutputResult.ok(convertContent);
	}

	@Override
	public OutputResult<String> removeDbId(ToolRo toolRo) {
        List<String> list = convertToList(toolRo.getContent(), toolRo.getSplit());
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            String lineContent = list.get(i).trim();
            if (!StringUtils.hasText(lineContent)) {
                continue;
            }
            // 将单个内容进行转换
            lineContent = lineContent.replace(toolRo.getProperty()+",", "");

            int startIndex = lineContent.indexOf("VALUES");
            if (startIndex < 0) {
                startIndex = lineContent.indexOf("values");
            }

            startIndex = startIndex + "VALUES".length();

            String startContent = lineContent.substring(0,startIndex);
            String endContent = lineContent.substring(startIndex);

            int startHaoIndex = endContent.indexOf("(");
            int endHaoIndex = endContent.indexOf(",");


            stringBuilder.append(startContent).append(endContent, 0, startHaoIndex +1).append(endContent.substring(endHaoIndex + 1))
                    .append(toolRo.getSplit()).append("\n");
        }
        return OutputResult.ok(stringBuilder.toString());

	}

	@Override
	public OutputResult<String> addOemFirm(ToolRo toolRo) {
        String sqlContent = toolRo.getContent();
        List<String> list = convertToList(toolRo.getContent2(), toolRo.getSplit());
        StringBuilder stringBuilder = new StringBuilder();
        Integer startIndex = toolRo.getStartId();


        for (int i = 0; i < list.size(); i++) {
            String lineContent = list.get(i).trim();
            if (!StringUtils.hasText(lineContent)) {
                continue;
            }
            String newSql = getNewSql(lineContent, toolRo.getIdProperty(),String.valueOf(startIndex));
            newSql = getNewSql(newSql, toolRo.getStateProperty(),toolRo.getStateProperty());
             newSql = getNewSql(newSql, toolRo.getDataProperty(),String.valueOf(startIndex));
             newSql = getNewSql(newSql, toolRo.getOemProperty(),toolRo.getOemFirm());
            startIndex += 1;
            stringBuilder
                    .append(newSql)
                    .append(toolRo.getSplit()).append("\n");
        }
        return OutputResult.ok(stringBuilder.toString());
	}

	@Override
	public OutputResult<String> prodOemFirm(ToolRo toolRo) {
        List<String> list = convertToList(toolRo.getContent(), toolRo.getSplit());
        StringBuilder stringBuilder = new StringBuilder();
        Integer startIndex = toolRo.getStartId();
        for (int i = 0; i < list.size(); i++) {
            String lineContent = list.get(i).trim();
            if (!StringUtils.hasText(lineContent)) {
                continue;
            }
            String newSql = getNewSql(lineContent, toolRo.getIdProperty(),String.valueOf(startIndex));
            newSql = getNewSql(newSql, toolRo.getStateProperty(),toolRo.getStateProperty());
            newSql = getNewSql(newSql, toolRo.getDataProperty(),String.valueOf(startIndex));
            newSql = getNewSql(newSql, toolRo.getOemProperty(),toolRo.getOemFirm());
            newSql = getNewSql(newSql, toolRo.getKeyUniqueProperty(),String.valueOf(startIndex));
            startIndex += 1;
            stringBuilder
                    .append(newSql)
                    .append(toolRo.getSplit()).append("\n");
        }
        return OutputResult.ok(stringBuilder.toString());

	}

    @Override
    public OutputResult<String> excelToSql(ToolRo toolRo) {
        List<String> list = convertToList(toolRo.getContent(), toolRo.getSplit());
        StringBuilder stringBuilder = new StringBuilder();
        Integer startIndex = toolRo.getStartId();
        for (int i = 0; i < list.size(); i++) {
            String lineContent = list.get(i).trim();
            if (!StringUtils.hasText(lineContent)) {
                continue;
            }
            String newSql = getNewSql(lineContent, toolRo.getIdProperty(),String.valueOf(startIndex));
            newSql = getNewSql(newSql, toolRo.getStateProperty(),toolRo.getStateProperty());
            newSql = getNewSql(newSql, toolRo.getDataProperty(),String.valueOf(startIndex));
            newSql = getNewSql(newSql, toolRo.getOemProperty(),toolRo.getOemFirm());
            newSql = getNewSql(newSql, toolRo.getKeyUniqueProperty(),String.valueOf(startIndex));
            startIndex += 1;
            stringBuilder
                    .append(newSql)
                    .append(toolRo.getSplit()).append("\n");
        }
        return OutputResult.ok(stringBuilder.toString());
    }

    public static Map<String, String> parseInsertStatement(String sql) {
        Map<String, String> columnValues = new LinkedHashMap<>();
        // 提取列名
        Pattern columnsPattern = Pattern.compile("\\(([^)]+)\\)");
        Matcher columnsMatcher = columnsPattern.matcher(sql);
        String[] columns = null;
        if (columnsMatcher.find()) {
            String columnsStr = columnsMatcher.group(1);
            columns = columnsStr.split(",\\s*");
            for (String column : columns) {
                columnValues.put(column, null); // 初始化列名，但没有值
            }
        }

        // 提取值
        Pattern valuesPattern = Pattern.compile("VALUES\\s*\\((.*?)\\)", Pattern.CASE_INSENSITIVE);
        Matcher valuesMatcher = valuesPattern.matcher(sql);
        if (valuesMatcher.find()) {
            String valuesStr = valuesMatcher.group(1);
            String[] values = valuesStr.split(",\\s*");
            int i = 0;
            for (String value : values) {
                // 假设所有的值都被单引号包围
                columnValues.put(columns[i++], value.replaceAll("^'(.*)'$", "$1"));
            }
        }
        return columnValues;
    }


    public String getNewSql(String sql, String columnName,String newValue) {
        Map<String, Integer> columnIndexValues = new LinkedHashMap<>();
        // 提取列名
        Pattern columnsPattern = Pattern.compile("\\(([^)]+)\\)");
        Matcher columnsMatcher = columnsPattern.matcher(sql);
        String[] columns;
        int length = 0;
        if (columnsMatcher.find()) {
            String columnsStr = columnsMatcher.group(1);
            columns = columnsStr.split(",\\s*");
            for (String column : columns) {
                columnIndexValues.put(column, length++); // 初始化列名，但没有值
            }
        }

        // 提取值
        Pattern valuesPattern = Pattern.compile("VALUES\\s*\\((.*?)\\)", Pattern.CASE_INSENSITIVE);
        Matcher valuesMatcher = valuesPattern.matcher(sql);
        int endJsonIndex = 4;
        int startJsonIndex = 7;
        // 填充的只是 5 和 6 位。
        if (valuesMatcher.find()) {
            String valuesStr = valuesMatcher.group(1);
            String[] values = valuesStr.split(",\\s*");
            // 将values 中的 第5位与 第12位进行合并
            int lastEndJsonIndex = values.length - 7;  // 13
            int singleEndJsonIndex = endJsonIndex + (lastEndJsonIndex - startJsonIndex) /2 +1; //为8
           // 7
            String[] newValues = new String[length];
            String jsonValue = StrUtil.join("", ArrayUtil.sub(values, endJsonIndex +1, singleEndJsonIndex +1));
            ArrayUtil.copy(values, 0, newValues, 0, endJsonIndex);
            newValues[endJsonIndex +1] = jsonValue;
            newValues[endJsonIndex + 2] = jsonValue;
            ArrayUtil.copy(values, endJsonIndex +3, newValues, startJsonIndex, 7);
            String oldValue = newValues[columnIndexValues.get(columnName)];
            sql = sql.replace(oldValue, newValue);
        }
        return sql;
    }



	public static String findContentBeforeParenthesis(String s) {
        if (!s.contains("(")) {
            return null;
        }
        s = s.substring(0,s.indexOf("("));
        // 看这个值 s 前面是否有 = 号
        if (!s.contains("=")) {
            return s;
        }
        return s.split("=")[1];
    }

    @Override
    public OutputResult<String> findCha(ToolRo toolRo) {
        List<String> list1 = convertToList(toolRo.getContent(), toolRo.getSplit());
        List<String> list2 = convertToList(toolRo.getContent2(), toolRo.getSplit());

        Set<String> stringSet = new HashSet<>(CollectionUtil.subtractToList(list1, list2));

        return OutputResult.ok(convertToStr(stringSet, toolRo.getSplit()));
    }

    @Override
    public OutputResult<String> findJiao(ToolRo toolRo) {
        List<String> list1 = convertToList(toolRo.getContent(), toolRo.getSplit());
        List<String> list2 = convertToList(toolRo.getContent2(), toolRo.getSplit());

        Set<String> stringSet = new HashSet<>(CollectionUtil.intersectionDistinct(list1, list2));

        return OutputResult.ok(convertToStr(stringSet, toolRo.getSplit()));
    }

    @Override
    public OutputResult<String> getKey(ToolRo toolRo) {
        // 字符串按照 ,号进行拆分。
        String[] split = toolRo.getContent().split("\\,");
        // 处理值。
        List<String> resultList = new ArrayList<>();

        for (String line : split) {
            if (!line.contains(toolRo.getProperty())) {
                continue;
            }
            // 将内容拆分成 :
            String[] keyValueArr = line.split("\\:");

            resultList.add(keyValueArr[1]);
        }
        return OutputResult.ok(StrUtil.join("\n", resultList));
    }

    @Override
    public OutputResult<String> removeNull(ToolRo toolRo) {
        // 字符串按照 ,号进行拆分。
        String[] split = toolRo.getContent().split("\\n");
        // 处理值。
        List<String> resultList = new ArrayList<>();

        for (String line : split) {
            if (line.contains(": null")) {
                continue;
            }
            if (toolRo.isRemoveTime()) {
                line = line.replace(" 00:00:00", "");
            }
            resultList.add(line);
        }
        return OutputResult.ok(StrUtil.join("\n", resultList));

    }

    @Override
    public OutputResult<String> concat(ToolRo toolRo) {
        String[] contentArr = toolRo.getContent().split("\\n");
        // 处理值。
        List<String> resultList = new ArrayList<>();

        for (String line : contentArr) {
            resultList.add(line.trim().replace(",", "").replace("\"", ""));
        }
        return OutputResult.ok(StrUtil.join(",", resultList));
    }




    private List<String> convertToList(String content, String splitStr) {
        return Arrays.stream(content.split(splitStr)).collect(Collectors.toList());
    }

    private String convertToStr(Collection<String> collection, String splitStr) {
        return StrUtil.join(splitStr, collection);
    }


    private String removeContent(String content, List<String> codeList) {
        //1.  将内容进行转换   按照 \n 进行拆分。
        String[] contentArr = content.split("\\n");

        List<String> resultList = new ArrayList<>();

        // 进行处理
        for (String lineStr : contentArr) {
            if (containsList(lineStr.trim(), codeList)) {
                continue;
            }
            resultList.add(lineStr);
        }
        return StrUtil.join("\n", resultList);
    }


    private boolean containsList(String trimStr, List<String> codeList) {
        for (String code : codeList) {
            if (trimStr.contains(code)) {
                return true;
            }
        }
        return false;
    }
}
