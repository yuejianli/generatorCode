package top.yueshushu.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.yueshushu.model.WebhookMessage;
import top.yueshushu.model.oauth2.AuthorizeInfo;
import top.yueshushu.model.oauth2.TokenInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>BeiRuiMapController此类用于贝瑞花生壳的映射 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/7/3 9:22</p>
 * <p>@remark:</p>
 */
@Slf4j
@Controller
@RequestMapping("/beiRui")
public class BeiRuiMapController {

	private String DEV_URL = "http://172.16.9.12:53340/testEzviz/ezvizCallback";

	private String CHK_URL = "https://chktppf.unisiot.com/device/ezvizCallback";


	@RequestMapping(value = "/map")
	public ResponseEntity<String> VipWebhook(@RequestHeader HttpHeaders header, @RequestBody String body) {
		log.info("贝瑞花生壳接收到消息并转发到本地");

		WebhookMessage receiveMessage = null;
		log.info("获取萤石 消息获取时间:{}",System.currentTimeMillis());
		try {
			receiveMessage = JSON.parseObject(body, WebhookMessage.class);
			receiveMessage.getHeader().setMessageId("-1");
		} catch (Exception e) {
			//异常处理
			log.error("萤石消息转换出错: header: {}, body: {}", header, body, e);
		}


		// 转换到对应的 开发环境,即模拟开发环境的请求信息.
		HttpUtil.createPost(CHK_URL).header("Content-Type", "application/json")
				.header("Accesstoken","a4878e80f7894025af3c7c3003c898b0")
				.body(JSONUtil.toJsonStr(receiveMessage)).execute();


		HttpUtil.createPost(DEV_URL).header("Content-Type", "application/json")
				.header("Accesstoken","a4878e80f7894025af3c7c3003c898b0")
				.body(JSONUtil.toJsonStr(receiveMessage)).execute();


		Map<String, String> result = new HashMap<>(1);
		assert receiveMessage != null;
		String messageId = receiveMessage.getHeader().getMessageId();
		result.put("messageId", messageId);
		log.info("获取的消息id是:{}, 接收到的消息是:{}", messageId,body);
		final ResponseEntity<String> resp = ResponseEntity.ok(JSON.toJSONString(result));
		// log.info("萤石 返回的信息:{}",JSON.toJSONString(resp));
		return resp;
	}



	@GetMapping(value = "/oauth/oauth/authorize")
	public ResponseEntity<String> authorize(@RequestHeader HttpHeaders header, AuthorizeInfo authorizeInfo) {
//		log.info("贝瑞花生壳接收到消息authorize 并转发到本地");
//		// 转换到对应的 开发环境,即模拟开发环境的请求信息.
//		String url = "https://devl.unisiot.com:8165/oauth/oauth/authorize?client_id=yv6hki1fgpc37fkt138cd25od0kg49pt&redirect_uri=https://cgw.duiopen.com/account-link/v1/skill/2024091900000002&state=1&scope=read%20write&response_type=code&user_locale=LOCALE";
//		log.info("url : {}", url);
//		String htmlBody =  HttpUtil.get(url);

		String htmlBody = "<!DOCTYPE HTML>\n" +
				"<html>\n" +
				"<head>\n" +
				"    <meta charset=\"utf-8\"/>\n" +
				"\n" +
				"    <meta name=\"viewport\" content=\"width=device-width,user-scalable=no\"/>\n" +
				"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\"/>\n" +
				"    \n" +
				"\n" +
				"    <title>授权登录</title>\n" +
				"\n" +
				"    <link href=\"https://devl.unisiot.com:8165/oauth/resources/bootstrap.min.css\" rel=\"stylesheet\"/>\n" +
				"    <decorator:head/>\n" +
				"    \n" +
				"    <meta charset=\"utf-8\">\n" +
				"    <meta name=\"viewport\"\n" +
				"        content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
				"    <meta http-equiv=\"Pragma\" content=\"no-cache\">\n" +
				"    <meta http-equiv=\"Cache-Control\" content=\"no-cache\">\n" +
				"    <meta name=\"renderer\" content=\"webkit\">\n" +
				"    <meta http-equiv=\"Expires\" content=\"0\">\n" +
				"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n" +
				"    \n" +
				"    <!-- 引入自适应类库，不建议在main.js里引入 -->\n" +
				"    <script>\n" +
				"        !function (e) {\n" +
				"            var t = e.document\n" +
				"                , n = t.documentElement\n" +
				"                , i = \"orientationchange\" in e ? \"orientationchange\" : \"resize\"\n" +
				"                , a = function e() {\n" +
				"                    var t = n.getBoundingClientRect().width;\n" +
				"                    return n.style.fontSize = 5 * Math.max(Math.min(t / 750 * 20, 11.2), 8.55) + \"px\",\n" +
				"                        e\n" +
				"                }();\n" +
				"            n.setAttribute(\"data-dpr\", e.navigator.appVersion.match(/iphone/gi) ? e.devicePixelRatio : 1),\n" +
				"                /iP(hone|od|ad)/.test(e.navigator.userAgent) && (t.documentElement.classList.add(\"ios\"),\n" +
				"                    parseInt(e.navigator.appVersion.match(/OS (\\d+)_(\\d+)_?(\\d+)?/)[1], 10) >= 8 && t.documentElement.classList.add(\"hairline\")),\n" +
				"                t.addEventListener && (e.addEventListener(i, a, !1),\n" +
				"                    t.addEventListener(\"DOMContentLoaded\", a, !1))\n" +
				"        }(window);\n" +
				"    </script>\n" +
				"    <style>\n" +
				"        html,\n" +
				"        body,\n" +
				"        div,\n" +
				"        p,\n" +
				"        input,\n" +
				"        label,\n" +
				"        img,\n" +
				"        ul,\n" +
				"        li {\n" +
				"            margin: 0;\n" +
				"            padding: 0;\n" +
				"            font-size: 0;\n" +
				"        }\n" +
				"\n" +
				"        #app {\n" +
				"            font-family: Helvetica, Arial, sans-serif, 'Avenir';\n" +
				"            -webkit-font-smoothing: antialiased;\n" +
				"            -moz-osx-font-smoothing: grayscale;\n" +
				"            text-align: center;\n" +
				"            color: #2c3e50;\n" +
				"            height: 100vh;\n" +
				"            overflow: hidden;\n" +
				"            position: relative;\n" +
				"        }\n" +
				"\n" +
				"        .logo img {\n" +
				"            margin-top: 60px;\n" +
				"            width: 5.5rem;\n" +
				"        }\n" +
				"\n" +
				"        .logo p {\n" +
				"            line-height: 0.5rem;\n" +
				"            height: 0.9rem;\n" +
				"            font-size: 0.275rem;\n" +
				"            color: #595959;\n" +
				"            letter-spacing: 1px;\n" +
				"        }\n" +
				"\n" +
				"        .form-input {\n" +
				"            font-size: 0;\n" +
				"            border-bottom: 2px solid #EFF0F2;\n" +
				"            display: block;\n" +
				"            text-align: left;\n" +
				"            margin: 0 20px;\n" +
				"        }\n" +
				"\n" +
				"        .form-input input {\n" +
				"            width: 100%;\n" +
				"            vertical-align: middle;\n" +
				"            background-repeat: no-repeat;\n" +
				"            background-size: 0.5rem 0.5rem;\n" +
				"            background-position: 0 0;\n" +
				"            height: 0.9rem;\n" +
				"            border: none;\n" +
				"            box-shadow: none;\n" +
				"            outline: none;\n" +
				"            color: #333;\n" +
				"            font-size: 0.3rem;\n" +
				"        }\n" +
				"\n" +
				"        .form-submit {\n" +
				"            margin-top: 20px;\n" +
				"        }\n" +
				"\n" +
				"        #submit {\n" +
				"            margin-top: .6rem;\n" +
				"            width: 6.2rem;\n" +
				"            background: #79D07D;\n" +
				"            height: .8rem;\n" +
				"            border: 0;\n" +
				"            color: white;\n" +
				"            font-size: 0.3rem;\n" +
				"            letter-spacing: 0.15rem;\n" +
				"            outline: none;\n" +
				"            border-radius: .8rem;\n" +
				"        }\n" +
				"\n" +
				"        #submit.disabled {\n" +
				"            background: lightgrey;\n" +
				"        }\n" +
				"\n" +
				"        input::-webkit-input-placeholder {\n" +
				"            color: #A0A0A0;\n" +
				"        }\n" +
				"\n" +
				"        input::-moz-placeholder {\n" +
				"            /* Mozilla Firefox 19+ */\n" +
				"            color: #A0A0A0;\n" +
				"        }\n" +
				"\n" +
				"        input:-moz-placeholder {\n" +
				"            /* Mozilla Firefox 4 to 18 */\n" +
				"            color: #A0A0A0;\n" +
				"        }\n" +
				"\n" +
				"        input:-ms-input-placeholder {\n" +
				"            /* Internet Explorer 10-11 */\n" +
				"            color: #A0A0A0;\n" +
				"        }\n" +
				"\n" +
				"        .label-danger {\n" +
				"            font-size: 0.2rem;\n" +
				"        }\n" +
				"\n" +
				"        .form-input input[name=\"password\"] {\n" +
				"            width: 80%;\n" +
				"        }\n" +
				"\n" +
				"        .form-input input[name=\"code\"] {\n" +
				"            width: 65%;\n" +
				"        }\n" +
				"\n" +
				"        .eye {\n" +
				"            width: .4rem;\n" +
				"            height: .4rem;\n" +
				"            margin: .25rem .2rem;\n" +
				"            vertical-align: middle;\n" +
				"            float: right;\n" +
				"        }\n" +
				"\n" +
				"        #getCode {\n" +
				"            position: relative;\n" +
				"            float: right;\n" +
				"            font-size: 0.28rem;\n" +
				"            height: 0.9rem;\n" +
				"            line-height: 0.9rem;\n" +
				"            color: #3c9cff;\n" +
				"            user-select: none;\n" +
				"        }\n" +
				"\n" +
				"        .login-way {\n" +
				"            padding: 0.4rem 0 0;\n" +
				"            text-align: center;\n" +
				"            font-size: 0.3rem;\n" +
				"            color: #79D07D;\n" +
				"            user-select: none;\n" +
				"        }\n" +
				"\n" +
				"        .hide {\n" +
				"            display: none;\n" +
				"        }\n" +
				"\n" +
				"        .mask {\n" +
				"            position: fixed;\n" +
				"            z-index: 1;\n" +
				"            left: 0;\n" +
				"            right: 0;\n" +
				"            bottom: 0;\n" +
				"            top: 0;\n" +
				"            background: rgba(0, 0, 0, .2);\n" +
				"        }\n" +
				"\n" +
				"        .host-list {\n" +
				"            position: absolute;\n" +
				"            min-height: 100px;\n" +
				"            max-height: calc(100vh - 300px);\n" +
				"            overflow-y: auto;\n" +
				"            top: 100vh;\n" +
				"            right: 15px;\n" +
				"            left: 15px;\n" +
				"            background: #fff;\n" +
				"            z-index: 2;\n" +
				"            border-radius: 5px;\n" +
				"            padding-bottom: 10px;\n" +
				"        }\n" +
				"\n" +
				"        .host-list p {\n" +
				"            font-size: 18px;\n" +
				"            text-align: center;\n" +
				"            color: #333;\n" +
				"            padding: 10px 0;\n" +
				"        }\n" +
				"\n" +
				"        .host-list li {\n" +
				"            line-height: 0.4rem;\n" +
				"            border-bottom: 1px solid #F5F5F5;\n" +
				"            font-size: 0.3rem;\n" +
				"            color: #2e8ded;\n" +
				"            padding: 10px 0;\n" +
				"        }\n" +
				"\n" +
				"        .host-list li p {\n" +
				"            color: #2e8ded;\n" +
				"            padding: 0px 0;\n" +
				"        }\n" +
				"        .bottom-info {\n" +
				"            margin-top: 3rem;\n" +
				"        }\n" +
				"\n" +
				"        .bottom-info p,\n" +
				"        .bottom-info a {\n" +
				"            color: #333333;\n" +
				"            font-size: 0.3rem;\n" +
				"        }\n" +
				"\n" +
				"        .bottom-info img {\n" +
				"            margin-bottom: 0.3rem;\n" +
				"            width: 3.5rem;\n" +
				"        }\n" +
				"    </style>\n" +
				"\n" +
				"\n" +
				"</head>\n" +
				"<body class=\"container\">\n" +
				"<div>\n" +
				"    <div>\n" +
				"        <decorator:body/>\n" +
				"        \n" +
				"    <div id=\"app\">\n" +
				"        <form action='https://devl.unisiot.com:8165/oauth/oauth/authorize' method='post' id=\"formInfo\">\n" +
				"            <input name='client_id' v-model='client_id' type='hidden' />\n" +
				"            <input name='redirect_uri' v-model='redirect_uri' type='hidden' />\n" +
				"            <input name='response_type' v-model='response_type' type='hidden' />\n" +
				"            <input name='scope' v-model='scope' type='hidden' />\n" +
				"            <input name='state' v-model='state' type='hidden' />\n" +
				"        </form>\n" +
				"        <form class=\"form-horizontal\" onsubmit=\"return false;\">\n" +
				"            <div class=\"logo\">\n" +
				"                <div style=\"position: relative;\">\n" +
				"                    <img src=\"https://devl.unisiot.com:8165/oauth/resources/user/img/ziguang.png\" alt=\"bg\" id=\"uiot_logo_show\">\n" +
				"                </div>\n" +
				"            </div>\n" +
				"\n" +
				"            <div class=\"login-form\">\n" +
				"                <div class=\"form-input\" style=\"border-bottom: 2px solid #EFF0F2;\">\n" +
				"                    <input type=\"text\" autocomplete=\"off\" placeholder=\"用户名\" required=\"required\" name=\"username\" />\n" +
				"                </div>\n" +
				"                <div class=\"form-input\" id=\"passwordInputBox\" style=\"border-bottom: 2px solid #EFF0F2;\">\n" +
				"                    <input type=\"password\" autocomplete=\"off\" placeholder=\"密码\" required=\"required\" name=\"password\" />\n" +
				"                    <img src=\"https://devl.unisiot.com:8165/oauth/resources/user/img/eye2.png\" alt=\"\" class=\"eye\" id=\"eye_show\">\n" +
				"                    <img src=\"https://devl.unisiot.com:8165/oauth/resources/user/img/eye1.png\" alt=\"\" class=\"eye hide\" id=\"eye_hide\">\n" +
				"                </div>\n" +
				"                <div class=\"form-input hide\" id=\"codeInputBox\" style=\"border-bottom: 2px solid #EFF0F2;\">\n" +
				"                    <input type=\"password\" autocomplete=\"off\" placeholder=\"验证码\" readonly=\"true\" required=\"required\" name=\"code\" />\n" +
				"                    <div id=\"getCode\" value=\"获取验证码\">获取验证码</div>\n" +
				"                </div>\n" +
				"                <div class=\"form-submit\">\n" +
				"                    <input type=\"submit\" id=\"submit\" class=\"disabled\" value='登录'>\n" +
				"                </div>\n" +
				"                <div class=\"login-way\">\n" +
				"                    <span class=\"login-way-code\">验证码登录</span>\n" +
				"                    <span class=\"login-way-password hide\">密码登录</span>\n" +
				"                </div>\n" +
				"            </div>\n" +
				"        </form>\n" +
				"        <div class=\"bottom-info\" id=\"bottom_id\">\n" +
				"            <img src=\"https://devl.unisiot.com:8165/oauth/resources/user/img/logo2.png\" alt=\"logo\" id=\"logo_img\">\n" +
				"            <p id=\"phone\">服务热线：<a href=\"tel:4006668906\" id=\"tel\">400-666-8906</a></p>\n" +
				"        </div>\n" +
				"        <div class=\"mask hide\"></div>\n" +
				"        <div class=\"host-list\">\n" +
				"            <form action='http://2i99758j32.oicp.vip/Generator/beiRui/oauth/oauth/token' method='post' id=\"login\">\n" +
				"                <input type=\"hidden\" name=\"username\">\n" +
				"                <input type=\"hidden\" name=\"password\">\n" +
				"                <p>请选择智能服务器</p>\n" +
				"                <ul></ul>\n" +
				"            </form>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"    <script src=\"https://devl.unisiot.com:8165/oauth/resources/user/js/jquery1.11.0.min.js\"></script>\n" +
				"    <script src=\"https://devl.unisiot.com:8165/oauth/resources/user/js/layer/layer.js\"></script>\n" +
				"    <script>\n" +
				"        var showEyeBtn = document.querySelector('#eye_show');\n" +
				"        var hideEyeBtn = document.querySelector('#eye_hide');\n" +
				"        var pwdInput = document.querySelector('[name=\"password\"]');\n" +
				"        var userNameInput = document.querySelector('[name=\"username\"]');\n" +
				"        var submitBtn = document.querySelector('#submit');\n" +
				"        var userNameIsEmpty = true;\n" +
				"        var pwdIsEmpty = true;\n" +
				"        var canSubmit = false;\n" +
				"        var loading = false;\n" +
				"\n" +
				"        var codeInput = document.querySelector('[name=\"code\"]');\n" +
				"        var codeBtn = document.querySelector('#getCode');\n" +
				"        var codeLoading = false;\n" +
				"        var codeIsEmpty = true;\n" +
				"        var codeTimer = null;\n" +
				"        var codeTimeNum = 60;\n" +
				"\n" +
				"        var loginWayBtn = document.querySelector('.login-way');\n" +
				"        var isPasswordLoginWay = true;\n" +
				"\n" +
				"        // 显示密码\n" +
				"        showEyeBtn.addEventListener('click', function (e) {\n" +
				"            showEyeBtn.classList.add('hide');\n" +
				"            hideEyeBtn.classList.remove('hide');\n" +
				"            pwdInput.setAttribute('type', 'text');\n" +
				"        });\n" +
				"        // 隐藏密码\n" +
				"        hideEyeBtn.addEventListener('click', function (e) {\n" +
				"            hideEyeBtn.classList.add('hide');\n" +
				"            showEyeBtn.classList.remove('hide');\n" +
				"            pwdInput.setAttribute('type', 'password');\n" +
				"        });\n" +
				"        // 用户名输入\n" +
				"        userNameInput.addEventListener('input', function (e) {\n" +
				"            userNameIsEmpty = this.value.length === 0;\n" +
				"            submitDisable();\n" +
				"        });\n" +
				"        // 密码输入\n" +
				"        pwdInput.addEventListener('input', function (e) {\n" +
				"            pwdIsEmpty = this.value.length === 0;\n" +
				"            submitDisable();\n" +
				"        });\n" +
				"        // 验证码输入\n" +
				"        codeInput.addEventListener('input', function (e) {\n" +
				"            codeIsEmpty = this.value.length === 0;\n" +
				"            submitDisable();\n" +
				"        });\n" +
				"        // 切换登录方式\n" +
				"        loginWayBtn.addEventListener('click', function (e) {\n" +
				"            $('.login-way-code').toggleClass('hide');\n" +
				"            $('.login-way-password').toggleClass('hide');\n" +
				"            $('#passwordInputBox').toggleClass('hide');\n" +
				"            $('#codeInputBox').toggleClass('hide');\n" +
				"            isPasswordLoginWay = !isPasswordLoginWay;\n" +
				"            submitDisable();\n" +
				"        });\n" +
				"        // 点击遮罩\n" +
				"        $(\".mask\").click(function (e) {\n" +
				"            hideMask();\n" +
				"        });\n" +
				"\n" +
				"        // 获取验证码\n" +
				"        codeBtn.addEventListener('click', function (e) {\n" +
				"            codeLoading = false;\n" +
				"            if (codeTimeNum < 60) {\n" +
				"                return;\n" +
				"            }\n" +
				"            getCode();\n" +
				"\n" +
				"        });\n" +
				"        // 验证码倒计时\n" +
				"        function startCodeTimer() {\n" +
				"            if (codeTimeNum === 0) {\n" +
				"                clearInterval(codeTimer);\n" +
				"                codeBtn.innerHTML = '重新获取';\n" +
				"                codeTimeNum = 60;\n" +
				"            } else {\n" +
				"                codeBtn.innerHTML = codeTimeNum + '秒后重新获取';\n" +
				"                codeTimeNum--;\n" +
				"            }\n" +
				"        }\n" +
				"\n" +
				"        // 验证手机号码格式\n" +
				"        function isMobile(mobile) {\n" +
				"            return /^1[3-9]\\d{9}$/.test(mobile)\n" +
				"        }\n" +
				"\n" +
				"        function getCode() {\n" +
				"            if (codeLoading) {\n" +
				"                return\n" +
				"            };\n" +
				"            if (userNameIsEmpty || !isMobile(userNameInput.value)) {\n" +
				"                layer.msg('请确认手机号是否正确');\n" +
				"                return\n" +
				"            };\n" +
				"            codeLoading = true;\n" +
				"            startCodeTimer();\n" +
				"            $(codeInput).attr('readonly', false)\n" +
				"            codeTimer = setInterval(startCodeTimer, 1000);\n" +
				"            var params = {\n" +
				"                phone: userNameInput.value,\n" +
				"                areaCode: '86'\n" +
				"            };\n" +
				"            $.ajax({\n" +
				"                //请求方式\n" +
				"                type: \"POST\",\n" +
				"                url: \"oauth/sendMsg\",\n" +
				"                dataType: 'json',\n" +
				"                contentType: 'application/json',\n" +
				"                data: JSON.stringify(params),\n" +
				"                //请求成功\n" +
				"                success: function (data) {\n" +
				"                    if (data.code != 0) {\n" +
				"                        layer.msg(data.msg);\n" +
				"                        return false;\n" +
				"                    }\n" +
				"                    codeLoading = false;\n" +
				"                    layer.msg('发送成功');\n" +
				"                }\n" +
				"            })\n" +
				"        }\n" +
				"\n" +
				"        // 改变提交按钮是否可点击\n" +
				"        function submitDisable() {\n" +
				"            canSubmit = !userNameIsEmpty && (isPasswordLoginWay ? !pwdIsEmpty : !codeIsEmpty);\n" +
				"            if (!canSubmit) {\n" +
				"                submitBtn.classList.add('disabled');\n" +
				"            } else {\n" +
				"                submitBtn.classList.remove('disabled');\n" +
				"            }\n" +
				"        }\n" +
				"        // 点击提交\n" +
				"        submitBtn.addEventListener('click', function (e) {\n" +
				"            if (isPasswordLoginWay) {\n" +
				"                // 密码登录\n" +
				"                submitPost(isPasswordLoginWay);\n" +
				"            } else {\n" +
				"                // 验证码登录\n" +
				"                checkCode();\n" +
				"            }\n" +
				"\n" +
				"        });\n" +
				"\n" +
				"        // 校验验证码是否正确\n" +
				"        function checkCode() {\n" +
				"            var phone = userNameInput.value;\n" +
				"            var code = codeInput.value;\n" +
				"            if(phone == '' || code == ''){\n" +
				"                return;\n" +
				"            }\n" +
				"            var params = {\n" +
				"                phone: phone,\n" +
				"                code: code\n" +
				"            }\n" +
				"            $.ajax({\n" +
				"                //请求方式\n" +
				"                type: \"POST\",\n" +
				"                url: \"oauth/checkCode\",\n" +
				"                dataType: 'json',\n" +
				"                contentType: 'application/json',\n" +
				"                data: JSON.stringify(params),\n" +
				"                //请求成功\n" +
				"                success: function (data) {\n" +
				"                    if (data.code !== 0) {\n" +
				"                        layer.msg(data.msg);\n" +
				"                        return false;\n" +
				"                    } else {\n" +
				"                        submitPost(false);\n" +
				"                    }\n" +
				"                }\n" +
				"            })\n" +
				"        };\n" +
				"\n" +
				"        function submitPost(isPasswordLoginWay) {\n" +
				"            if (!canSubmit || loading) {\n" +
				"                return;\n" +
				"            }\n" +
				"            loading = true;\n" +
				"            var getSnParam = {};\n" +
				"            if(isPasswordLoginWay){\n" +
				"                getSnParam = { username: userNameInput.value, password: pwdInput.value };\n" +
				"            }else{\n" +
				"                getSnParam = { username: userNameInput.value };\n" +
				"            }\n" +
				"           \n" +
				"\t\t var dataStr = `{\n" +
				"    \"code\": 0,\n" +
				"    \"msg\": \"\",\n" +
				"    \"result\": {\n" +
				"        \"snList\": [\n" +
				"            {\n" +
				"                \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"oemFirm\": \"0X8001\",\n" +
				"                \"remark\": \"倪志明主机_d67454\",\n" +
				"                \"hostType\": 5.0,\n" +
				"                \"snSecret\": \"2411p0********2fcd66\"\n" +
				"            }\n" +
				"        ]\n" +
				"    }\n" +
				"}`;\n" +
				"\t\t    var data = JSON.parse(dataStr);\n" +
				"\t\t\tconsole.log(data);\n" +
				"                    if (data.code != 0) {\n" +
				"                        layer.msg(data.msg);\n" +
				"                        //alert(data.msg);\n" +
				"                        return false;\n" +
				"                    }\n" +
				"\n" +
				"                    var snListLength = data.result.snList.length;\n" +
				"                    console.info(snListLength);\n" +
				"                    if (snListLength == 1) {\n" +
				"\n" +
				"                        var regSnId = data.result.snList[0].regSnId;\n" +
				"                        var sn = data.result.snList[0].sn;\n" +
				"                        regSnId = regSnId ? regSnId:\"\";\n" +
				"                        checkHost(regSnId,sn);\n" +
				"                        return false;\n" +
				"                    }\n" +
				"\n" +
				"\n" +
				"                    var temp = '';\n" +
				"                    data.result.snList.forEach(function (el) {\n" +
				"                        var regSnId = el.regSnId ? el.regSnId:\"\";\n" +
				"                        temp += '<li onclick=\"checkHost(\\'' + regSnId + '\\',\\'' + el.sn + '\\')\">';\n" +
				"                        var remark = el.remark;\n" +
				"                        if(remark){\n" +
				"                            temp += '<p style=\"font-size: 14px\">'+ el.remark +'</p>';\n" +
				"                        }\n" +
				"                        temp +='<p>'+ el.snSecret +'</p>'\n" +
				"                            + '</li>';\n" +
				"                    });\n" +
				"                    $(\".host-list ul\").html(temp);\n" +
				"                    $(\".mask\").removeClass('hide');\n" +
				"                    $(\".host-list\").animate({\n" +
				"                        top: '150px',\n" +
				"                    });\n" +
				"                    loading = false;\n" +
				"\t\t\t\t\t\n" +
				"\t\t\t\t\t\n" +
				"            loading = false;\n" +
				"            console.info(\"请求结束\");\n" +
				"        }\n" +
				"\n" +
				"        function checkHost(regSnId,sn) {\n" +
				"            var password = pwdInput.value;\n" +
				"            $('[name=\"password\"]').val(pwdInput.value);\n" +
				"            if(regSnId != ''){\n" +
				"                $('[name=\"username\"]').val(userNameInput.value + '|' + regSnId +\"|\"+ password);\n" +
				"            }else if(sn){\n" +
				"                $('[name=\"username\"]').val(userNameInput.value + '/' + sn +\"/\"+ password);\n" +
				"            }else{\n" +
				"                layer.msg(\"账号信息不存在，请检查后重试\");\n" +
				"            }\n" +
				"            $('#login').submit();\n" +
				"        }\n" +
				"\n" +
				"        function hideMask() {\n" +
				"            $(\".host-list\").animate({\n" +
				"                top: '100vh',\n" +
				"            });\n" +
				"            $(\".mask\").addClass('hide');\n" +
				"        }\n" +
				"        function getContextPath() {\n" +
				"            return \"https://devl.unisiot.com:8165/oauth/\";\n" +
				"        }\n" +
				"        window.onload = function () {\n" +
				"            var dominArr = document.domain.split('.');\n" +
				"            var imgplantpath = \"img\";\n" +
				"            let tel = document.getElementById('tel');\n" +
				"            if (\"sxoauth\" == dominArr[0] || \"devsxoauth\" == dominArr[0] || \"chksxoauth\" == dominArr[0]) {\n" +
				"                imgplantpath = \"sximg\"\n" +
				"                tel.href = '';\n" +
				"                tel.innerHTML = \"\";\n" +
				"                document.getElementById(\"bottom_id\").style.display = \"none\";\n" +
				"            }\n" +
				"            var uiotLogoShow = document.getElementById(\"uiot_logo_show\");\n" +
				"            uiotLogoShow.src = getContextPath() + \"/resources/user/\" + imgplantpath + \"/ziguang.png\"\n" +
				"        };\n" +
				"    </script>\n" +
				"\n" +
				"    </div>\n" +
				"    \n" +
				"</div>\n" +
				"</body>\n" +
				"</html>";
		log.info("打印信息: {}", htmlBody);
		return ResponseEntity.ok(htmlBody);
	}


	@PostMapping(value = "/oauth/oauth/token")
	@ResponseBody
	public Map<String,Object> token(@RequestHeader HttpHeaders header, TokenInfo tokenInfo) {
		log.info("贝瑞花生壳接收到消息token 并转发到本地");
		String accessToken = UUID.fastUUID().toString(true);
		Map<String,Object> resultMap = new HashMap<>(8);
		resultMap.put("access_token",accessToken);
		resultMap.put("refresh_token",accessToken);
		resultMap.put("expires_in",72000);
		resultMap.put("token_type","bearer");
		resultMap.put("scope","read write");
		resultMap.put("openid", "");
		return resultMap;
	}


	/**
	 * 思必驰接入
	 * @param request
	 * @param response
	 * @param strJson
	 * @return
	 */
	@RequestMapping(value = "speech", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String speech(HttpServletRequest request, HttpServletResponse response, @RequestBody String strJson) {
		log.info("贝瑞花生壳接收到消息speech 并转发到本地");
		// 转换到对应的 开发环境,即模拟开发环境的请求信息.
		return HttpUtil.createPost("https://devl.unisiot.com:8152/SKILL/smarthome/speech")
				.header("Accesstoken","a4878e80f7894025af3c7c3003c898b0")
				.body(strJson)
				.execute().body();
	}



	// 方法用于将对象转换为 URL 编码的参数字符串
	private String toUrlEncodedParams(AuthorizeInfo request){
		StringBuilder sb = new StringBuilder();
		if (request.getClient_id() != null) {
			appendParam(sb, "client_id", request.getClient_id());
		}
		if (request.getRedirect_uri() != null) {
			appendParam(sb, "redirect_uri", request.getRedirect_uri());
		}

		if (request.getState() != null) {
			appendParam(sb, "state", request.getState());
		}
		if (request.getScope() != null) {
			appendParam(sb, "scope", request.getScope());
		}
		if (request.getResponse_type() != null) {
			appendParam(sb, "response_type", request.getResponse_type());
		}
		if (request.getUser_locale() != null) {
			appendParam(sb, "user_locale", request.getUser_locale());
		}

		return sb.toString();
	}


	private String toUrlTokenEncodedParams(TokenInfo request){
		StringBuilder sb = new StringBuilder();
		if (request.getClient_id() != null) {
			appendParam(sb, "client_id", request.getClient_id());
		}
		if (request.getRedirect_uri() != null) {
			appendParam(sb, "redirect_uri", request.getRedirect_uri());
		}

		if (request.getState() != null) {
			appendParam(sb, "state", request.getState());
		}
		if (request.getScope() != null) {
			appendParam(sb, "scope", request.getScope());
		}
		if (request.getResponse_type() != null) {
			appendParam(sb, "response_type", request.getResponse_type());
		}
		if (request.getUser_locale() != null) {
			appendParam(sb, "user_locale", request.getUser_locale());
		}

		if (request.getClient_secret() != null) {
			appendParam(sb, "client_secret", request.getClient_secret());
		}
		if (request.getGrant_type() != null) {
			appendParam(sb, "grant_type", request.getGrant_type());
		}
		if (request.getCode() != null) {
			appendParam(sb, "code", request.getCode());
		}


		return sb.toString();
	}



	private static void appendParam(StringBuilder sb, String name, String value)  {
		if (sb.length() > 0) {
			sb.append("&");
		}
		try {
			sb.append(URLEncoder.encode(name, String.valueOf(StandardCharsets.UTF_8)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		sb.append("=");
		try {
			sb.append(URLEncoder.encode(value, String.valueOf(StandardCharsets.UTF_8)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
