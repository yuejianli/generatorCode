package top.yueshushu.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.ftp.Ftp;
import cn.hutool.extra.ftp.FtpMode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.model.tool.FtpRo;
import top.yueshushu.model.tool.ToolRo;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>FtpController此类用于 FTP处理 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2025/2/7 16:29</p>
 * <p>@remark:</p>
 */
@RestController
@RequestMapping("/ftp")
@Api(value = "FTP小工具", hidden = true)
@ApiIgnore
public class FtpController {

	@PostMapping("/getContent")
	@ApiOperation("获取文件的内容")
	public OutputResult<String> getContent(@RequestBody FtpRo ftpRo) throws Exception {
		String content = readFileFromFtp(ftpRo.getServer(), ftpRo.getPort(), ftpRo.getUser(), ftpRo.getPassword(), ftpRo.getFilePath());
		return OutputResult.ok(content);
	}


	public String readFileFromFtp(String server, int port, String user, String password, String filePath) throws Exception {
		FTPClient ftpClient = new FTPClient();
		StringBuilder content = new StringBuilder();

		try {
			// 连接到FTP服务器
			ftpClient.connect(server, port);
			// 登录FTP服务器
			if (!ftpClient.login(user, password)) {
				throw new IOException("无法登录FTP服务器");
			}

			// 设置文件类型为二进制
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			// 使用被动模式
			ftpClient.enterLocalPassiveMode();

			// 读取文件
			boolean success = ftpClient.retrieveFile(filePath, new FileOutputStream("/tmp/tempfile"));
			if (success) {
				// 读取临时文件内容
				java.io.FileReader fr = new java.io.FileReader("/tmp/tempfile");
				BufferedReader br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					content.append(line).append("\n");
				}
				br.close();
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				// 登出并断开连接
				ftpClient.logout();
				ftpClient.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return content.toString();
	}


}
