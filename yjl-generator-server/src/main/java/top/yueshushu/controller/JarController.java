package top.yueshushu.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.core.text.csv.CsvWriter;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.model.JarParam;
import top.yueshushu.model.tool.ToolRo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>JarController此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/13 16:44</p>
 * <p>@remark:</p>
 */
@Slf4j
@RestController
@RequestMapping("/jar")
public class JarController {
	@GetMapping("/extraction")
	@ApiOperation("提取ps -aux |grep java的进程信息")
	public List<String> extraction() {
		// 读取文件的内容
		List<String> lineList = FileUtil.readLines("F:\\project.txt", Charset.defaultCharset());
		// 对每一个字符串进行处理
		List<JarParam> paramList = new ArrayList<>();

		for (String str: lineList) {
			// 非 jar信息
			if (!(str.contains("/bin/java") && str.contains(" -jar "))) {
				continue;
			}
			try {
				// 提取出jar包名称
				String jarPath = str.substring(str.indexOf(" -jar ") +5).trim();
				//
				JarParam jarParam = new JarParam();
				String[] pathArr = jarPath.split("/");

				String fileFullName = pathArr[pathArr.length -1];
				// 进行替换，找到名称
				String fileName = fileFullName.replace(".jar", "").replace("-1.0-SNAPSHOT", "");

				String regex = "-Dproj\\.logs=(.*)";

				// 创建 Pattern 对象
				Pattern pattern = Pattern.compile(regex);

				// 创建 Matcher 对象
				Matcher matcher = pattern.matcher(str);

				// 检查是否匹配
				if (matcher.find()) {
					// 提取匹配的值
					String value = matcher.group(1);
					if (StrUtil.isBlank(value)) {
						continue;
					}

					jarParam.setLogsPath(value.split(" ")[0]);
				} else {
					continue;
				}
				jarParam.setFilePath(jarPath);
				jarParam.setFileName(fileName);
				jarParam.setFileFullName(fileFullName);
				log.info("jarParam: {}", jarParam);
				paramList.add(jarParam);
			}catch (Exception e) {
				log.error("出现了异常 {}",str,e);
			}
		}
		// paramList 按照 logsPath 和文件名称进行排序
		paramList.sort(Comparator.comparing(JarParam::getLogsPath)
				.thenComparing(JarParam::getFileName));

		List<String> resultList = new ArrayList<>();
		List<List<String>> data = new ArrayList<>();
		paramList.forEach(
				n-> {
					resultList.add(n.toString());
					List<String> row = new ArrayList<>();
					row.add(n.getFileName());
					row.add(n.getFilePath());
					row.add(n.getLogsPath());
					data.add(row);
				}
		);
		// 客户到 csv 文件里面
		writeDataToCsv(data,"F:\\project.csv");
		return resultList;
	}
	public static void writeDataToCsv(List<List<String>> data, String fileName) {
		CsvWriter writer = CsvUtil.getWriter(new File(fileName),Charset.defaultCharset()); // 第二个参数表示是否自动关闭流，默认为 false
		writer.write(data);
		System.out.println("CSV 文件已成功写入：" + fileName);
	}
}
