package top.yueshushu.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.yueshushu.model.maven.MavenRo;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;

/**
 * <p>MavenController此类用于 Maven 安装到本地 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/5/13 0013 8:08</p>
 * <p>@remark:</p>
 */
@Slf4j
@Controller
@RequestMapping("/maven")
public class MavenController {

	@Value("${mavenDownloadPath:E:\\\\javaRepositoryDownloadJar}")
	private String mavenDownloadPath ;

	public static final String MAVEN_INSTALL_COMMAND = "mvn install:install-file -Dfile={} -DgroupId={} -DartifactId={} -Dversion={} -Dpackaging=jar";
	public static final String MAVEN_INSTALL_RESULT = "File: {} groupId={} artifactId={} version={} 安装到本地成功";

	@GetMapping("/simpleTest")
	public String simpleTest() {
		// Maven命令和参数
		// 你可以根据需要修改这些参数
		String command = "cmd /c mvn -v";

		// 创建ProcessBuilder实例
		Runtime r = Runtime.getRuntime();

		// 设置工作目录（可选，如果需要的话）
		// processBuilder.directory(new File("path/to/your/maven/project"));

		// 尝试启动进程
		try {
			Process process = r.exec(command);

			// 读取标准输出
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

			// 等待进程结束
			int exitCode = process.waitFor();
			System.out.println("\nExited with error code : " + exitCode);
			return "运行maven 成功";

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			return "运行maven 失败";
		}
	}


	@RequestMapping("/installMaven")
	public String installMaven(@RequestBody MavenRo mavenRo) {
		if (!StringUtils.hasText(mavenRo.getFileDownloadUrl())) {
			mavenRo.setFileDownloadUrl(mavenDownloadPath);
		}
		//1. 下载网络文件到本地
		File downFile = HttpUtil.downloadFileFromUrl(mavenRo.getUrl(), mavenRo.getFileDownloadUrl());

		// 2. 取出文件名，解析出对应的信息
		String fileName = downFile.getName();
		
		if (!StringUtils.hasText(mavenRo.getGroupId())) {
			mavenRo.setGroupId(convertFileNameToGroupId(fileName));
		}
		if (!StringUtils.hasText(mavenRo.getArtifactId())) {
			mavenRo.setArtifactId(convertFileNameToArtifactId(fileName));
		}
		if (!StringUtils.hasText(mavenRo.getVersion())) {
			mavenRo.setVersion(convertFileNameToVersion(fileName));
		}
		mavenRo.setFileDownloadUrl(mavenRo.getFileDownloadUrl() + File.separator + fileName);

		log.info("安装 mavenRo对象:{}",mavenRo);
		// 执行的命令（例如：列出当前目录下的文件和文件夹）
		String command = buildCommand(mavenRo);
		try {

			// 执行命令
			String newMvnCommand = command.replace("mvn", "cmd /c mvn").trim();
			log.info("执行命令:{}", newMvnCommand);
			Runtime r = Runtime.getRuntime();
			Process process = r.exec(newMvnCommand);

			// 读取命令输出
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				log.info(line);
			}

			// 等待命令执行完成
			int exitCode = process.waitFor();
			log.info("with error code :{}", exitCode);

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			return command;
		}

		String result =  StrUtil.format(MAVEN_INSTALL_RESULT,
				mavenRo.getFileDownloadUrl(),
				mavenRo.getGroupId(), mavenRo.getArtifactId(), mavenRo.getVersion());
		log.info(result);
		return command;
	}

	private String buildCommand(MavenRo mavenRo) {
		return StrUtil.format(MAVEN_INSTALL_COMMAND,
				mavenRo.getFileDownloadUrl(),
				mavenRo.getGroupId(), mavenRo.getArtifactId(), mavenRo.getVersion());
	}

	private String convertFileNameToVersion(String fileName) {
		String artifactId = convertFileNameToArtifactId(fileName);
		// 删除 artifactId
		fileName = fileName.replace(artifactId+"-", "");

		String[] versionSplit = fileName.split("-");
		// 多个，一般为 版本号和日期
		if (versionSplit.length > 1) {
			return versionSplit[0] + "-SNAPSHOT";
		}
		// 是正式版本
		return versionSplit[0];
	}

	private String convertFileNameToArtifactId(String fileName) {
		// 将 fileName 	按照 - 进行拆分，直到 1.0 或者 0.01结束
		StringBuffer stringBuffer = new StringBuffer();
		String[] split = fileName.split("-");

		for (int i = 0; i < split.length; i++) {
			String s = split[i];
			if (s.startsWith("1.") || s.startsWith("0.")) {
				break;
			}
			stringBuffer.append(s).append("-");
		}
		// 删除最后一个 -
		stringBuffer.deleteCharAt(stringBuffer.length() - 1);
		return stringBuffer.toString();
	}
	private String convertFileNameToGroupId(String fileName) {
		String artifactId = convertFileNameToArtifactId(fileName);
		// 去掉最后一个 -信息
		int lastIndexOf = artifactId.lastIndexOf("-");
		artifactId = artifactId.substring(0, lastIndexOf);

		artifactId = artifactId.replace("-", ".");

		String module ;
		if (artifactId.startsWith("back")) {
			module = artifactId;
		}else if (artifactId.startsWith("iot")) {
			module = artifactId.substring(4);
		}else if (artifactId.startsWith("micro")) {
			module = "";
		} else {
			module = artifactId;
		}
		return "com.uiotsoft." + module;
	}
}
