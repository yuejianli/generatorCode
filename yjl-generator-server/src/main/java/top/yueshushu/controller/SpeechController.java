package top.yueshushu.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.yueshushu.util.JsonUtil;

import java.util.Map;

/**
 * <p>SpeechController此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/29 18:38</p>
 * <p>@remark:</p>
 */
@Controller
@Slf4j
@RequestMapping("SKILL/smarthome")
public class SpeechController {
	@RequestMapping(value = "speechControl", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String speechControl(@RequestBody String strJson) {
		log.info("---------------*speech*----------------"+ strJson);
		Map<String, Object> rqMap = JsonUtil.jsonToMap(strJson);
		Map<String, Object> headerMap = (Map<String, Object>) rqMap.get("header");
		String messageId = (String) headerMap.get("messageId");

		String responseText =  "{\n" +
				"    \"header\": {\n" +
				"        \"messageId\": \"1bd5d003-31b9-476f-ad03-71d471922820\",\n" +
				"        \"name\": \"Discover\",\n" +
				"        \"namespace\": \"DUI.SmartHome.Discovery\",\n" +
				"        \"payloadVersion\": 1\n" +
				"    },\n" +
				"    \"payload\": {\n" +
				"        \"appliances\": [\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryTemperature\",\n" +
				"                    \"QueryHumidity\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentEnvironment\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"1ce74cd71ce74cd7\",\n" +
				"                    \"hardwareVersion\": \"V3.20\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 22,\n" +
				"                    \"softwareVersion\": \"V3.60\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1431\",\n" +
				"                \"applianceType\": \"SENSOR\",\n" +
				"                \"description\": \"四合一精灵CO2\",\n" +
				"                \"friendlyName\": \"四合一精灵CO2\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"env_4_1_air_genius_co2\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryPM25\",\n" +
				"                    \"QueryTemperature\",\n" +
				"                    \"QueryHumidity\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentEnvironment\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"c2763b3fc2763b3f\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.55\"\n" +
				"                },\n" +
				"                \"applianceId\": \"735\",\n" +
				"                \"applianceType\": \"SENSOR\",\n" +
				"                \"description\": \"七合一空气盒子smj\",\n" +
				"                \"friendlyName\": \"七合一空气盒子smj\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"env_7_1_air_box\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryTemperature\",\n" +
				"                    \"QueryHumidity\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentEnvironment\",\n" +
				"                    \"controlType\": 2,\n" +
				"                    \"deviceMac\": \"00158d0005d42c22\",\n" +
				"                    \"hardwareVersion\": \"V4.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V4.25\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1503\",\n" +
				"                \"applianceType\": \"SENSOR\",\n" +
				"                \"description\": \"温湿度探头\",\n" +
				"                \"friendlyName\": \"温湿度探头\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"env_temp_hum_sensor\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryTemperature\",\n" +
				"                    \"QueryHumidity\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentEnvironment\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"0000000400000004\",\n" +
				"                    \"hardwareVersion\": \"V5.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 24,\n" +
				"                    \"softwareVersion\": \"V3.60\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1507\",\n" +
				"                \"applianceType\": \"SENSOR\",\n" +
				"                \"description\": \"温湿度探头\",\n" +
				"                \"friendlyName\": \"温湿度探头\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"env_temp_hum_sensor\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"IncreaseChannel\",\n" +
				"                    \"DecreaseChannel\",\n" +
				"                    \"IncreaseVolume\",\n" +
				"                    \"DecreaseVolume\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentHomeAppliance\",\n" +
				"                    \"controlType\": 2,\n" +
				"                    \"deviceMac\": \"00158d00072a28f4\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 7,\n" +
				"                    \"softwareVersion\": \"V4.30\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1464\",\n" +
				"                \"applianceType\": \"SET_TOP_BOX\",\n" +
				"                \"description\": \"机顶盒1\",\n" +
				"                \"friendlyName\": \"机顶盒1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"ha_ir_set_top_box\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"IncreaseChannel\",\n" +
				"                    \"DecreaseChannel\",\n" +
				"                    \"IncreaseVolume\",\n" +
				"                    \"DecreaseVolume\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentHomeAppliance\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0005692f43\",\n" +
				"                    \"hardwareVersion\": \"V4.11\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 7,\n" +
				"                    \"softwareVersion\": \"V3.60\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1522\",\n" +
				"                \"applianceType\": \"SET_TOP_BOX\",\n" +
				"                \"description\": \"机顶盒2\",\n" +
				"                \"friendlyName\": \"机顶盒2\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"ha_ir_set_top_box\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"IncreaseChannel\",\n" +
				"                    \"DecreaseChannel\",\n" +
				"                    \"IncreaseVolume\",\n" +
				"                    \"DecreaseVolume\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentHomeAppliance\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0005692f43\",\n" +
				"                    \"hardwareVersion\": \"V4.11\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 7,\n" +
				"                    \"softwareVersion\": \"V3.60\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1523\",\n" +
				"                \"applianceType\": \"SET_TOP_BOX\",\n" +
				"                \"description\": \"机顶盒\",\n" +
				"                \"friendlyName\": \"机顶盒\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"ha_ir_set_top_box\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentHomeAppliance\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"5a5bfed75a5bfed7\",\n" +
				"                    \"hardwareVersion\": \"V2.15\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.51\"\n" +
				"                },\n" +
				"                \"applianceId\": \"734\",\n" +
				"                \"applianceType\": \"SOCKET\",\n" +
				"                \"description\": \"智能插座smj\",\n" +
				"                \"friendlyName\": \"智能插座smj\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"ha_smart_socket\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"SetTemperature\",\n" +
				"                    \"QueryTemperature\",\n" +
				"                    \"IncreaseTemperature\",\n" +
				"                    \"DecreaseTemperature\",\n" +
				"                    \"SetWindSpeed\",\n" +
				"                    \"QueryWindSpeed\",\n" +
				"                    \"IncreaseWindSpeed\",\n" +
				"                    \"DecreaseWindSpeed\",\n" +
				"                    \"SetMode\",\n" +
				"                    \"QueryMode\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentHvac\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"C8F09E4BA9E4\",\n" +
				"                    \"hardwareVersion\": \"V2.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V1.01.005\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1595\",\n" +
				"                \"applianceType\": \"AIR_CONDITION\",\n" +
				"                \"description\": \"码库空调\",\n" +
				"                \"friendlyName\": \"码库空调\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"hvac_ir_air_conditioner_maku\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:09:39:69:84-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"22\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 2\",\n" +
				"                \"friendlyName\": \"Hue color lamp 2\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:02:d9:1d:3c-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"23\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 3\",\n" +
				"                \"friendlyName\": \"Hue color lamp 3\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:08:5f:88:17-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"24\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue ambiance downlight 1\",\n" +
				"                \"friendlyName\": \"Hue ambiance downlight 1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:08:5f:8f:b1-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"25\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue ambiance downlight 2\",\n" +
				"                \"friendlyName\": \"Hue ambiance downlight 2\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:02:93:78:c3-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"26\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 4\",\n" +
				"                \"friendlyName\": \"Hue color lamp 4\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:08:5f:e0:9f-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"27\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue ambiance downlight 3\",\n" +
				"                \"friendlyName\": \"Hue ambiance downlight 3\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:09:39:68:56-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"28\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 9\",\n" +
				"                \"friendlyName\": \"Hue color lamp 9\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:08:5f:ca:03-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"29\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue ambiance downlight 6\",\n" +
				"                \"friendlyName\": \"Hue ambiance downlight 6\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:15:8d:00:06:5b:76:d0-01\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"30\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"On/Off light 1\",\n" +
				"                \"friendlyName\": \"On/Off light 1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:09:39:69:5f-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"63\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 1\",\n" +
				"                \"friendlyName\": \"Hue color lamp 1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:09:3e:10:96-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"64\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 2\",\n" +
				"                \"friendlyName\": \"Hue color lamp 2\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:02:44:3a:7e-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"65\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue color lamp 4\",\n" +
				"                \"friendlyName\": \"Hue color lamp 4\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 3,\n" +
				"                    \"deviceMac\": \"00:17:88:01:08:5f:7c:11-0b\",\n" +
				"                    \"hardwareVersion\": \"\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"\"\n" +
				"                },\n" +
				"                \"applianceId\": \"66\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"Hue ambiance downlight 1\",\n" +
				"                \"friendlyName\": \"Hue ambiance downlight 1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_d_philips_hue_light\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TurnOn\",\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"SetBrightness\",\n" +
				"                    \"QueryBrightness\",\n" +
				"                    \"IncreaseBrightness\",\n" +
				"                    \"DecreaseBrightness\",\n" +
				"                    \"SetColorTemperature\",\n" +
				"                    \"IncreaseColorTemperature\",\n" +
				"                    \"DecreaseColorTemperature\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0009637a89\",\n" +
				"                    \"hardwareVersion\": \"V5.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.62\"\n" +
				"                },\n" +
				"                \"applianceId\": \"632\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"智能色温射灯smj\",\n" +
				"                \"friendlyName\": \"智能色温射灯smj\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_smart_color_temperature_spotlight\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TurnOn\",\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"SetBrightness\",\n" +
				"                    \"QueryBrightness\",\n" +
				"                    \"IncreaseBrightness\",\n" +
				"                    \"DecreaseBrightness\",\n" +
				"                    \"SetColorTemperature\",\n" +
				"                    \"IncreaseColorTemperature\",\n" +
				"                    \"DecreaseColorTemperature\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"76469cd976469cd9\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.57\"\n" +
				"                },\n" +
				"                \"applianceId\": \"633\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"灯具_25\",\n" +
				"                \"friendlyName\": \"灯具_25\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_smart_color_temperature_spotlight\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"b40ecf0109a80000\",\n" +
				"                    \"hardwareVersion\": \"V4.63\",\n" +
				"                    \"channelNum\": 0,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"634\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"双开零火开关smj\",\n" +
				"                \"friendlyName\": \"双开零火开关smj\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_double_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"b40ecf0109a80000\",\n" +
				"                    \"hardwareVersion\": \"V4.63\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"635\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"双开零火开关0\",\n" +
				"                \"friendlyName\": \"双开零火开关0\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_double_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"b40ecf0109a80000\",\n" +
				"                    \"hardwareVersion\": \"V4.63\",\n" +
				"                    \"channelNum\": 2,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"636\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"双开零火开关1\",\n" +
				"                \"friendlyName\": \"双开零火开关1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_double_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"b40ecf00dc940000\",\n" +
				"                    \"hardwareVersion\": \"V5.11\",\n" +
				"                    \"channelNum\": 0,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.70\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1394\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"单开零火开关\",\n" +
				"                \"friendlyName\": \"单开零火开关\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_single_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"b40ecf00dc940000\",\n" +
				"                    \"hardwareVersion\": \"V5.11\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.70\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1395\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"穿墙模式\",\n" +
				"                \"friendlyName\": \"穿墙模式\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_single_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"7cb94c79465f0000\",\n" +
				"                    \"hardwareVersion\": \"V5.20\",\n" +
				"                    \"channelNum\": 0,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.60\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1591\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"单开零火开关\",\n" +
				"                \"friendlyName\": \"单开零火开关\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_single_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"7cb94c79465f0000\",\n" +
				"                    \"hardwareVersion\": \"V5.20\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.60\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1592\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"单开零火开关0\",\n" +
				"                \"friendlyName\": \"单开零火开关0\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_single_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0007d1aef0\",\n" +
				"                    \"hardwareVersion\": \"V5.11\",\n" +
				"                    \"channelNum\": 0,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 22,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"41\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开零火开关的\",\n" +
				"                \"friendlyName\": \"三开零火开关的\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0007d1aef0\",\n" +
				"                    \"hardwareVersion\": \"V5.11\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 22,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"42\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开0\",\n" +
				"                \"friendlyName\": \"三开0\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0007d1aef0\",\n" +
				"                    \"hardwareVersion\": \"V5.11\",\n" +
				"                    \"channelNum\": 2,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 22,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"43\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开1\",\n" +
				"                \"friendlyName\": \"三开1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"00158d0007d1aef0\",\n" +
				"                    \"hardwareVersion\": \"V5.11\",\n" +
				"                    \"channelNum\": 3,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 22,\n" +
				"                    \"softwareVersion\": \"V3.68\"\n" +
				"                },\n" +
				"                \"applianceId\": \"44\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开2\",\n" +
				"                \"friendlyName\": \"三开2\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"1a4fc55f1a4fc55f\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 0,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 19,\n" +
				"                    \"softwareVersion\": \"V3.55\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1440\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开零火开关\",\n" +
				"                \"friendlyName\": \"三开零火开关\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"1a4fc55f1a4fc55f\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.55\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1441\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"主灯\",\n" +
				"                \"friendlyName\": \"主灯\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"1a4fc55f1a4fc55f\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 2,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 22,\n" +
				"                    \"softwareVersion\": \"V3.55\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1442\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开零火开关1\",\n" +
				"                \"friendlyName\": \"三开零火开关1\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 4,\n" +
				"                    \"deviceType\": \"intelligentLighting\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"1a4fc55f1a4fc55f\",\n" +
				"                    \"hardwareVersion\": \"V3.10\",\n" +
				"                    \"channelNum\": 3,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 19,\n" +
				"                    \"softwareVersion\": \"V3.55\"\n" +
				"                },\n" +
				"                \"applianceId\": \"1443\",\n" +
				"                \"applianceType\": \"LIGHT\",\n" +
				"                \"description\": \"三开零火开关22的11\",\n" +
				"                \"friendlyName\": \"三开零火开关22的11\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"l_zf_three_switch\",\n" +
				"                \"zone\": \"二楼\"\n" +
				"            },\n" +
				"            {\n" +
				"                \"actions\": [\n" +
				"                    \"TimingTurnOn\",\n" +
				"                    \"TurnOn\",\n" +
				"                    \"QueryPowerState\",\n" +
				"                    \"TimingTurnOff\",\n" +
				"                    \"TurnOff\",\n" +
				"                    \"Pause\",\n" +
				"                    \"SetOpenDegree\",\n" +
				"                    \"IncreaseOpenDegree\",\n" +
				"                    \"DecreaseOpenDegree\"\n" +
				"                ],\n" +
				"                \"additionalApplianceDetails\": {\n" +
				"                    \"floorId\": 1,\n" +
				"                    \"deviceType\": \"intelligentWindowCurtain\",\n" +
				"                    \"controlType\": 1,\n" +
				"                    \"deviceMac\": \"8d3634b38d3634b3\",\n" +
				"                    \"hardwareVersion\": \"V1.00\",\n" +
				"                    \"channelNum\": 1,\n" +
				"                    \"userUnique\": \"e68750732fa1b13fb3af50fbf7f4f794\",\n" +
				"                    \"sn\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                    \"roomId\": 6,\n" +
				"                    \"softwareVersion\": \"V3.55\"\n" +
				"                },\n" +
				"                \"applianceId\": \"639\",\n" +
				"                \"applianceType\": \"CURTAIN\",\n" +
				"                \"description\": \"智能窗帘电机smj\",\n" +
				"                \"friendlyName\": \"智能窗帘电机smj\",\n" +
				"                \"group\": \"2321g9a58de34094aa4f409719d67454\",\n" +
				"                \"manufacturerName\": \"UIOT超级智慧家\",\n" +
				"                \"modelName\": \"wc_smart_curtain_motor\",\n" +
				"                \"zone\": \"一楼\"\n" +
				"            }\n" +
				"        ]\n" +
				"    }\n" +
				"}";
		responseText.replace("1bd5d003-31b9-476f-ad03-71d471922820",messageId).replaceAll("\\n","");
		return responseText;
	}
}
