package top.yueshushu.controller;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import sun.misc.BASE64Decoder;
import top.yueshushu.common.exception.BusinessException;
import top.yueshushu.util.MyHttpAUtil;
import top.yueshushu.util.RSAUtil;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * <p>StudyFileController此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/8/15 14:37</p>
 * <p>@remark:</p>
 */
@RestController
@RequestMapping("/file")
@Api(value = "下载视频文件", hidden = true)
@Slf4j
public class StudyFileController {
//	@Resource
//	private RestTemplate restTemplate;
//	@Resource
//	private CloseableHttpClient httpClient;
//	@Resource
//	private RedisUtil redisUtil;
//
//	private static final String FILE_URL = "D:\\培训资料\\";
//
//	public static  String TOKEN = "";
//	@GetMapping("/initToken")
//	public void initToken (String token) {
//		TOKEN = token;
//		redisUtil.set("token",TOKEN);
//	}
//	@GetMapping("/download")
//	public String downloadFile(String url) {
//		// 根据http地址下载文件
//		Map<String,String> requestHeaders = new HashMap<>();
//		requestHeaders.put("referer", "http://video.iot.loc:8101/");
//		requestHeaders.put("origin", "http://video.iot.loc:8101");
//		requestHeaders.put("host", "172.16.8.11:3627");
//		requestHeaders.put("Token", getToken());
//		String responseText = MyHttpAUtil.sendGet(httpClient,url, requestHeaders);
//		// 对内容进行解析，获取 data.chapterInfo 信息。
//		// 将 responseText 转换成 jsonObject 对象
//		JSONObject jsonObject = JSONUtil.parseObj(responseText);
//		// 获取 data.chapterInfo 信息。
//		JSONObject data = jsonObject.getJSONObject("data");
//		JSONArray jsonArray = data.getJSONArray("chapterInfo");
//
//		// 对每一个文件进行处理.
//		jsonArray.forEach(item -> {
//			// 获取文件名称
//			// 将item 转换成为 JSONObject 对象
//			JSONObject itemObj = JSONUtil.parseObj(item);
//
//			String fileName = itemObj.get("title").toString();
//
//			// 文件目录不存在，则创建
//			File file = new File(FILE_URL + fileName);
//			if (!file.exists()) {
//				file.mkdirs();
//			}
//
//			JSONArray children = itemObj.getJSONArray("children");
//			children.forEach(child -> {
//				// 获取文件名称
//				JSONObject childObj = JSONUtil.parseObj(child);
//				Object fileUrlObj = childObj.get("fileUrl");
//				if (fileUrlObj != null && StrUtil.isNotBlank(fileUrlObj.toString())) {
//					// 下载文件
//					String fullName = FILE_URL + fileName + File.separator + childObj.get("fileName").toString();
//
//					File file1 = new File(fullName);
//
//					if (!file1.exists()) {
//						HttpUtil.downloadFile(fileUrlObj.toString(), fullName);
//					} else {
//						log.info("文件已经存在，跳过下载,{}", fullName);
//					}
//				}
//			});
//		});
//
//		return "请求成功";
//	}
//
//	private String getToken() {
//		if (StrUtil.isNotBlank(TOKEN)) {
//			return TOKEN;
//		}
//		TOKEN = redisUtil.get("token");
//		return TOKEN;
//	}
//
//
//	@GetMapping("/downloadAllByCategory")
//	public String downloadAllByCategory(String categoryParentId,Integer totalPage) {
//		// 根据http地址下载文件
//		Map<String,String> requestHeaders = new HashMap<>();
//		requestHeaders.put("referer", "http://video.iot.loc:8101/");
//		requestHeaders.put("origin", "http://video.iot.loc:8101");
//		requestHeaders.put("host", "172.16.8.11:3627");
//		requestHeaders.put("Token", getToken());
//		Map<String, Object> params = new HashMap<>();
//		params.put("categoryParentId", categoryParentId);
//		params.put("categoryId", "");
//		for (int i = 1; i <= totalPage; i++) {
//			String url = "http://172.16.8.11:3627/learning/front/course/"+i+"/8";
//			String responseText = MyHttpAUtil.sendPostJson(httpClient,url,params, requestHeaders);
//			// 对内容进行解析，获取 data.chapterInfo 信息。
//			// 将 responseText 转换成 jsonObject 对象
//			JSONObject jsonObject = JSONUtil.parseObj(responseText);
//			// 获取 data.chapterInfo 信息。
//			JSONObject data = jsonObject.getJSONObject("data");
//			JSONArray jsonArray = data.getJSONArray("courses");
//
//			// 对每一个文件进行处理.
//			jsonArray.forEach(item -> {
//				// 将item 转换成为 JSONObject 对象
//				JSONObject itemObj = JSONUtil.parseObj(item);
//				String id = itemObj.get("id").toString();
//				String downUrl = "http://172.16.8.11:3627/learning/front/course/" +id;
//				downloadFile(downUrl);
//				ThreadUtil.safeSleep(200);
//			});
//
//		}
//		return "请求成功";
//	}
//
//
//	@GetMapping("/startView")
//	public String startView(String url,Integer lastTime,String totalTime) {
//		// 根据http地址下载文件
//		Map<String,String> requestHeaders = new HashMap<>();
//		requestHeaders.put("Referer", "http://video.iot.loc:8101/");
//		requestHeaders.put("Origin", "http://video.iot.loc:8101");
//		requestHeaders.put("Host", "172.16.8.11:3627");
//		requestHeaders.put("Token",getToken());
//		requestHeaders.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");
//
//
//
//		String startUrl = url + "?allTime=0";
//		MyHttpAUtil.sendGet(httpClient,startUrl, requestHeaders);
//
//		// 进行请求该数，并且到结束处理。
//		ThreadUtil.execAsync(
//				()->{
//					// 获取一个随机的数
//					int random = RandomUtil.randomInt(0, 9);
//
//					Integer totalNum = RandomUtil.randomInt(150,300);
//					if (StrUtil.isNotBlank(totalTime)) {
//						String[] split = totalTime.split(":");
//
//						totalNum = (Integer.parseInt(split[0]) * 60 + Integer.parseInt(split[1]) ) /10 -1;
//					}
//
//					log.info("共运行 {}次", totalNum);
//
//
//					Integer nowLastTime = lastTime ;
//					if (lastTime == null || lastTime == 0) {
//						nowLastTime = random;
//					}
//					int subMinute = 0;
//					int cycleNum = 0;
//					for (int i = 0; i < totalNum; i++) {
//						String url1 = url.replace("reocrd","last") + "?lastTime=" + nowLastTime;
//						log.info("请求的url:{}", url1);
//						String responseText1 = MyHttpAUtil.sendGet(httpClient,url1, requestHeaders);
//						ThreadUtil.safeSleep(1000 + RandomUtil.randomInt(100,500));
//						log.info("请求内容: {}",responseText1);
//						nowLastTime += 10;
//
//						subMinute += 1;
//						if (subMinute % 6 == 0) {
//							cycleNum = + 1;
//							subMinute = 0;
//							String newStartUrl = url+ "?allTime=" + 59 * cycleNum;
//							String responseText2 = MyHttpAUtil.sendGet(httpClient,newStartUrl, requestHeaders);
//							log.info(" {} 请求内容: {}",newStartUrl,responseText2);
//						}
//					}
//					if (subMinute != 0) {
//						String newStartUrl = url +"?allTime=" + 59 * cycleNum + subMinute * 10;
//						String responseText2 = MyHttpAUtil.sendGet(httpClient,newStartUrl, requestHeaders);
//						log.info(" {} 请求内容: {}",newStartUrl,responseText2);
//					}
//					log.info(" {} 爬虫结束 {}", url,totalNum);
//				},false
//		);
//		return "爬虫成功";
//	}
//
//
//	public static final String KEY_ALGORITHM = "RSA";
//	public static byte[] decryptBASE64(String key) throws Exception {
//		return (new BASE64Decoder()).decodeBuffer(key);
//	}
//
//	// 获取公钥
//	public static PublicKey getPublicKeyFromPem(String publicKey) throws Exception {
//		BASE64Decoder base64decoder = new BASE64Decoder();
//		byte[] b = base64decoder.decodeBuffer(publicKey);
//		KeyFactory kf = KeyFactory.getInstance(KEY_ALGORITHM);
//		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(b);
//		PublicKey pubKey = kf.generatePublic(keySpec);
//		return pubKey;
//	}
//
//	/**
//	 * 用公钥解密
//	 *
//	 * @param data 密文
//	 * @param publicKey 公钥
//	 * @return
//	 * @throws Exception
//	 */
//// RSA最大解密密文大小
//	private static final int MAX_DECRYPT_BLOCK = 128;
//
//	public static byte[] decryptByPublicKey(byte[] data, PublicKey publicKey) throws Exception {
//		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
//		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
//		cipher.init(Cipher.DECRYPT_MODE, publicKey);
//		byte[] enBytes = null;
//		for (int i = 0; i < data.length; i += MAX_DECRYPT_BLOCK) {
//			byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + MAX_DECRYPT_BLOCK));
//			enBytes = ArrayUtils.addAll(enBytes, doFinal);
//		}
//		return enBytes;
//	}
//
//
//
//	public static void main(String[] args) throws Exception {
//		// 假设这里有一个公钥字符串
//		String publicKey =
//				"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQXIp1TgEoD/QHOAJusQ/ZvSoC+2nxVLQHrc31w4F+wVsalUqWjyWIGMT8fj+Ft2mQjpzlIpxqHRykn2fZOqmRnauAwN4n/8Ipu1sQKBCvhBn+s2T9psBdCROFrhHr3t1w1rGnl5NtS7UWijY8tedy6S0O90ZOR7S1Z6w0iU9bYQIDAQAB";
//
//		// 解密的数据（通常这是加密后的数据）
//		String data = "HRArNEUQTYySCS30GVPPwF6fs0fOtnGuqbi37UZnGNsj9ZBCi2jSnIrMX3yhwmgrY9X3s284gM/vf7+n7510wFoDLT7O+5E4dCRDB2O8sZ50notiM7o5/eVIeeOIZ2/Pn2Ff1FRIeotj97D0JN6NzxTHzdkBlsk1wBgASfKfnymCOpmA7ZomiVimHjY760QItkV17G1w098jpJ5JquaXglsnYF5AQA59DQAuRXMyKN8qJwxj2FZLAVqb6cQca01LL3THlGRHBgxgkz8eG2AcKUaYQuiN5PYCSyDp9+xLPtA/PoR3VHrYQVf+9IseUWr4m3EXxejGxNzaEQefN6MLKQ==";
////		byte[] privateDataBytes = decryptBASE64(data);
////		byte[] dataBytes = decryptByPublicKey(privateDataBytes, getPublicKeyFromPem(publicKey));
////		System.out.println("公钥解密：" + new String(dataBytes, StandardCharsets.UTF_8));
//
//		System.out.println("code:" + genCodeVerifier());
//	}
//
//
//	public static String genCodeVerifier() {
//		String codeVerifier = IdUtil.fastSimpleUUID();
//		// codeChallenge：codeVerifier 的 SHA256
//		String codeChallenge = Base64Utils.encodeToUrlSafeString(DigestUtil.sha256(codeVerifier))
//				.replaceAll("=", "")
//				.replaceAll("\\+", "-")
//				.replaceAll("/", "_");
//		return codeChallenge;
//	}
}
