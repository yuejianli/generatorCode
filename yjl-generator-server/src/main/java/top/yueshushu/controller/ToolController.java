package top.yueshushu.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import top.yueshushu.business.ToolBusiness;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.model.tool.ToolRo;

import javax.annotation.Resource;

/**
 * 简单小工具相应的控制类
 *
 * @author yuejianli
 * @date 2023-10-08
 */
@RestController
@RequestMapping("/tool")
@Api(value = "简单小工具", hidden = true)
@ApiIgnore
public class ToolController {
    @Resource
    private ToolBusiness toolBusiness;

    @PostMapping("/removeSwagger")
    @ApiOperation("移除Swagger相应的注解信息")
    public OutputResult<String> removeSwagger(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.removeSwagger(toolRo);
    }

    @PostMapping("/removeMyBatis")
    @ApiOperation("移除MyBatis相应的注解信息")
    public OutputResult<String> removeMyBatis(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.removeMyBatis(toolRo);
    }

    @PostMapping("/removeEasyExcel")
    @ApiOperation("移除EasyExcel相应的注解信息")
    public OutputResult<String> removeEasyExcel(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.removeEasyExcel(toolRo);
    }

    @PostMapping("/removePrefixLine")
    @ApiOperation("移除相关的前缀行")
    public OutputResult<String> removePrefixLine(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.removePrefixLine(toolRo);
    }


    @PostMapping("/findCha")
    @ApiOperation("找两个文字的差集")
    public OutputResult<String> findCha(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.findCha(toolRo);
    }

    @PostMapping("/findJiao")
    @ApiOperation("找两个文字的差集")
    public OutputResult<String> findJiao(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.findJiao(toolRo);
    }

    @PostMapping("/getKey")
    @ApiOperation("获取某个属性的值")
    public OutputResult<String> getKey(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.getKey(toolRo);
    }

    @PostMapping("/removeNull")
    @ApiOperation("去掉为空的属性信息")
    public OutputResult<String> removeNull(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.removeNull(toolRo);
    }

    @PostMapping("/removeDbId")
    @ApiOperation("移除数据库的Id")
    public OutputResult<String> removeDbId(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        toolRo.setProperty("`id`");
        return toolBusiness.removeDbId(toolRo);
    }

    @PostMapping("/removeDbRuleId")
    @ApiOperation("移除数据库的Id")
    public OutputResult<String> removeDbRuleId(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        toolRo.setProperty("`ruleId`");
        return toolBusiness.removeDbId(toolRo);
    }



    @PostMapping("/addOemFirm")
    @ApiOperation("追加不同的信息到品牌")
    public OutputResult<String> addOemFirm(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.addOemFirm(toolRo);
    }


    @PostMapping("/prodOemFirm")
    @ApiOperation("追加到正式环境")
    public OutputResult<String> prodOemFirm(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.prodOemFirm(toolRo);
    }


    @PostMapping("/concat")
    @ApiOperation("连接信息为字符串")
    public OutputResult<String> concat(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.concat(toolRo);
    }


    @PostMapping("/convertToExcel")
    @ApiOperation("转换成Excel")
    public OutputResult<String> convertToExcel(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.convertToExcel(toolRo);
    }

    @PostMapping("/pojoToJson")
    @ApiOperation("转换成json")
    public OutputResult<String> pojoToJson(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.pojoToJson(toolRo);
    }

    @PostMapping("/excelToSql")
    @ApiOperation("插入到Excel表里面")
    public OutputResult<String> excelToSql(@RequestBody ToolRo toolRo) {
        if (!StringUtils.hasText(toolRo.getContent())) {
            return OutputResult.ok();
        }
        return toolBusiness.excelToSql(toolRo);
    }
}
