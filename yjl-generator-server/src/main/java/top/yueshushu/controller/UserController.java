package top.yueshushu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>UserController此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/29 16:44</p>
 * <p>@remark:</p>
 */
@Controller
public class UserController {

	@RequestMapping("getUserInfo")
	@ResponseBody
	public String getUserInfo() {
		return "get user info ok";
	}

}
