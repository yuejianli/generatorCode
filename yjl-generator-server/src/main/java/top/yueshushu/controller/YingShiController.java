package top.yueshushu.controller;

import com.alibaba.fastjson.JSON;
import com.ezviz.open.sdk.auth.token.resource.Action;
import com.ezviz.open.sdk.auth.token.resource.GeneralResourceTokenGenerator;
import com.ezviz.open.sdk.auth.token.resource.GeneralResourceTokenParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import top.yueshushu.common.utils.OutputResult;
import top.yueshushu.model.EzvizParam;
import top.yueshushu.model.WebhookMessage;
import top.yueshushu.model.tool.ToolRo;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/ezviz")
public class YingShiController {

	@RequestMapping(value = "/webhook")
	public ResponseEntity<String> VipWebhook(@RequestHeader HttpHeaders header, @RequestBody String body) {
		WebhookMessage receiveMessage = null;
		log.info("获取萤石 消息获取时间:{}", System.currentTimeMillis());
		try {
			receiveMessage = JSON.parseObject(body, WebhookMessage.class);
		} catch (Exception e) {
			//异常处理
			log.error("萤石消息转换出错: header: {}, body: {}", header, body, e);
		}
		Map<String, String> result = new HashMap<>(1);
		assert receiveMessage != null;
		String messageId = receiveMessage.getHeader().getMessageId();
		result.put("messageId", messageId);
		log.info("获取的消息id是:{}, 接收到的消息是:{}", messageId, body);
		final ResponseEntity<String> resp = ResponseEntity.ok(JSON.toJSONString(result));
		// log.info("萤石 返回的信息:{}",JSON.toJSONString(resp));
		return resp;
	}

	@GetMapping("/getToken")
	@ApiOperation("获取Token值")
	public OutputResult<String> getKey(EzvizParam ezvizParam) {
		GeneralResourceTokenGenerator generator = new GeneralResourceTokenGenerator();
		generator.init(ezvizParam.getAppKey(), ezvizParam.getSecret());

		GeneralResourceTokenParam param = new GeneralResourceTokenParam();
		// 创建一个action，不同业务的action参数不同，这里以终端入会业务为例。
		Action action = new Action(ezvizParam.getRetcInfo());
		//设置业务参数：strRoomId
		action.setAttribute("strRoomId", ezvizParam.getStrRoomId());
		//设置业务参数：customId
		action.setAttribute("customId", ezvizParam.getCustomId());
		//RTC场景下，可以使用本token控制终端在会议中的权限。1(0b0001)：可发送音频流；2(0b0010)：可发送视频流；4(0b0100)：可发起屏幕共享。同时授予三个权限，将三个权限对应编码相加。1+2+4=7。
		action.setAttribute("permission", ezvizParam.getPermission());
		param.addAction(action);
		// 设置appid
		param.setAppid(ezvizParam.getAppId());
		// 设置过期时间，最长过期时间为604800秒（7天）
		param.setExpire(ezvizParam.getExpire());
		String token = generator.generateToken(param);
		return OutputResult.ok(token);
	}

}
