package top.yueshushu.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import top.yueshushu.common.exception.BusinessException;
import top.yueshushu.common.exception.ErrorCode;
import top.yueshushu.common.utils.OutputResult;


/**
 * 异常处理器
 *
 * @author 两个蝴蝶飞 1290513799@qq.com
 * <a href="https://www.yueshushu.top/code">自动生成代码</a>
 */
@Slf4j
@RestControllerAdvice
public class BusinessExceptionHandler {
    /**
     * 处理自定义异常
     */
    @ExceptionHandler(BusinessException.class)
    public OutputResult<String> handleRenException(BusinessException ex) {

        return OutputResult.error(ex.getCode(), ex.getMsg());
    }

    /**
     * SpringMVC参数绑定，Validator校验不正确
     */
    @ExceptionHandler(BindException.class)
    public OutputResult<String> bindException(BindException ex) {
        FieldError fieldError = ex.getFieldError();
        assert fieldError != null;
        return OutputResult.error(fieldError.getDefaultMessage());
    }

    @ExceptionHandler(Exception.class)
    public OutputResult<String> handleException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return OutputResult.error(ErrorCode.INTERNAL_SERVER_ERROR);
    }

}