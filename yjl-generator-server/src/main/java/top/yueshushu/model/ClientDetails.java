package top.yueshushu.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName(value = "client_details")
@Data
public class ClientDetails {

	@TableId
	private String clientId;
	@TableField(value = "resource_ids")
	private String resourceIds;
	@TableField(value = "client_secret")
	private String clientSecret;
	@TableField(value = "scope")
	private String scope;
	@TableField(value = "authorized_grant_types")
	private String authorizedGrantTypes;
	@TableField(value = "web_server_redirect_uri")
	private String webServerRedirectUri;
	@TableField(value = "authorities")
	private String authorities;
	@TableField(value = "access_token_validity")
	private Integer accessTokenValidity;
	@TableField(value = "refresh_token_validity")
	private Integer refreshTokenValidity;
	@TableField(value = "additional_information")
	private String additionalInformation;
	@TableField(value = "autoapprove")
	private String autoApprove;

	// Getters and Setters
}