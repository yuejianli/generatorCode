package top.yueshushu.model;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>EzvizParam此类用于 ezviz的参数处理 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/7/18 17:58</p>
 * <p>@remark:</p>
 */
@Data
public class EzvizParam implements Serializable {

	private String appKey = "91685c862ce049dc816b02cb425e5bea";
	private String secret = "3de9a8674e9a791b24d7563ffbf57317";
  // 创建一个action，不同业务的action参数不同，这里以终端入会业务为例。
	private String retcInfo = "ERTC_INFO";
	// 设置业务参数：strRoomId
	private String strRoomId = "ID1699430483";
	// 设置业务参数：customId
	private String customId = "7ca19da6c7164bc5ad7e0a";
	// RTC场景下，可以使用本token控制终端在会议中的权限。1(0b0001)：可发送音频流；2(0b0010)：
	// 可发送视频流；4(0b0100)：可发起屏幕共享。同时授予三个权限，将三个权限对应编码相加。1+2+4=7。
	private String permission = "7";
	 // 设置appid
	private String appId = "f758a146b2b24fc7b9705e232bce9f02";
	// 设置过期时间，最长过期时间为604800秒（7天）
	private Integer expire = 604800;




}
