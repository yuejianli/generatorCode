package top.yueshushu.model;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>JarParam此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/13 16:52</p>
 * <p>@remark:</p>
 */
@Data
public class JarParam implements Serializable {
	private String fileName;
	private String fileFullName;
	private String filePath;
	private String logsPath;


	@Override
	public String toString() {
		return fileName + "    " + filePath + "    " + logsPath;
	}
	public String toConsole() {
		return fileName + "\t" + filePath + "\t" + logsPath;
	}
}
