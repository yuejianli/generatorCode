package top.yueshushu.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Blob;

/**
 * <p>OauthCode此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/29 17:50</p>
 * <p>@remark:</p>
 */
@TableName(value = "oauth_code")
@Data
public class MyOauthCode {
	@TableId(value = "code")
	private String code;
	@TableField(value = "authentication")
	private Blob authentication;
}
