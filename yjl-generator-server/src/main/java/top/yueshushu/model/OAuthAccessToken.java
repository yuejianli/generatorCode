package top.yueshushu.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Blob;

/**
 * <p>AccessToken此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/29 17:43</p>
 * <p>@remark:</p>
 */
@TableName(value = "oauth_access_token")
@Data
public class OAuthAccessToken {
	@TableId(value = "authentication_id")
	private String tokenId;
	@TableField(value = "token")
	private Blob token;
	@TableField(value = "user_name")
	private String userName;
	@TableField(value = "client_id")
	private String clientId;
	@TableField(value = "authentication")
	private Blob authentication;
	@TableField(value = "refresh_token")
	private String refreshToken;

}
