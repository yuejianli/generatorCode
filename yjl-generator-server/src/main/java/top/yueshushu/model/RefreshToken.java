package top.yueshushu.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Blob;

/**
 * <p>RefreshToken此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/29 17:46</p>
 * <p>@remark:</p>
 */
@TableName(value = "oauth_refresh_token")
@Data
public class RefreshToken {
	@TableId(value = "token_id")
	private String tokenId;
	@TableField(value = "token")
	private Blob token;
	@TableField(value = "authentication")
	private Blob authentication;
}
