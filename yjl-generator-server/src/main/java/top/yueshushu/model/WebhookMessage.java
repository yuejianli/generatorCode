package top.yueshushu.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class WebhookMessage implements Serializable {

    private WebhookMessageHeader header;

    private Object body;

}
