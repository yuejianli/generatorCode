package top.yueshushu.model.maven;

import lombok.Data;

/**
 * <p>MavenRo此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/5/13 0013 8:09</p>
 * <p>@remark:</p>
 */
@Data
public class MavenRo implements java.io.Serializable{
	private String url;
	private String groupId;
	private String artifactId;
	private String version;
	private String fileDownloadUrl;
}
