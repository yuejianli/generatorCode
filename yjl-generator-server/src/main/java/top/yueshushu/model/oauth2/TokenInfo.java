package top.yueshushu.model.oauth2;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>AuthorizeInfo此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2024/9/19 14:02</p>
 * <p>@remark:</p>
 */
@Data
public class TokenInfo implements Serializable {
	private String client_id;
	private String redirect_uri;
	private String state;
	private String scope;
	private String response_type;
	private String user_locale;
	private String client_secret;
	private String grant_type;
	private String code;
}
