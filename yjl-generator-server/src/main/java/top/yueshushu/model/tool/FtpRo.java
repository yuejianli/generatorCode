package top.yueshushu.model.tool;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>FtpRo此类用于 </p>
 * <p>@author: zgwl_yuejl </p>
 * <p>@date:2025/2/7 16:29</p>
 * <p>@remark:</p>
 */
@Data
public class FtpRo implements Serializable {
	private String server= "dev-uhftp.unisiot.com";
	private int port = 21;
	private String user = "uiothome";
	private String password = "JO3I8s6E9Sf";
	private String fileDir = "/upload/host_file/version/sql_version/";
	private String filePath = "PACT_VER_V3.885.01_ZH_CN.json";

	public String getFilePath() {
		if (filePath.startsWith("/upload/")) {
			return filePath;
		}
		return fileDir + filePath;
	}

}
