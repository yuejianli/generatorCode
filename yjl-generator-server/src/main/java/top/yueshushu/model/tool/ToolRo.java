package top.yueshushu.model.tool;

import lombok.Data;

import java.io.Serializable;

/**
 * 工具使用的Ro
 *
 * @author yuejianli
 * @date 2023-10-08
 */
@Data
public class ToolRo implements Serializable {
    private String content;

    private String content2;

    private String split = "\\,";

    private String property = "priceTagCode";

    private boolean removeTime = true;

    private Integer startId;
    private String oemFirm = "0X8003";
    private Integer state = 2;

    private String idProperty;
    private String dataProperty;
    private String keyUniqueProperty;
    private String oemProperty;
    private String stateProperty;
}
